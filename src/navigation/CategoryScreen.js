/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import EventEmitter from "@services/AppEventEmitter";

import { Color, Images, Styles, withTheme } from "@common";
import { TabBarIcon } from "@components";
import { Category } from "@containers";
import { Logo, Back, EmptyView } from "./IconNav";

@withTheme
export default class CategoryScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => {

    const dark = navigation.getParam("dark", false);
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );

    const handleBack = () => {
      EventEmitter.emit('cat-back-button');
      console.log('Event Emmitted', EventEmitter)
    }

    return {
      headerTitle: Logo(),
      headerLeft: Back(navigation, null, dark, handleBack),
      headerRight: EmptyView(),
      tabBarIcon: ({ tintColor }) => (
        <TabBarIcon
          css={{ width: 18, height: 18 }}
          icon={Images.IconCategory}
          tintColor={tintColor}
        />
      ),

      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitleStyle: Styles.Common.headerStyle,
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  componentDidMount() {
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    EventEmitter.addListener('cat-back-button', () => console.log('Connection 1 Emmiter'));

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {
        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Category
        onViewProductScreen={(item) => {
          navigate("DetailScreen", item);
        }}
        onViewCategory={(item) => {
          navigate("CategoryScreen", item);
        }}
        ref={child => {this.child = child}}
        navigation={this.props.navigation}
      />
    );
  }
}
