/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View } from "react-native";

import { Color, Styles, withTheme } from "@common";
import { toast } from "@app/Omni";
import { SafeAreaView, LogoSpinner } from "@components";
import { Detail } from "@containers";
import { Logo, Back, CartWishListIcons } from "./IconNav";

// API
import API from "@services/Common";

@withTheme
export default class DetailScreen extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      product: false,
      isLoading: true
    }
  }

  static navigationOptions = ({ navigation }) => {
    const dark = navigation.getParam("dark", false);
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );

    return {
      headerTitle: Logo(),
      tabBarVisible: false,
      headerLeft: Back(navigation, null, dark),
      headerRight: CartWishListIcons(navigation),
      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitleStyle: Styles.Common.headerStyle,
    };
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  async componentDidMount() {
    const { state } = this.props.navigation;
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    this.props.navigation.setParams({
      headerStyle: Styles.Common.toolbar(background, dark),
      dark,
    });
    console.log('Calling api')
    let prods = await API.getProductById(state.params.product.product_id);

    console.log(prods);

    try {
      if (prods && prods.success) {
        // prods.product_info.images.push(prods.product_info.thumb)
        prods.product_info.product_id = prods.product_id;
        this.setState({ product: prods.product_info, isLoading: false })
      } else {
        if (prods && prods.error) {
          toast(prods.error.message);
        }
      }
    } catch (error) {
      console.log('error', error)
    }
    this.setState({ isLoading: false });
  }

  formatProductResult = (prod) => {
    try {
      if (prod.options.length > 0) {
        const options = [];
        prod.options.map((data) => {
          data.selectedOption = data.product_option_value[0].name;
          options.push(data)
        });
        prod.options = options;
      }
      console.log('formatProductResult', prod)
      return prod;
    } catch (error) {
      console.log('Error formatProductResult => ', error)
      return prod;
    }
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.theme.dark !== nextProps.theme.dark) {
      const {

        theme: {
          colors: { background },
          dark,
        },
      } = nextProps;
      this.props.navigation.setParams({
        headerStyle: Styles.Common.toolbar(background, dark),
        dark,
      });
    }
  }

  render() {
    const { state, navigate } = this.props.navigation;
    console.log("state.params.product", state.params.product);
    const { product, isLoading } = this.state;

    const prods = typeof product === 'object' ? product : state.params.product;
    return (
      <SafeAreaView isSafeAreaBottom>
        {isLoading ? <LogoSpinner fullStretch /> :
          <View style={{ flex: 1 }}>
            {typeof state.params !== "undefined" && (
              <Detail
                product={this.formatProductResult(prods)}
                onViewCart={() => navigate("CartScreen")}
                onViewProductScreen={(_product) => {
                  this.props.navigation.setParams(_product);
                  navigate("DetailScreen", product)
                }}
                navigation={this.props.navigation}
                onLogin={() => navigate("LoginScreen")}
                onOpenWebsite={(url) => navigate("CustomPage", { url, isBack: true })}
              />
            )}
          </View>
        }
      </SafeAreaView>
    );
  }
}
