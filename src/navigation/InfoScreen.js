/** @format */

import React, { PureComponent } from "react";
import { View, Dimensions, Text, Animated, TextInput, I18nManager, TouchableOpacity, StyleSheet } from "react-native";
import { WebView } from "react-native-webview";
import { Color, Styles, withTheme } from "@common";
import { CustomPage } from "@containers";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Menu, NavBarLogo, Back } from "./IconNav";
import { toast } from "@app/Omni";
import API from '@services/Common';
const { width, scale, height } = Dimensions.get("window");
import Spinner from "@components/Spinner";


export default class InfoScreen extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      info: '',
      loading: false,
      form: false,
      contactForm: {
        name: '',
        email: '',
        enquiry: '',
      },
      btnLoading: false,
    }
  }

  static navigationOptions = ({ navigation }) => {
    const headerStyle = navigation.getParam(
      "headerStyle",
      Styles.Common.toolbar()
    );
    const dark = navigation.getParam("dark", false);
    const isBack = navigation.getParam("isBack", false);
    return {
      headerTitle: NavBarLogo({ navigation }),
      headerLeft: isBack ? Back(navigation) : Menu(dark),

      headerTintColor: Color.headerTintColor,
      headerStyle,
      headerTitleStyle: Styles.Common.headerStyle,

      // use to fix the border bottom
      headerTransparent: true,
    };
  };

  async componentDidMount() {
    const {
      navigation,
    } = this.props;

    const { state: { params } } = navigation;

    if (params.modal) {
      this.setState({ form: true });
    } else {
      this.setState({ loading: true });

      let info = await API.getInformation(params.information_id);
      if (info) {
        this.setState({ info, loading: false });
      } else {
        toast('Something went wrong!')
        this.setState({ info: false, loading: false })
      }
    }

  }


  render() {
    const { state } = this.props.navigation;
    const { info, loading, form } = this.state;

    if (loading) {
      return <Spinner mode="overlay" />;
    }

    if (loading === false && info === false) {
      return <View><Text style={{ textAlign: 'center' }}>No Info Available...</Text></View>;
    }

    const getHTML = () => {

      return `<html><head><style type="text/css">
                    body {
                      margin: 8;
                      padding: 0 10px;
                      font: 32px arial, sans-serif;
                      background: #fff;
                      color:black;
                    }
                    a, h1, h2, h3, li {
                      font: 14px arial, sans-serif !important;
                    }
                    img {
                      height: auto;
                    }
              </style></head><body>${info}</body>`;
    };

    return (
      <WebView
        source={{ html: getHTML() }}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
        style={{
          marginTop: 60,
          width: '100%',
          flex: 1
        }}
      />
    );

  }
}


const styles = StyleSheet.create({
  container: {
    marginTop: 60 + 40,
    flexDirection: 'row',
    height: height / 1.5,
    margin: 10,
  },
  contactTitle: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20
  },
  contactContainer: {
    marginLeft: 5,
    marginRight: 5,
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 20
  },
  input: {
    borderBottomWidth: 1,
    borderColor: Color.blackTextPrimary,
    height: 20,
    marginBottom: 30,
    marginTop: 4,
    padding: 0,
    margin: 0,
    paddingLeft: 10,
    paddingBottom: 4,
    fontSize: 12,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: Color.blackTextPrimary,
  },
  textarea: {
    borderBottomWidth: 1,
    borderColor: Color.blackTextPrimary,
    marginBottom: 30,
    marginTop: 4,
    padding: 0,
    margin: 0,
    paddingLeft: 10,
    paddingBottom: 4,
    fontSize: 12,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: Color.blackTextPrimary,
  },
  title: {
    color: 'red'
  },
  lable: {
    color: Color.Text,
    fontSize: 14,
    fontWeight: 'bold'
  },
  submitBtnTxt: {
    backgroundColor: Color.attributes.green,
    textAlign: 'center',
    padding: 10,
    fontWeight: 'bold',
    color: '#fff',
    borderRadius: 5,
    marginLeft: 60,
    marginRight: 60,
  },
});