/**
 * Created by Premji on 01/03/2017.
 * An API for JSON API Auth Word Press plugin.
 * https://wordpress.org/plugins/json-api-auth/
 *
 * @format
 */

import { AppConfig } from "@common";
import { auth_request, common_request, menu_request } from "../Omni";

import WPUserAPI from './WPUserAPI';


const url = AppConfig.WooCommerce.url;

const postParams = async (data) => {
  let param = await WPUserAPI.getSetLocalData();
  const cart_id = await WPUserAPI.getSetCardId();

  console.log('postParams', cart_id)

  if (!data) {
    data = {};
  }
  data.language_id = 1;
  data.language_code = 'en';
  data.currency = 'USD';

  if (param) {
    data.language_id = param.language_id;
    data.language_code = param.language_code;
    data.currency = param.currency;
  }

  if (cart_id) {
    data.cart_id = cart_id;
  }

  return data;
}

const getParamsCartId = async (data) => {
  const cart_id = await WPUserAPI.getSetCardId();

  if (cart_id) {
    data += `&cart_id=${cart_id}`;
  }

  return data;
}

const getParamsCurrency = async (data) => {
  let param = await WPUserAPI.getSetLocalData();

  if (param) {
    data += `&language_code=${param.language_code}`;
    data += `&currency=${param.currency}`;
  } else {
    data += `&language_code=en`;
    data += `&currency=USD`;
  }

  return data;
}

const getParams = async (data) => {
  let param = await WPUserAPI.getSetLocalData();
  const cart_id = await WPUserAPI.getSetCardId();
  

  if (param) {
    data += `&language_id=${param.language_id}`;
    data += `&language_code=${param.language_code}`;
    data += `&currency=${param.currency}`;
  } else {
    data += `&language_id=1`;
    data += `&language_code=en`;
    data += `&currency=USD`;
  }

  if (cart_id) {
    data += `&cart_id=${cart_id}`;
  }

  return data;
}

const tokenParams = async (data = {}) => {
  let param = await WPUserAPI.getSetLocalData();
  let token = await WPUserAPI.getSetDeviceToken();

  if (!data) {
    data = {};
  }
  data.language_id = 1;
  data.language_code = 'en';
  data.currency = 'USD';
  if (param) {
    data.language_id = param.language_id;
    data.language_code = param.language_code;
    data.currency = param.currency;
  }
  if (token) {
    data.device_token = token;
  }
  return data;
}

const API = {
  getHomeData: async (id = 1, code = 'en') => {
    const _url = await getParamsCartId(`${url}/index.php?route=app/home/modules`);
    return await auth_request(_url, { language_id: id, language_code: code, currency: 'USD' }, true);
  },
  createCartId: async () => {
    const _url = `${url}/index.php?route=app/quote/add`;
    let params = await tokenParams();
    return await common_request(_url, params, true);
  },
  getCartId: async () => {
    const _url = `${url}/index.php?route=app/quote/mine`;
    let params = await postParams();
    return await auth_request(_url, params, true);
  },
  getFilters: async (category_id) => {
    const _url = `${url}/index.php?route=app/filter/filters`;
    let params = await getParams(_url);
    if (category_id) {
      params += `&category_id=${category_id}`;
    }
    return await auth_request(params);
  },
  getGuestCardId: async () => {
    const _url = `${url}/index.php?route=app/quote/guest`;
    return await common_request(_url, await tokenParams(), true);
  },
  getUserInfo: async () => {
    const _url = getParams(`${url}/index.php?route=app/account/info`);
    return await common_request(_url);
  },
  getCartProductCount: async () => {
    const _url = `${url}/index.php?route=app/shopping/items`;
    // {
    //   "language_id": "1",
    //   "language_code": "en",
    //   "cart_id": "4-1113"
    // }
    return await auth_request(_url, { language_id: id, language_code: code, cart_id: '' }, true);
  },
  mergeGuestToCustomerCart: async (guest, customer) => {
    const _url = `${url}/index.php?route=app/shopping/merge`;
    // {
    //   "language_id": "1",
    //   "language_code": "en",
    //   "guest_cart_id": "5-9095",
    //   "customer_cart_id": "5-9095"
    //   }
    let params = await tokenParams({ guest_cart_id: guest, customer_cart_id: customer });
    return await auth_request(_url, params, true);
  },

  getProducts: async (category_id, limit, page, sort) => {
    // console.log('getProducts => ', category_id, limit, page, sort)
    let formatted = `${url}/index.php?route=app/catalog/products&category_id=${category_id}&page=${page} 
    ${sort.hasOwnProperty('order') ? `&sort=${sort.orderby}&order=${sort.order}` : '&sort=pd.name&order=ASC'}&limit=${limit}`;

    if (sort.hasOwnProperty('filter') && sort.filter && sort.filter.hasOwnProperty('filters') && sort.filter.filters.length > 0) {
      formatted += `&filter=`;
      sort.filter.filters.map((data, i) => {
        formatted += `${data.filter_id}${sort.filter.filters.length != (i + 1) ? `,` : ``}`;
      })
    };

    const _url = await getParams(formatted);
    return await auth_request(_url);
  },
  searchProducts: async (name, per_page, page, filter = {}) => {
    console.log('getProducts => ', name, per_page, page, filter)

    let tempParams = `search=${name}&page=${page}&limit=${per_page}`;
    if (filter.hasOwnProperty('category')) {
      tempParams += `&category_id=${filter.category}`;
    }
    if (filter.hasOwnProperty('desc') && filter.desc) {
      tempParams += `&description=${filter.desc}`;
    }
    if (filter.hasOwnProperty('sub') && filter.sub) {
      tempParams += `&sub_category=${filter.sub}`;
    }
    if (filter.hasOwnProperty('sort') && filter.sort) {
      tempParams += `&sort=${filter.sort.orderby}&order=${filter.sort.order}`;
    }
    const _url = await getParams(`${url}/index.php?route=app/catalog/search&${tempParams}`);

    return await auth_request(_url);
  },
  assignPaymentAddressToCart: async (params) => {
    const _url = `${url}/index.php?route=app/payment/add`;
    let param = await postParams(params);
    return await auth_request(_url, param, true);
  },
  assignShippingAddressToCart: async (params) => {
    const _url = `${url}/index.php?route=app/shipping/add`;
    let param = await postParams(params);
    return await auth_request(_url, param, true);
  },
  shippingMethods: async () => {
    const _url = await getParamsCurrency(`${url}/index.php?route=app/shipping/methods`);
    let param = await postParams();
    return await auth_request(_url, param, true);
  },
  paymentMethods: async (param = {}) => {
    const _url = await getParamsCurrency(`${url}/index.php?route=app/payment/methods`); //USD (through GET request)
    let format = await postParams(param)
    return await auth_request(_url, format, true);
  },
  addOrder: async (payload) => {
    const _url = await getParamsCurrency(`${url}/index.php?route=app/order/add`); //USD (through GET request)
    let param = await postParams(payload);
    return await auth_request(_url, param, true);
  },
  confirmOrder: async (order_id) => {
    const _url = `${url}/index.php?route=app/order/confirm`;
    let param = await postParams({ order_id });
    return await auth_request(_url, param, true);
  },
  orderStatus: async (order_id) => {
    const _url = `${url}/index.php?route=app/order/status`;
    let param = await postParams({ order_id });
    return await auth_request(_url, param, true);
  },
  myOrders: async (page = 1) => {
    // token
    const _url = `${url}/index.php?route=app/order/mine&page=${page}`;
    return await auth_request(_url);
  },
  getOrderById: async (order_id) => {
    // token
    const _url = `${url}/index.php?route=app/order/info&order_id=${order_id}`;
    return await auth_request(_url);
  },
  getBrands: async () => {
    const _url = `${url}/index.php?route=app/feed/brand`;
    return await auth_request(_url);
  },
  getProductById: async (product_id) => {
    const _url = await getParams(`${url}/index.php?route=app/catalog/info&product_id=${product_id}`); // WPUserAPI.getSetCardId  getParams
    return await auth_request(_url);
  },
  getReviewById: async (product_id, page = 1, limit = 5) => {
    const _url = await getParams(`${url}/index.php?route=app/review/reviews&product_id=${product_id}&page=${page}limit=${limit}`); // WPUserAPI.getSetCardId  getParams
    return await auth_request(_url);
  },
  registerEmail: async (product_id, page = 1, limit = 5) => {
    const _url = `${url}/index.php?route=app/customer/registration_email`;
    return await auth_request(_url);
  },
  addReview: async (review) => {
    const { name, product_id, rating } = review;
    // token added in url
    const _url = `${url}/index.php?route=app/review/add`;
    let params = await postParams(
      {
        product_id: product_id,
        name: name,
        text: review.review,
        rating: rating,
      }
    );
    return await common_request(_url, params, true);
  },
  getCartItem: async () => {
    const _url = await getParams(`${url}/index.php?route=app/cart/items`);
    return await auth_request(_url, await postParams(), true);
  },
  getInformation: async (id) => {
    const _url = await getParams(`${url}/index.php?route=app/information&information_id=${id}`);
    return await menu_request(_url);
  },
  contactus: async (param = {}) => {
    const _url = `${url}/index.php?route=app/contact/add`;
    return await common_request(_url, await postParams(param), true);
  },
  addCartItem: async (param = {}) => {
    const _url = `${url}/index.php?route=app/shopping/add`;
    return await auth_request(_url, await postParams(param), true);
  },
  editCartItem: async (param = {}) => {
    const _url = `${url}/index.php?route=app/shopping/edit`;
    return await auth_request(_url, await postParams(param), true);
  },
  deleteCartItem: async (key) => {
    const _url = `${url}/index.php?route=app/shopping/delete`;
    return await auth_request(_url, await postParams({ key: key }), true);
  },
  getUserAddress: async () => {
    const _url = await getParamsCurrency(`${url}/index.php?route=app/address/addresses`);
    return await common_request(_url);
  },
  removeAddress: async (param) => {
    const _url = `${url}/index.php?route=app/address/delete`
    return await common_request(_url, await postParams(param), true);
  },
  addUserAddress: async (form) => {
    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      _default,
      password
    } = form;
    const _url = `${url}/index.php?route=app/address/add`
    return await common_request(_url,
      {
        display_name: `${firstname} ${lastname}`,
        firstname: firstname,
        lastname: lastname,
        telephone: telephone,
        fax: fax,
        company: company,
        address_1: address_1,
        address_2: address_2,
        city: city,
        country_id: country_id,
        country: country,
        postcode: postcode,
        zone_id: zone_id,
        default: _default,
        state: state,
      }, true);
  },
  updateUserAddress: async (form) => {
    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      _default,
      address_id
    } = form;
    const _url = `${url}/index.php?route=app/address/edit`
    return await common_request(_url,
      {
        display_name: `${firstname} ${lastname}`,
        firstname: firstname,
        lastname: lastname,
        telephone: telephone,
        fax: fax,
        company: company,
        address_1: address_1,
        address_2: address_2,
        city: city,
        country_id: country_id,
        country: country,
        postcode: postcode,
        zone_id: zone_id,
        default: _default,
        state: state,
        address_id: address_id,
      }, true);
  },
};

export default API;
