/**
 * Created by Premji on 01/03/2017.
 * An API for JSON API Auth Word Press plugin.
 * https://wordpress.org/plugins/json-api-auth/
 *
 * @format
 */

import { AppConfig } from "@common";
import { request, common_request, error } from "../Omni";
import store from "@store/configureStore";

const url = AppConfig.WooCommerce.url;

const Api = {
  login: async (data = {}) => {
    return await common_request(
      `${url}/index.php?route=app/customer/login`, data, true);
  },
};

export default Api;
