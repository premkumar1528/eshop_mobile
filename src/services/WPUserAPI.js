/**
 * Created by Premji on 01/03/2017.
 * An API for JSON API Auth Word Press plugin.
 * https://wordpress.org/plugins/json-api-auth/
 *
 * @format
 */

import { AppConfig } from "@common";
import { request, common_request, error } from "./../Omni";
import AsyncStorage from '@react-native-community/async-storage';

const url = AppConfig.WooCommerce.url;
const isSecured = url.startsWith("https");
const secure = isSecured ? "" : "&insecure=cool";
const cookieLifeTime = 120960000000;

const getLocalData = (data) => data != null ? JSON.parse(data) : false;
let AppToken, cartId, localData = false;

const getParamsCurrency = async (data) => {
  let param = await WPUserAPI.getSetLocalData();

  if (param) {
    data += `&language_code=${param.language_code}`;
    data += `&currency=${param.currency}`;
  } else {
    data += `&language_code=en`;
    data += `&currency=USD`;
  }

  return data;
}

const postParams = async (data = {}) => {
  let param = await WPUserAPI.getSetLocalData();

  if (!data) {
    data = {};
  }
  data.language_id = 1;
  data.language_code = 'en';
  if (param) {
    data.language_id = data.language_id;
    data.language_code = data.language_code;
  }
  return data;
}


const WPUserAPI = {
  login: async (username, password) => {
    const _url = `${url}/index.php?route=app/customer/login`;
    const param = await postParams({ email: username, password: password });
    return await common_request(_url, param, true)
  },
  loginFacebook: async (token) => {
    const _url = `${url}/api/mstore_user/fb_connect/?second=${cookieLifeTime}&access_token=${token}${secure}`;
    return await request(_url);
  },
  loginSMS: async (token) => {
    const _url = `${url}/api/mstore_user/sms_login/?access_token=${token}${secure}`;
    return await request(_url);
  },
  getLang: async (id, code) => {
    const _url = `${url}/index.php?route=app/local/language`;
    return await common_request(_url, { language_id: id, language_code: code });
  },
  getCurrency: async (id, code) => {
    const _url = `${url}/index.php?route=app/local/currency`;
    return await common_request(_url, { language_id: id, language_code: code });
  },
  getCounrys: async (id = 1, code = 'en') => {
    const _url = `${url}/index.php?route=app/local/country`;
    return await common_request(_url, { language_id: id, language_code: code });
  },

  register: async ({
    username,
    firstname,
    lastname,
    email,
    telephone,
    fax,
    company,
    address_1,
    address_2,
    city,
    postcode,
    country,
    country_id,
    state,
    zone_id,
    confirm,
    password
  }) => {
    try {
      const _url = `${url}/index.php?route=app/customer/registration`;
      return await common_request(_url,
        {
          display_name: `${firstname} ${lastname}`,
          firstname: firstname,
          lastname: lastname,
          email: email,
          telephone: telephone,
          fax: fax,
          username: username,
          company: company,
          address_1: address_1,
          address_2: address_2,
          city: city,
          country_id: country_id,
          country: country,
          postcode: postcode,
          zone_id: zone_id,
          state: state,
          password: password,
          confirm: confirm
        }, true);
    } catch (err) {
      error(err);
      return { error: err };
    }
  },
  forgotPassword: async ({
    email,
  }) => {
    try {
      // const nonce = await WPUserAPI.getNonce();
      const _url = `${url}/index.php?route=app/customer/forgot_password`;
      console.log('APi Hit url', _url)
      return await common_request(_url, { email: email }, true);
    } catch (err) {
      error(err);
      return { error: err };
    }
  },
  getSetLocalData: async (loginData = false) => {

    try {
      if (loginData === 'reset') {
        localData = false;
        await AsyncStorage.removeItem("@loginAuthData")
        return false;
      }
      if (loginData) {
        localData = loginData;
        console.log('Set Local Data', loginData);
        return await AsyncStorage.setItem("@loginAuthData", JSON.stringify(loginData));
      }
      console.log('get Local Data', localData)
      if (localData && localData != null) return localData;
      const json = await AsyncStorage.getItem("@loginAuthData");
      localData = getLocalData(json);
      return localData;
    } catch (e) {
      console.log('error getSetToken', e)
      return false
    }
  },
  getSetDeviceToken: async (loginData = {}, set = false) => {
    if (set) {
      return await AsyncStorage.setItem("@deviceToken", JSON.stringify(loginData));
    } else {
      const json = await AsyncStorage.getItem("@deviceToken");
      return json;
    }
  },
  getSetToken: async (loginData) => {
    try {
      if (loginData === 'reset') {
        AppToken = false;
        return await AsyncStorage.removeItem("@Token");
      }
      if (loginData) {
        await AsyncStorage.setItem("@Token", JSON.stringify(loginData), () => {
          console.log('set Token', loginData)
          AppToken = loginData;
          return AppToken;
        });
      }
      if (AppToken && AppToken != null && AppToken != "reset") return (AppToken);
      await AsyncStorage.getItem("@Token", (result) => {
        AppToken = getLocalData(result);
        return (AppToken);
      });
    } catch (e) {
      console.log('error getSetToken', e)
      return (false);
    }
  },
  getSetCardId: async (set) => {
    console.log('getSetCardId', set, typeof set)
    if (set === 'reset') {
      cartId = false;
      await AsyncStorage.removeItem("@cartId")
      return false;
    }
    if (typeof set === 'string' && set != '' && set != 'reset') {
      cartId = set;
      return cartId;
    }
    return cartId;
  },
  logOut: async () => {
    await WPUserAPI.getSetToken('reset');
    await WPUserAPI.getSetCardId('reset');
    cartId = false;
  },
};

export default WPUserAPI;
