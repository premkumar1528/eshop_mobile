/**
 * Created by Premji on 17/02/2017.
 *
 * @format
 */
import reactotron from "reactotron-react-native";
import { PixelRatio, ToastAndroid } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import store from "@store/configureStore";
import _EventEmitter from "EventEmitter";
import { Images, Constants, Config } from "@common";
import _Icon from "react-native-vector-icons/MaterialCommunityIcons";
import _IconIO from "react-native-vector-icons/Ionicons";
import _Timer from "react-timer-mixin";
import _Validate from "./ultils/Validate";
import _BlockTimer from "./ultils/BlockTimer";
import _FacebookAPI from "./services/FacebookAPI";

import WPUserAPI from "@services/WPUserAPI";


// const { actions: SideMenuActions } = require('@redux/SideMenuRedux')

export const Icon = _Icon;
export const IconIO = _IconIO;
export const EventEmitter = new _EventEmitter();
export const Timer = _Timer;
export const Validate = _Validate;
export const BlockTimer = _BlockTimer;
export const FacebookAPI = _FacebookAPI;
export const Reactotron = reactotron;

const _log = (values) => __DEV__ && reactotron.log(values);
const _warn = (values) => __DEV__ && reactotron.warn(values);
const _error = (values) => __DEV__ && reactotron.error(values);
export function connectConsoleToReactotron() {
  // console.log = _log;
  // console.warn = _warn;
  // console.error = _error;
}
export const log = _log;
export const warn = _warn;
export const error = _error;

/**
 * An async fetch with error catch
 * @param url
 * @param data
 * @returns {Promise.<*>}
 */

const formData = (data) => {
  const body = new FormData();
  for (let key in data) {
    body.append(key, data[key]);
  }
  return body;
}

const fetchOptions = (body, method, token = false) => {
  // console.log('fetchOptions', body, method, token)
  return (
    method ? token ? {
      method: "POST",
      headers: { "Content-Type": "multipart/form-data", "Customer-Authorization": token },
      body: formData(body),
    } : {
        method: "POST",
        headers: { "Content-Type": "multipart/form-data" },
        body: formData(body),
      } : {
        method: "GET",
        headers: { "Content-Type": "application/json", "Customer-Authorization": token },
      }
  )
};

const fetchOptionsJson = (body, method, token = false) => {

  return (
    method ? token ? {
      method: "POST",
      headers: { "Content-Type": "application/json", "Customer-Authorization": token },
      body: JSON.stringify(body),
    } : {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(body),
      } : token ? {
        method: "GET",
        headers: { "Content-Type": "application/json", "Customer-Authorization": token },
      } : {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          },
        }
  )
};

export const request = async (url, data = {}) => {
  try {
    _warn(url)
    const response = await fetch(url, data);
    _warn(response)
    return await response.json();
  } catch (err) {
    _error(err);
    return { error: err };
  }
};

export const common_request = async (url, data = {}, method = false) => {
  console.log('common_request URL: ' + url, 'data: ' + JSON.stringify(data));
  let token = await WPUserAPI.getSetToken();
  try {
    _warn(url)
    console.log('common_request', fetchOptions(data, method, token), token);
    // console.log('token', await AsyncStorage.getItem("@Token"))
    const response = await fetch(url, fetchOptions(data, method, token));
    _warn(response)
    return await response.json();
  } catch (err) {
    _error(err);
    return { error: err };
  }
};

export const auth_request = async (url, data = {}, method = false) => {
  console.log('auth_request URL: ' + url, 'data: ' + JSON.stringify(data));
  // let params = await AsyncStorage.getItem("@local_params");
  let token = await WPUserAPI.getSetToken();
  try {
    console.log('auth_request', url, fetchOptionsJson(data, method, token), token);
    _warn(url)
    const response = await fetch(url, fetchOptionsJson(data, method, token));
    _warn(response)
    return await response.json();
  } catch (err) {
    _error(err);
    return { error: err };
  }
};

export const menu_request = async (url, data = {}, method = false) => {
  // console.log('auth_request URL: ' + url, 'data: ' + JSON.stringify(data));
  // let params = await AsyncStorage.getItem("@local_params");
  // let token = await WPUserAPI.getSetToken();
  try {
    console.log('auth_request', url);
    _warn(url)
    const response = await fetch(url);
    _warn(response)
    return await response.text();
  } catch (err) {
    _error(err);
    return { error: err };
  }
};

// Drawer
export const openDrawer = () =>
  // EventEmitter.emit(Constants.EmitCode.SideMenuOpen)
  store.dispatch({
    type: Constants.EmitCode.SideMenuOpen,
  });
export const closeDrawer = () =>
  // EventEmitter.emit(Constants.EmitCode.SideMenuClose)
  store.dispatch({
    type: Constants.EmitCode.SideMenuClose,
  });
export const toggleDrawer = () =>
  // EventEmitter.emit(Constants.EmitCode.SideMenuClose)
  store.dispatch({
    type: Constants.EmitCode.SideMenuToggle,
  });

/**
 * Display the message toast-like (work both with Android and iOS)
 * @param msg Message to display
 * @param duration Display duration
 */
export const toast = (msg, duration = 4000) =>
  EventEmitter.emit(Constants.EmitCode.Toast, msg, duration);

export const CustomToast = (props) => {
  if (props.visible) {
    ToastAndroid.showWithGravityAndOffset(
      props.message,
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM
    );
    return null;
  }
  return null;
};

export const getProductImage = (uri, containerWidth) => {
  // Enhance number if you want to fetch a better quality image (may affect performance
  const DPI_NUMBER = 0.5; // change this to 1 for high quality image

  if (!Config.ProductSize.enable) {
    return uri;
  }

  if (typeof uri !== "string") {
    return Images.PlaceHolderURL;
  }

  // parse uri into parts
  const index = uri.lastIndexOf(".");
  let editedURI = uri.slice(0, index);
  const defaultType = uri.slice(index);

  const pixelWidth = PixelRatio.getPixelSizeForLayoutSize(containerWidth);

  switch (true) {
    case pixelWidth * DPI_NUMBER < 300:
      editedURI = `${editedURI}-small${defaultType}`;
      break;
    case pixelWidth * DPI_NUMBER < 600:
      editedURI = `${editedURI}-medium${defaultType}`;
      break;
    case pixelWidth * DPI_NUMBER < 1400:
      editedURI = `${editedURI}-large${defaultType}`;
      break;
    default:
      editedURI += defaultType;
  }
  return editedURI;
};

export const getNotification = async () => {
  try {
    const notification = await AsyncStorage.getItem("@notification");
    return JSON.parse(notification);
  } catch (error) {
    // console.log(error);
  }
};
