/** @format */

import { Constants, Tools, Languages } from "@common";
import { WooWorker } from "api-ecommerce";
import Validate from "../ultils/Validate.js";
import { toast } from "@app/Omni";

import WPUserAPI from '@services/WPUserAPI';
import API from '@services/Common';

const types = {
  ADD_CART_ITEM: "ADD_CART_ITEM",
  LOAD_CART_ITEM: "LOAD_CART_ITEM",
  REMOVE_CART_ITEM: "REMOVE_CART_ITEM",
  DELETE_CART_ITEM: "DELETE_CART_ITEM",
  EMPTY_CART: "EMPTY_CART",
  CREATE_NEW_ORDER_PENDING: "CREATE_NEW_ORDER_PENDING",
  CREATE_NEW_ORDER_SUCCESS: "CREATE_NEW_ORDER_SUCCESS",
  CREATE_NEW_ORDER_ERROR: "CREATE_NEW_ORDER_ERROR",
  VALIDATE_CUSTOMER_INFO: "VALIDATE_CUSTOMER_INFO",
  INVALIDATE_CUSTOMER_INFO: "INVALIDATE_CUSTOMER_INFO",
  FETCH_MY_ORDER: "FETCH_MY_ORDER",
  FETCH_CART_PENDING: "FETCH_CART_PENDING",
  GET_SHIPPING_METHOD_PENDING: "GET_SHIPPING_METHOD_PENDING",
  GET_SHIPPING_METHOD_SUCCESS: "GET_SHIPPING_METHOD_SUCCESS",
  GET_SHIPPING_METHOD_FAIL: "GET_SHIPPING_METHOD_FAIL",
  SELECTED_SHIPPING_METHOD: "SELECTED_SHIPPING_METHOD",
  GET_ORDER_NOTES_PENDING: "GET_ORDER_NOTES_PENDING",
  GET_ORDER_NOTES_SUCCESS: "GET_ORDER_NOTES_SUCCESS",
  GET_ORDER_NOTES_FAIL: "GET_ORDER_NOTES_FAIL",
};

const validateOption = (key, option) => {
  console.log(key, option)
  let value = option.product_option_value.find((data) => (data.name).toLowerCase() === (key).toLowerCase());
  // console.log('validateOption', value)

  if (option.type === 'checkbox') {
    let options = '';
    option.product_option_value.map(data => {
      if (data.checked) {
        options += ',' + product_option_value_id;
      }
    })
    return options != '' ? options : false;
  }
  return value ? value.product_option_value_id : false;
}

const formatProduct = async (prod) => {

  let respose = {}, _option = {};
  respose.option = {};
  try {
    respose.product_id = prod.product_id;
    respose.quantity = prod.quantity;
    respose.cart_id = await WPUserAPI.getSetCardId();

    // prod.quantity = 1;
    prod.product = prod;

    if (prod.hasOwnProperty('options') && prod.options.length > 0) {
      prod.options.map((data) => {
        if (data.selectedOption && validateOption(data.selectedOption, data)) {
          _option[`${data.product_option_id}`] = validateOption(data.selectedOption, data)
          respose.option = _option;
          // console.log('=======>', data.product_option_id, validateOption(data.selectedOption, data))
        }
      })
    }
  } catch (error) {
    console.log(error)
  }

  prod.respose = respose;
  return prod;
}

export const actions = {
  addCartItem: async (dispatch, _product, variation) => {
    console.log('add to cart product', _product)
    let product = await formatProduct(_product);
    let responce = await API.addCartItem(product.respose);
    console.log('addCartItem response:', responce)
    if (responce && responce.hasOwnProperty('error')) {
      let cartID = await API.getCartId();
      if (responce.error.message == "Invalid cart id!") {
        if (cartID.success) {
          await WPUserAPI.getSetCardId(cartID.cart_id);
        }
        toast("Something went wrong!. Please try again.");
      } else {
        toast(responce.error.message);
      }
      return false;
    } else {
      toast(responce.success.message, 4000);
      actions.loadCartItem(dispatch, responce.cart);
    }

    // let cart_data = await API.getCartItem();
    // if (cart_data && cart_data.products && cart_data.products.length > 0) {
    //   actions.loadCartItem(dispatch, cart_data);
    // }
    // dispatch({
    //   type: types.ADD_CART_ITEM,
    //   product,
    //   variation,
    // });
  },

  fetchMyOrder: async (dispatch, user) => {
    dispatch({ type: types.FETCH_CART_PENDING });

    let orders = await API.myOrders();
    console.log('Mhy Orders', orders);

    dispatch({
      type: types.FETCH_MY_ORDER,
      orders,
    });


  },

  updateCartItem: async (dispatch, product, quantity) => {
    let responce = await API.editCartItem({ quantity, key: product.key });
    console.log('update cart response', responce)
    if (responce && responce.hasOwnProperty('error')) {
      let cartID = await API.getCartId();
      if (responce.error.message == "Invalid cart id!") {
        if (cartID.success) {
          await WPUserAPI.getSetCardId(cartID.cart_id);
        }
        toast("Something went wrong!. Please try again.");
      } else {
        toast(responce.error.message);
      }
      return false;
    } else {
      toast(responce.success.message, 4000);
      actions.loadCartItem(dispatch, responce.cart);
    }
  },
  removeCartItem: async (dispatch, product, variation) => {

    console.log('remove cart item', product);



    // dispatch({
    //   type: types.REMOVE_CART_ITEM,
    //   product,
    //   variation,
    // });
  },

  loadCartItem: async (dispatch, product) => {
    dispatch({
      type: types.LOAD_CART_ITEM,
      product,
    });
  },

  deleteCartItem: async (dispatch, product, variation, quantity) => {

    let responce = await API.deleteCartItem(product.key);
    console.log('removeCartItem', responce);
    if (responce && responce.hasOwnProperty('error')) {
      toast(responce.error.message, 2000)
      return false;
    } else {
      toast(responce.success.message, 4000);
      actions.loadCartItem(dispatch, responce.cart);
    }

    dispatch({
      type: types.DELETE_CART_ITEM,
      product,
      variation,
      quantity,
    });
  },

  emptyCart: (dispatch) => {
    dispatch({
      type: types.EMPTY_CART,
    });
  },
  validateCustomerInfo: (dispatch, customerInfo) => {
    const { first_name, last_name, address_1, email, phone } = customerInfo;
    if (
      first_name.length == 0 ||
      last_name.length == 0 ||
      address_1.length == 0 ||
      email.length == 0 ||
      phone.length == 0
    ) {
      dispatch({
        type: types.INVALIDATE_CUSTOMER_INFO,
        message: Languages.RequireEnterAllFileds,
      });
    } else if (!Validate.isEmail(email)) {
      dispatch({
        type: types.INVALIDATE_CUSTOMER_INFO,
        message: Languages.InvalidEmail,
      });
    } else {
      dispatch({
        type: types.VALIDATE_CUSTOMER_INFO,
        message: "",
        customerInfo,
      });
    }
  },
  createNewOrder: async (dispatch, payload) => {
    dispatch({ type: types.CREATE_NEW_ORDER_PENDING });
    const json = await WooWorker.createOrder(payload);

    if (json.hasOwnProperty("id")) {
      // dispatch({type: types.EMPTY_CART});
      dispatch({ type: types.CREATE_NEW_ORDER_SUCCESS, orderId: json.id });
    } else {
      dispatch({
        type: types.CREATE_NEW_ORDER_ERROR,
        message: Languages.CreateOrderError,
      });
    }
  },
  getShippingMethod: async (dispatch, payload) => {
    dispatch({ type: types.GET_SHIPPING_METHOD_PENDING });
    const json = undefined; //await WooWorker.getShippingMethod(payload);

    if (json === undefined) {
      dispatch({
        type: types.GET_SHIPPING_METHOD_FAIL,
        message: Languages.ErrorMessageRequest,
      });
    } else if (json.code) {
      dispatch({ type: types.GET_SHIPPING_METHOD_FAIL, message: json.message });
    } else {
      dispatch({ type: types.GET_SHIPPING_METHOD_SUCCESS, shippings: json });
    }
  },
  selectShippingMethod: (dispatch, shippingMethod) => {
    dispatch({ type: types.SELECTED_SHIPPING_METHOD, shippingMethod });
  },

  finishOrder: async (dispatch, payload) => {
    dispatch({ type: types.CREATE_NEW_ORDER_SUCCESS });
  },

  getOrderNotes: async (dispatch, orderId) => {
    dispatch({ type: types.GET_ORDER_NOTES_PENDING });
    const json = await WooWorker.getOrderNotes(orderId);

    if (json === undefined) {
      dispatch({
        type: types.GET_ORDER_NOTES_FAIL,
        message: Languages.ErrorMessageRequest,
      });
    } else if (json.code) {
      dispatch({ type: types.GET_ORDER_NOTES_FAIL, message: json.message });
    } else {
      dispatch({ type: types.GET_ORDER_NOTES_SUCCESS, orderNotes: json });
    }
  },
};

const initialState = {
  cartItems: [],
  total: 0,
  totalPrice: 0,
  totals: {},
  myOrders: [],
  isFetching: false,
};

export const reducer = (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case types.ADD_CART_ITEM: {
      const isExisted = state.cartItems.some((cartItem) =>
        compareCartItem(cartItem, action)
      );
      console.log(isExisted, state.cartItems, cartItem(undefined, action))
      console.log([...state.cartItems, cartItem(undefined, action)])

      return Object.assign(
        {},
        state,
        isExisted
          ? { cartItems: state.cartItems.map((item) => cartItem(item, action)) }
          : { cartItems: [...state.cartItems, cartItem(undefined, action)] },
        {
          total: state.total + 1,
          totalPrice: state.totalPrice + getPrice(action),
        }
      );
    }
    case types.LOAD_CART_ITEM: {
      console.log(type, action.product)
      return Object.assign(
        {},
        state,
        {
          cartItems: formatCartItem(action.product),
          totals: action.product.totals,
          total: getTotal(action.product, true),
          totalPrice: getTotal(action.product),
        }
      );
    }

    case types.REMOVE_CART_ITEM: {
      const index = state.cartItems.findIndex((cartItem) =>
        compareCartItem(cartItem, action)
      ); // check if existed
      return index == -1
        ? state // This should not happen, but catch anyway
        : Object.assign(
          {},
          state,
          state.cartItems[index].quantity == 1
            ? {
              cartItems: state.cartItems.filter(
                (cartItem) => !compareCartItem(cartItem, action)
              ),
            }
            : {
              cartItems: state.cartItems.map((item) =>
                cartItem(item, action)
              ),
            },
          {
            total: state.total - 1,
            totalPrice: state.totalPrice - getPrice(action),
          }
        );
    }

    case types.DELETE_CART_ITEM: {
      const index1 = state.cartItems.findIndex((cartItem) =>
        compareCartItem(cartItem, action)
      ); // check if existed
      return index1 == -1
        ? state // This should not happen, but catch anyway
        : Object.assign({}, state, {
          cartItems: state.cartItems.filter(
            (cartItem) => !compareCartItem(cartItem, action)
          ),
          total: state.total - Number(action.quantity),
          totalPrice:
            state.totalPrice - Number(action.quantity) * getPrice(action),
        });
    }

    case types.EMPTY_CART:
      return Object.assign({}, state, {
        type: types.EMPTY_CART,
        cartItems: [],
        total: 0,
        totalPrice: 0,
      });

    case types.INVALIDATE_CUSTOMER_INFO:
      return Object.assign({}, state, {
        message: action.message,
        type: types.INVALIDATE_CUSTOMER_INFO,
      });

    case types.VALIDATE_CUSTOMER_INFO:
      return Object.assign({}, state, {
        message: null,
        type: types.VALIDATE_CUSTOMER_INFO,
        customerInfo: action.customerInfo,
      });

    case types.CREATE_NEW_ORDER_SUCCESS:
      return Object.assign({}, state, {
        type: types.CREATE_NEW_ORDER_SUCCESS,
        cartItems: [],
        total: 0,
        totalPrice: 0,
      });

    case types.CREATE_NEW_ORDER_ERROR:
      return Object.assign({}, state, {
        type: types.CREATE_NEW_ORDER_ERROR,
        message: action.message,
      });

    case types.FETCH_MY_ORDER:
      return Object.assign({}, state, {
        type: types.FETCH_MY_ORDER,
        isFetching: false,
        myOrders: action.orders.orders,
      });

    case types.FETCH_CART_PENDING: {
      return {
        ...state,
        isFetching: true,
      };
    }

    case types.GET_SHIPPING_METHOD_PENDING:
    case types.GET_ORDER_NOTES_PENDING: {
      return Object.assign({}, state, {
        ...state,
        isFetching: true,
        error: null,
      });
    }

    case types.GET_SHIPPING_METHOD_FAIL:
    case types.GET_ORDER_NOTES_FAIL: {
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error,
      });
    }

    case types.GET_SHIPPING_METHOD_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        shippings: action.shippings,
        error: null,
      });
    }

    case types.GET_ORDER_NOTES_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        orderNotes: action.orderNotes,
        error: null,
      });
    }

    case types.SELECTED_SHIPPING_METHOD: {
      return Object.assign({}, state, {
        ...state,
        shippingMethod: action.shippingMethod,
      });
    }

    default: {
      return state;
    }
  }
};

const compareCartItem = (cartItem, action) => {
  // warn(action.variation);
  if (action.variation) {
    if (cartItem.variation) {
      return (
        cartItem.product.product_id === action.product.product_id &&
        cartItem.variation.id === action.variation.id
      );
    }
    return false;
  }

  return cartItem.product.product_id == action.product.product_id;
};

const cartItem = (
  state = { product: undefined, quantity: 1, variation: undefined },
  action
) => {
  switch (action.type) {
    case types.ADD_CART_ITEM:
      return state.product === undefined
        ? Object.assign({}, state, {
          product: action.product,
          variation: action.variation,
        })
        : !compareCartItem(state, action)
          ? state
          : Object.assign({}, state, {
            quantity:
              state.quantity < Constants.LimitAddToCart
                ? state.quantity + 1
                : state.quantity,
          });
    case types.REMOVE_CART_ITEM:
      return !compareCartItem(state, action)
        ? state
        : Object.assign({}, state, { quantity: state.quantity - 1 });
    default:
      return state;
  }
};

const formatCartItem = (product) => {
  let responce = [];

  try {
    if (product && product.products && product.products.length > 0) {
      product.products.map((element) => {
        responce.push({
          product: element,
          quantity: element.quantity,
          variation: null
        })
      });
    }
  } catch (error) {
    console.log('formatCartItem', error, product)
  }

  return responce;
};

// get price from variation or product and format
function getTotal(action, quantity) {
  let total = 0;

  try {
    if (!action) return false;
    if (action && !(action.products)) return false;
    if (action.products.length > 0) {
      action.products.map((prod) => {
        total += prod.quantity;
      })
    };
  } catch (error) {
    console.log('error in getTotal:', error, action)
  }


  return quantity ? total : action.totals[1].text.slice(1);
}

function getPrice(action) {
  return Number(
    action.variation === undefined ||
      action.variation == null ||
      action.variation.price === undefined ||
      action.variation.price === ""
      ? Tools.getPriceIncluedTaxAmount(action.product, null, true)
      : Tools.getPriceIncluedTaxAmount(action.product, null, true)
  );
}
