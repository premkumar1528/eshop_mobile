/**
 * Created by Premji on 14/02/2017.
 *
 * @format
 */

const types = {
  LOGOUT: "LOGOUT",
  LOGIN: "LOGIN_SUCCESS",
  FORGOT: "FORGOT_SUCCESS",
  FINISH_INTRO: "FINISH_INTRO",
  LANGUAGE: "SUCCESS_LANGUAGE",
  CURRENCY: "SUCCESS_CURRENCY",
};

export const user_actions = {
  login: (user, token) => {
    return { type: types.LOGIN, user: user.customer_info, token };
  },
  logout() {
    return { type: types.LOGOUT };
  },
  finishIntro() {
    return { type: types.FINISH_INTRO };
  },
  fotGotPass() {
    return { type: types.FORGOT };
  },
  languages: (languages) => {
    return { type: types.LANGUAGE, languages };
  },
  currency: (currencys) => {
    return { type: types.CURRENCY, currencys };
  },
};

const initialState = {
  user: null,
  token: null,
  finishIntro: null,
  languages: [],
  currencys: [],
};

const initialLogOutState = {
  user: null,
  token: null,
  finishIntro: true,
  languages: [],
  currencys: [],
};

export const reducer = (state = initialState, action) => {
  const { type, user, languages, currencys, token } = action;

  switch (type) {
    case types.LOGOUT:
      return {...initialLogOutState};
    case types.LOGIN:
      return { ...state, user, token };
    case types.FORGOT:
      return { ...state };
    case types.FINISH_INTRO:
      return { ...state, finishIntro: true };
    case types.LANGUAGE:
      return { ...state, languages: languages ? languages : [] };
    case types.CURRENCY:
      return { ...state, currencys: currencys ? currencys : [] };
    default:
      return state;
  }
};
