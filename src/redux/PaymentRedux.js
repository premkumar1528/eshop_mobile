/**
 * Created by Premji on 06/03/2017.
 *
 * @format
 */

import { WooWorker } from "api-ecommerce";

import API from  '@services/Common'

const types = {
  PAYMENT_FETCH_SUCCESS: "PAYMENT_FETCH_SUCCESS",
  PAYMENT_FETCHING: "PAYMENT_FETCHING",
  PAYMENT_FETCH_FAILURE: "PAYMENT_FETCH_FAILURE",
  SHIP_FETCH_SUCCESS: "SHIP_FETCH_SUCCESS",
  SHIP_FETCHING: "SHIP_FETCHING",
  SHIP_FETCH_FAILURE: "SHIP_FETCH_FAILURE",
};

export const actions = {
  fetchShipments: async (dispatch) => {
    dispatch({ type: types.SHIP_FETCHING });

    console.log('shipment is fetching', types.SHIP_FETCHING)

    // const json = await WooWorker.getPayments();
    let json = await API.shippingMethods();

    console.log('shipping fetching', json)

    if (json === undefined) {
      dispatch({ type: types.SHIP_FETCH_FAILURE });
    } else if (json.error) {
      dispatch({ type: types.SHIP_FETCH_FAILURE });
    } else {
      dispatch({
        type: types.SHIP_FETCH_SUCCESS,
        payload: json.shipping_methods,
        finish: true,
      });
    }
  },
  fetchPayments: async (dispatch, code) => {
    dispatch({ type: types.PAYMENT_FETCHING });

    // const json = await WooWorker.getPayments();
    let json = await API.paymentMethods(code);

    console.log('Payment fetching', json)

    if (json === undefined) {
      dispatch({ type: types.PAYMENT_FETCH_FAILURE });
    } else if (json.error) {
      dispatch({ type: types.PAYMENT_FETCH_FAILURE });
    } else {
      dispatch({
        type: types.PAYMENT_FETCH_SUCCESS,
        payload: json.payment_methods,
        finish: true,
      });
    }
  },
};

const initialState = {
  list: [],
  shipMethod: [],
  isFetching: false,
};

export const reducer = (state = initialState, action) => {
  const { extra, type, payload, finish } = action;

  switch (type) {
    case types.PAYMENT_FETCH_SUCCESS:
      return {
        ...state,
        list: payload, //,payload.filter((payment) => payment.enabled === true),
        isFetching: false,
      };

    case types.PAYMENT_FETCH_FAILURE:
      return {
        ...state,
        finish: true,
        isFetching: false,
      };

    case types.PAYMENT_FETCHING:
      return {
        ...state,
        isFetching: true,
      };

    case types.SHIP_FETCH_SUCCESS:
      return {
        ...state,
        shipMethod: payload, //,payload.filter((SHIP) => SHIP.enabled === true),
        isFetching: false,
      };

    case types.SHIP_FETCH_FAILURE:
      return {
        ...state,
        finish: true,
        isFetching: false,
      };

    case types.SHIP_FETCHING:
      return {
        ...state,
        isFetching: true,
      };

    default:
      return state;
  }
};
