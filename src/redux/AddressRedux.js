/** @format */

import { Config } from "@common";
// import { warn } from '@app/Omni'
import { WooWorker } from "api-ecommerce";
import _ from "lodash";

const types = {
  ADD_ADDRESS: "ADD_ADDRESS",
  REMOVE_ADDRESS: "REMOVE_ADDRESS",
  SELECTED_ADDRESS: "SELECTED_ADDRESS",
  INIT_ADDRESSES: "INIT_ADDRESSES",
  INIT_ALL_ADDRESSES: "INIT_ALL_ADDRESSES",
  UPDATE_SELECTED_ADDRESS: "UPDATE_SELECTED_ADDRESS",
};

export const actions = {
  addAddress: (dispatch, address) => {
    dispatch({ type: types.ADD_ADDRESS, address });
  },

  removeAddress: (dispatch, index) => {
    dispatch({ type: types.REMOVE_ADDRESS, index });
  },

  selectAddress: (dispatch, address) => {
    dispatch({ type: types.SELECTED_ADDRESS, address });
  },
  initAllAddresses: (dispatch, address) => {
    if (address.length > 0) {
      address = address.map((data) => {
        data.first_name = data.firstname;
        data.last_name = data.lastname;
        return data;
      })
    }
    console.log('dispatch', address)
    dispatch({ type: types.INIT_ALL_ADDRESSES, address });
  },
  initAddresses: (dispatch, customerInfo) => {
    const address = {
      first_name: customerInfo.firstname,
      last_name: customerInfo.lastname,
      address_1: customerInfo.address_1,
      address_2: customerInfo.address_2,
      address_format: customerInfo.address_format,
      address_id: customerInfo.address_id,
      city: customerInfo.city,
      company: customerInfo.company,
      country: customerInfo.country,
      country_id: customerInfo.country_id,
      custom_field: customerInfo.custom_field,
      is_default: customerInfo.is_default,
      iso_code_2: customerInfo.iso_code_2,
      iso_code_3: customerInfo.iso_code_3,
      postcode: customerInfo.postcode,
      zone: customerInfo.zone,
      zone_code: customerInfo.zone_code,
      zone_id: customerInfo.zone_id,
      phone: customerInfo.telephone,
    };
    dispatch({ type: types.INIT_ADDRESSES, address });
  },
  updateSelectedAddress: (dispatch, address) => {
    dispatch({ type: types.UPDATE_SELECTED_ADDRESS, address });
  },
};

const initialState = {
  list: [],
  reload: false,
};

export const reducer = (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case types.ADD_ADDRESS: {
      state.list.push(action.address);
      return {
        ...state,
        reload: !state.reload,
      };
    }
    case types.REMOVE_ADDRESS: {
      state.list.splice(action.index, 1);
      return {
        ...state,
        reload: !state.reload,
      };
    }
    case types.SELECTED_ADDRESS: {
      return {
        ...state,
        reload: state.reload,
        selectedAddress: action.address,
      };
    }
    case types.INIT_ADDRESSES: {
      return {
        ...state,
        reload: !state.reload,
        selectedAddress: action.address,
      };
    }
    case types.INIT_ALL_ADDRESSES: {
      return {
        ...state,
        reload: !state.reload,
        list: action.address,
      };
    }
    case types.UPDATE_SELECTED_ADDRESS: {
      const list = state.list || [];
      let index = -1;
      list.forEach((item, i) => {
        if (_.isEqual(item, state.selectedAddress)) {
          index = i;
        }
      });
      if (index > -1) {
        list.splice(index, 1);
      }
      list.push(action.address);
      return {
        ...state,
        reload: !state.reload,
        list,
        selectedAddress: action.address,
      };
    }
    default: {
      return state;
    }
  }
};
