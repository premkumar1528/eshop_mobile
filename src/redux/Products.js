/**
 * Created by Premji on 06/03/2017.
 *
 * @format
 */

const types = {
  PRODUCT_SUCCESS: "PRODUCT_SUCCESS",
  FILTER_SUCCESS: "FILTER_SUCCESS",
  CART_SUCCESS: "CART_SUCCESS",
  GET_CART_ID: "GET_CART_ID",
  PRODUCT_FETCH_FAILURE: "PRODUCT_FETCH_FAILURE",
};

export const product_actions = {
  load_products: (products) => {
    return { type: types.PRODUCT_SUCCESS, products };
  },
  load_filters: (products) => {
    return { type: types.PRODUCT_SUCCESS, products };
  },
};

const initialState = {
  list: [],
  isFetching: false,
};

export const reducer = (state = initialState, action) => {
  const { type, products, filter } = action;

  switch (type) {
    case types.PRODUCT_SUCCESS:
      return {
        ...state,
        list: products,
      };

    case types.FILTER_SUCCESS:
      return {
        ...state,
        _fiters: filter,
      };

    default:
      return state;
  }
};
