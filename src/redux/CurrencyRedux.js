/**
 * Created by Premji on 06/03/2017.
 *
 * @format
 */

import { Config } from '@common';
import WPUserAPI from "@services/WPUserAPI";
// import { warn, log } from '@app/Omni'
// import CurrencyWorker from '@services/CurrencyWorker'

const types = {
  CHANGE_CURRENCY: "CHANGE_CURRENCY",
};

export const actions = {
  changeCurrency: async (dispatch, currency) => {
    console.log()
    let data = await WPUserAPI.getSetLocalData();
    await WPUserAPI.getSetLocalData({
      language_id: data.language_id,
      language_code: data.language_code,
      currency: currency.code,
    })
    dispatch({ type: types.CHANGE_CURRENCY, currency });
  },
};

const initialState = {
  symbol: Config.DefaultCurrency.symbol_left,
  name: Config.DefaultCurrency.name,
  symbol_native: Config.DefaultCurrency.symbol,
  decimal_digits: Config.DefaultCurrency.precision,
  rounding: 0,
  code: Config.DefaultCurrency.code,
  name_plural: Config.DefaultCurrency.name_plural,
  currency_id: Config.DefaultCurrency.currency_id,
  name: Config.DefaultCurrency.name,
  symbol_left: Config.DefaultCurrency.symbol_left,
  symbol_right: Config.DefaultCurrency.symbol_right,
};

export const reducer = (state = initialState, action) => {
  const { currency } = action;
  switch (action.type) {
    case types.CHANGE_CURRENCY:
      return Object.assign({}, state, { ...currency });
    default:
      return state;
  }
};
