/**
 * Created by Premji on 14/02/2017.
 *
 * @format
 */

import { Config, AppConfig } from "@common";

// import { warn } from '@app/Omni'
import { request, common_request, error } from "./../Omni";
const url = AppConfig.WooCommerce.url;
import { WooWorker } from "api-ecommerce";

const types = {
  FETCH_CATEGORIES_PENDING: "FETCH_CATEGORIES_PENDING",
  FETCH_CATEGORIES_SUCCESS: "FETCH_CATEGORIES_SUCCESS",
  FETCH_CATEGORIES_FAILURE: "FETCH_CATEGORIES_FAILURE",

  SWITCH_DISPLAY_MODE: "SWITCH_DISPLAY_MODE",
  SET_SELECTED_CATEGORY: "SET_SELECTED_CATEGORY",
  SET_PREV_CATEGORY: "SET_PREV_CATEGORY",
  CATEGORY_SELECT_LAYOUT: "CATEGORY_SELECT_LAYOUT",
};

export const DisplayMode = {
  ListMode: "ListMode",
  GridMode: "GridMode",
  CardMode: "CardMode",
};


const formatCatogories = (categories) => {
  // console.log('categories', categories);
  let cat_props = categories;
  let cats = [];
  if (categories && categories.length) {
    categories.map((data) => {
      data.id = Number(data.category_id);
      data.parent = 0;
      data.menu_order = 0;
      data.count = data.children ? data.children.length : 0;
      cats.push(data);
      if (data.children && data.children.length) {
        data.children.map((d) => {
          d.parent = Number(data.category_id);
          d.id = Number(d.category_id);
          d.count = 0;
          cats.push(d);
        })
      }
    });
    // console.log('final categories', cats);
    return cats;
  }
  return cat_props;
}

export const actions = {
  fetchCategories: async (dispatch) => {
    dispatch({ type: types.FETCH_CATEGORIES_PENDING });
    const _url = `${url}/index.php?route=app/feed/category`;
    const res = await common_request(_url)
    const json = await formatCatogories(res.categories); // woocom plugin 

    if (json === undefined) {
      dispatch(actions.fetchCategoriesFailure(JSON.stringify(res)));
    } else if (json.code) {
      dispatch(actions.fetchCategoriesFailure(json.message));
    } else {
      dispatch(actions.fetchCategoriesSuccess(json));
    }
  },
  fetchCategoriesSuccess: (items) => {
    return { type: types.FETCH_CATEGORIES_SUCCESS, items };
  },
  fetchCategoriesFailure: (error) => {
    return { type: types.FETCH_CATEGORIES_FAILURE, error };
  },
  switchDisplayMode: (mode) => {
    return { type: types.SWITCH_DISPLAY_MODE, mode };
  },
  setSelectedCategory: (category) => {
    return { type: types.SET_SELECTED_CATEGORY, category };
  },
  setPrevCategory: (category) => {
    return { type: types.SET_PREV_CATEGORY, category };
  },
  setActiveLayout: (value) => {
    return { type: types.CATEGORY_SELECT_LAYOUT, value };
  },
};

const initialState = {
  isFetching: false,
  error: null,
  displayMode: DisplayMode.GridMode,
  list: [],
  selectedCategory: null,
  previousSelectedCats: [],
  selectedLayout: Config.CategoryListView,
};

export const reducer = (state = initialState, action) => {
  const { type, mode, error, items, category, value } = action;

  switch (type) {
    case types.FETCH_CATEGORIES_PENDING: {
      return {
        ...state,
        isFetching: true,
        error: null,
      };
    }
    case types.FETCH_CATEGORIES_SUCCESS: {
      return {
        ...state,
        isFetching: false,
        list: items ? items : [],
        error: null,
      };
    }
    case types.FETCH_CATEGORIES_FAILURE: {
      return {
        ...state,
        isFetching: false,
        list: [],
        error,
      };
    }
    case types.SWITCH_DISPLAY_MODE: {
      return {
        ...state,
        displayMode: mode,
      };
    }
    case types.SET_SELECTED_CATEGORY: {
      return {
        ...state,
        selectedCategory: category,
      };
    }
    case types.SET_PREV_CATEGORY: {
      return {
        ...state,
        previousSelectedCats: category,
      };
    }
    case types.CATEGORY_SELECT_LAYOUT:
      return {
        ...state,
        isFetching: false,
        selectedLayout: value || false,
      };

    default: {
      return state;
    }
  }
};
