/** @format */

import { StyleSheet, I18nManager } from "react-native";
import { Constants, Color } from "@common";

export default StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "red",
    position: 'relative',
    borderBottomWidth: 1,
    borderBottomColor: "#d4dce1",
  },
  content: {
    flexDirection: "row",
    margin: 10,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  infoView: {
    marginLeft: 10,
    marginRight: 10,
    flex: 1,
  },
  title: {
    fontSize: 15,
    fontFamily: Constants.fontFamily,
    color: Color.Text,
  },
  priceContainer: {
    flexDirection: I18nManager.isRTL ? "row-reverse" : "column",
    marginTop: 10,
    alignItems: "flex-start",
    justifyContent: "flex-start",
    backgroundColor: "white",
  },
  totalPriceContainer: {
    flexDirection: I18nManager.isRTL ? "row-reverse" : "row",
    marginTop: 10,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  productPrice: {
    fontSize: 14,
    paddingTop: 3,
    color: Color.blackTextSecondary,
    fontFamily: Constants.fontFamily,
  },
  sale_price_con: {
    flexDirection: "row",
    justifyContent: "center",
  },
  sale_price: {
    textDecorationLine: "line-through",
    color: Color.blackTextDisable,
    marginLeft: 5,
    fontSize: 12,
    marginTop: 4,
    fontFamily: Constants.fontFamily,
  },
  price: {
    fontSize: 14,
    color: Color.Text,
    fontFamily: Constants.fontHeader,
  },
  totalPrice: {
    fontSize: 14,
    color: Color.Text,
    fontFamily: Constants.fontHeader,
  },
  totalTax: {
    fontSize: 12,
    color: Color.Text,
    fontFamily: Constants.fontHeader,
  },
  productVariant: (textColor) => ({
    marginLeft: 10,
    fontSize: 12,
    color: textColor,
    fontFamily: Constants.fontHeader,
  }),
  quantity: {
    marginRight: 10,
  },
  variantContainer: {
    flex: 1
  },
  variantContainerTitle: {
    flex: 1,
    fontSize: 14,
    color: Color.Text,
    marginBottom: 5,
    fontStyle: 'italic',
    fontWeight: 'bold',
  },
  icon: {
    width: 15,
    height: 15,
    resizeMode: "contain",
    margin: 10,
  },
  btnTrash: {
    position: "absolute",
    right: I18nManager.isRTL ? 'auto' : 70,
    left: I18nManager.isRTL ? 115 : 'auto',
    bottom: I18nManager.isRTL ? 'auto' : 10,
    top: I18nManager.isRTL ? 10 : 'auto',
  },
  btnRemoveTrash: {
    position: "absolute",
    right: I18nManager.isRTL ? 'auto' : 20,
    left: I18nManager.isRTL ? 115 : 'auto',
    bottom: I18nManager.isRTL ? 'auto' : 10,
    top: I18nManager.isRTL ? 10 : 'auto',
  },
  star: (textColor) => ({
    justifyContent: I18nManager.isRTL ? "flex-end" : 'flex-start',
    marginTop: 8,
    marginLeft: I18nManager.isRTL ? 30 : 0,
  }),
});
