/** @format */

import React, { PureComponent } from "react";
import { TouchableOpacity, Text, View, Image, Dimensions } from "react-native";

import ChangeQuantity from "@components/ChangeQuantity";
import { connect } from "react-redux";
import { withTheme, Tools } from "@common";
import styles from "./styles";
import { Rating } from '@components'

class ProductItem extends PureComponent {
  onChangeQuantity = (quantity) => {
    console.log('onchange quantity', this.props.quantity, quantity)
    if (this.props.quantity < quantity) {
      this.props.updateCartItem(this.props.product, quantity);
    } else {
      this.props.updateCartItem(this.props.product, quantity);
    }
  };

  render() {
    const {
      product,
      quantity,
      viewQuantity,
      viewDeleteBtn,
      viewRemoveBtn,
      isSearch,
      variation,
      onPress,
      onRemove,
    } = this.props;
    const {
      theme: {
        colors: { background, text, lineColor },
        dark: isDark,
      },
    } = this.props;

    console.log('product view', product)

    return (
      <View
        style={[
          styles.container,
          { backgroundColor: background },
          isDark && { borderBottomColor: lineColor },
        ]}>
        <View style={styles.content}>
          <TouchableOpacity onPress={() => onPress({ product })}>
            <Image
              source={{ uri: product.thumb }}
              style={styles.image}
            />
          </TouchableOpacity>

          <View
            style={[
              styles.infoView,
              { width: Dimensions.get("window").width - 180 },
            ]}>
            <TouchableOpacity onPress={() => onPress({ product })}>
              <Text style={[styles.title, { color: text }]}>
                {product.name}
              </Text>
            </TouchableOpacity>
            <View style={styles.totalPriceContainer}>
              <Text style={[styles.totalPrice, { color: text }]}>
                Model: <Text style={{ color: '#ff0000' }}>{product.model}</Text>
              </Text>
            </View>
            <View style={styles.priceContainer}>
              {
                isSearch && product.strike_price != '' ?
                  <View style={styles.sale_price_con}>
                    <Text style={styles.sale_price}>{product.price}</Text><Text>&nbsp;</Text><Text style={[styles.productPrice, { color: text }]}>{product.strike_price}</Text>
                  </View>
                  :
                  null
              }
              
              {product.option && product.option.length > 0 &&
                typeof product.option !== "undefined" &&
                <View style={styles.variantContainer}>
                  <Text style={styles.variantContainerTitle}>Variants: </Text>
                  {
                    product.option.map((variant) => {
                      return (
                        <Text
                          key={variant.name}
                          style={styles.productVariant(text)}>
                          {variant.name} - {variant.value},
                        </Text>
                      );
                    })}
                </View>
              }
            </View>
            {
              isSearch ?
                <View style={styles.totalPriceContainer}>
                  <Text style={[styles.totalTax, { color: text }]}>
                    Ex Tax: <Text style={{ color: '#ff0000' }}>{product.tax}</Text>
                  </Text>
                </View> :
                <View style={styles.totalPriceContainer}>
                  <Text style={[styles.totalPrice, { color: text }]}>
                    Price: <Text style={{ color: '#ff0000' }}>{product.price}</Text>
                  </Text>
                </View>
            }
            {
              isSearch && product.rating > 0 ? <Rating style={styles.star(viewRemoveBtn)} rating={Number(product.rating)} size={19} /> : null
            }
          </View>
          {viewQuantity && (
            <ChangeQuantity
              style={styles.quantity}
              quantity={quantity}
              product={product}
              onChangeQuantity={this.onChangeQuantity}
            />
          )}
        </View>

        {viewDeleteBtn && (
          <TouchableOpacity
            style={styles.btnTrash}
            onPress={() => onRemove(product, variation)}>
            <Image
              source={require("@images/ic_trash.png")}
              style={[styles.icon, { tintColor: text }]}
            />
          </TouchableOpacity>
        )}
        {viewRemoveBtn && (
          <TouchableOpacity
            style={styles.btnRemoveTrash}
            onPress={() => onRemove(product, variation)}>
            <Image
              source={require("@images/ic_trash.png")}
              style={[styles.icon, { tintColor: text }]}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CartRedux");
  return {
    ...ownProps,
    ...stateProps,
    updateCartItem: (product, quantity) => {
      actions.updateCartItem(dispatch, product, quantity);
    },
    removeCartItem: (product, variation) => {
      actions.removeCartItem(dispatch, product, variation);
    },
  };
}

export default connect(
  null,
  undefined,
  mergeProps
)(withTheme(ProductItem));
