import React from 'react'
import {
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Animated,
  Text
} from 'react-native'
import styles from './style'
import { Images, withTheme } from '@common'

class Item extends React.Component {

  render() {
    let { item, onPress, selected, edit, onRemove } = this.props;

    var address = ""
    if (item.address_1 != "") {
      address += item.address_1 + ", " + item.address_2 + ", "
    }

    if (item.city != "") {
      address += item.city + ", "
    }

    if (item.zone != "") {
      address += item.zone + ", "
    }

    if (item.country != "") {
      address += item.country
    }

    const {
      theme: {
        colors: {
          background, text
        }
      }
    } = this.props;

    let validStyle = {
      backgroundColor: background
    };

    if (selected) {
      validStyle['borderColor'] = 'green';
      validStyle['borderWidth'] = 1;
    }

    return (
      <TouchableOpacity style={[styles.container, validStyle]} activeOpacity={0.85} onPress={onPress}>
        <View style={styles.content}>
          <Text style={[styles.name, { color: text }]}>{item.first_name + " " + item.last_name}</Text>
          {/* <Text style={[styles.text, {color: text}]}>{item.email}</Text>
          <Text style={[styles.text, {color: text}]}>{item.phone}</Text> */}
          <Text style={[styles.text, { color: text }]}>{item.postcode}</Text>
          <Text style={[styles.text, { color: text }]}>{address}</Text>
        </View>
        <View style={styles.buttons}>
          <TouchableOpacity onPress={edit}>
            <Image source={Images.IconEdit} style={[styles.icon, { tintColor: "blue" }]} />
          </TouchableOpacity>
          {!selected && <View />}
          {!selected && (
            <TouchableOpacity onPress={onRemove}>
              <Image source={require("@images/ic_trash.png")} style={styles.icon} />
            </TouchableOpacity>
          )}
        </View>
      </TouchableOpacity>
    )
  }

}

export default withTheme(Item)
