import React from "react";

import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { Dimensions, StyleSheet, View, Text, TouchableOpacity, Image } from 'react-native';

const { width: screenWidth } = Dimensions.get('window')

export default class DoubleBanner extends React.PureComponent {

    constructor(props) {
        super(props);
    }

    render() {
        const { onViewPost, config } = this.props;

        // onPress={onViewPost}


        let imageData = config && config._item && config._item.info ? config._item.info : false;
        console.log('DoubleBanner', imageData);

        return (
            <View style={[styles.container, styles.boxShadow]}>
                {
                    imageData &&
                    imageData.map((data, index) => {
                        console.log('image data', data.image)

                        return (
                            index < 3 &&
                            <TouchableOpacity
                                onPress={() => onViewPost(data)}
                                style={[styles.imageContainer(index)]}
                            >
                                <Image
                                    style={imageData.length > 2 ? styles.item3 : styles.item}
                                    key={index}
                                    resizeMode='contain'
                                    source={{ uri: data.image }}
                                />
                            </TouchableOpacity>
                        )
                    })
                }
            </View>
        );
    }
}

const imageHeightWidth = (screenWidth / 2);
const commonPadding = 15;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: screenWidth,
        height: 'auto',
        backgroundColor: 'white',
        marginTop: 5,
        padding: 0,
    },
    boxShadow: {
        borderWidth: 1,
        borderRadius: 0,
        borderColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        marginBottom: 10,
        elevation: 1,
    },
    item: {
        width: '90%',
        height: '100%',
        backgroundColor: 'white',
        display: 'flex',
        borderColor: '#ebe8e8',
        borderWidth: 1,
    },
    item3: {
        width: imageHeightWidth - 56,
        height: imageHeightWidth - 100,
        overflow: 'hidden',
        marginTop: 3,
        marginBottom: 0,
    },
    imageContainer: (index) => ({
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 0 }), // Prevent a random Android rendering issue
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingTop: commonPadding,
        paddingBottom: commonPadding,
        paddingLeft: index === 0 ? commonPadding : 0,
        justifyContent: 'space-between',
        width: imageHeightWidth - 1,
        height: imageHeightWidth,
        overflow: 'hidden',
    }),
    image: {
        ...StyleSheet.absoluteFillObject,
    },
})
