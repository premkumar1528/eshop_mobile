/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { FlatList, View, Text } from "react-native";
import { Constants, Images, Config, Languages, withTheme, AppConfig } from "@common";
import { HorizonLayout, AdMob, BannerSlider, BannerImage, PostList, BlogList } from "@components";
import { find } from "lodash";
import styles from "./styles";
import Categories from "./Categories";
import Slider from "./Slider";
import DoubleBanner from "./double_banner";
import HHeader from "./HHeader";
import { warn } from "@app/Omni";

class HorizonList extends PureComponent {
  static propTypes = {
    config: PropTypes.object,
    index: PropTypes.number,
    fetchPost: PropTypes.func,
    fetchNews: PropTypes.func,
    onShowAll: PropTypes.func,
    list: PropTypes.array,
    fetchProductsByCollections: PropTypes.func,
    setSelectedCategory: PropTypes.func,
    onViewProductScreen: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
    collection: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.page = 1;
    this.limit = Constants.pagingLimit;
    this.defaultList = [
      {
        id: 1,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 2,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
      {
        id: 3,
        name: Languages.loading,
        images: [Images.PlaceHolder],
      },
    ];
  }

  /**
   * handle load more
   */
  _nextPosts = () => {
    const { config, index, fetchPost, fetchNews, collection } = this.props;
    this.page += 1;
    if (!collection.finish) {
      fetchPost({ config, index, page: this.page });
    }
  };

  _viewAll = () => {
    const {
      config,
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
      fetchNews
    } = this.props;
    const selectedCategory = find(
      list,
      (category) => category.category_id == config._item.category_id
    );
    console.log('_viewAll', selectedCategory, this.props)
    setSelectedCategory(selectedCategory);
    fetchProductsByCollections(config._item.category_id, config.tag, this.page, index);
    onShowAll(selectedCategory, index);
  };

  showProductsByCategory = (config) => {
    const {
      onShowAll,
      index,
      list,
      fetchProductsByCollections,
      setSelectedCategory,
    } = this.props;
    console.log('config.category', config)
    const selectedCategory = find(
      list,
      (category) => category.category_id == config._item.category_id
    );
    setSelectedCategory(selectedCategory);
    fetchProductsByCollections(config.category, config.tag, this.page, index);
    onShowAll(config, index);
  };

  onViewProductScreen = (product) => {
    console.log('on product screens')
    this.props.onViewProductScreen({ product });
  };

  onTapDoubleBanner = (product) => {
    if (product.entity === "category") {
      product.category_id = product.entity_id;
      const {
        config,
        onShowAll,
        index,
        list,
        fetchProductsByCollections,
        setSelectedCategory,
        fetchNews
      } = this.props;
      const selectedCategory = find(
        list,
        (category) => category.category_id == product.category_id
      );
      console.log('_viewAll', selectedCategory, this.props);
      if (selectedCategory) {
        setSelectedCategory(selectedCategory);
      }
      fetchProductsByCollections(product.category_id, config._item.tag, this.page, index);
      onShowAll(selectedCategory, index);
    } else if (product.entity == "product") {
      product.product_id = product.entity_id;
      this.props.onViewProductScreen({ product });
    }
    console.log('on product screens', product);
  };

  renderItem = ({ item, index }) => {
    const { layout } = this.props.config;

    if (item === null) return <View key="post_" />;

    return (
      <HorizonLayout
        product={item}
        key={`post-${index}`}
        onViewPost={this.onViewProductScreen}
        layout={layout}
        config={this.props.config}
      />
    );
  };

  renderHeader = (listItems) => {
    const { showCategoriesScreen, config, theme } = this.props;
    return (
      <HHeader
        showCategoriesScreen={showCategoriesScreen}
        config={config}
        listItems={listItems}
        theme={theme}
        viewAll={this._viewAll}
      />
    );
  };

  renderLayout = (pro, index) => {
    const {
      config,
    } = this.props;
    const list = typeof collection != "undefined" && typeof collection.list != 'undefined' ? collection.list : this.defaultList;
    const isPaging = !!config.paging;

    return (
      <View
        style={[
          styles.flatWrap,
          // config.color && {
          //   backgroundColor: config.color,
          // },
          styles.boxShadow,
        ]}>
        {config.name && this.renderHeader()}
        <FlatList
          overScrollMode="never"
          contentContainerStyle={styles.flatlist}
          data={list}
          keyExtractor={(item, index) => `post__${index}`}
          renderItem={this.renderItem}
          showsHorizontalScrollIndicator={false}
          horizontal
          pagingEnabled={isPaging}
          onEndReached={false && this._nextPosts}
        />
      </View>
    )
  }

  render() {

    // console.log(JSON.stringify(this.props))
    const {
      onViewProductScreen,
      collection,
      config,
      products,
      _item,
      theme: {
        colors: { text },
      },
      news
    } = this.props;

    const { VerticalLayout } = AppConfig;


    const list = typeof collection != "undefined" && typeof collection.list != 'undefined' ? collection.list : this.defaultList;
    // console.log('collection', list)

    let listItems = config && config._item ? config._item : list;
    // console.log('Hlist ', config.layout, config._item, listItems);

    const isPaging = !!config.paging;

    switch (config.layout) {
      case Constants.Layout.circleCategory:
        return (
          <Categories
            config={config}
            categories={this.props.list}
            items={Config.HomeCategories}
            type={config.theme}
            onPress={this.showProductsByCategory}
          />
        );
      case Constants.Layout.Slider:
        return (
          <Slider config={config} onViewPost={this.onViewProductScreen} />
        );

      case Constants.Layout.BannerSlider:
        return (
          <BannerSlider data={list} config={config} onViewPost={this.onViewProductScreen} />
        );

      case Constants.Layout.doubleBanner:
        return (
          <DoubleBanner data={list} config={config} onViewPost={this.onTapDoubleBanner} />
        );

      case Constants.Layout.BannerImage:
        return (
          <BannerImage
            viewAll={this._viewAll}
            config={config}
            data={list}
            onViewPost={this.onViewProductScreen}
          />
        );
    }

    // return (
    //   products.length &&
    //   products.map((pro, index) => {
    //     this.renderLayout(pro, index);
    //   })
    // );

    // console.log('listItems.products', listItems.products)

    return (
      <View
        style={[
          styles.flatWrap,
          // config.color && {
          //   backgroundColor: config.color,
          // },
          styles.boxShadow
        ]}>
        {config.name && this.renderHeader(listItems)}
        <FlatList
          overScrollMode="never"
          contentContainerStyle={styles.flatlist}
          data={listItems && listItems.products ? listItems.products : list}
          keyExtractor={(item, index) => `post__${index}`}
          renderItem={this.renderItem}
          showsHorizontalScrollIndicator={false}
          horizontal
          pagingEnabled={isPaging}
          onEndReached={false && this._nextPosts}
        />
      </View>
    );
  }
}

export default withTheme(HorizonList);
