import React from "react";

import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import { Dimensions, StyleSheet, View, Text, TouchableOpacity } from 'react-native';

const { width: screenWidth } = Dimensions.get('window')

export default class Slider extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            entries: [
                {
                    image: 'https://previews.123rf.com/images/ylivdesign/ylivdesign1705/ylivdesign170502813/78258025-online-shopping-banner-horizontal-cartoon-style.jpg'
                },
                {
                    image: 'https://static2.bigstockphoto.com/3/6/2/large1500/263798284.jpg'
                },
            ]
        };
    }

    _renderItem({ item, index }, parallaxProps) {
        // console.log('_renderItem', item)
        // onPress={() => onPress({ ...item, circle: true, name: label })}
        // <TouchableOpacity
        // style={styles.container}
        // activeOpacity={0.75}>
        // </TouchableOpacity>

        return (
            <View style={styles.item}>
                <ParallaxImage
                    source={{ uri: item.image }}
                    containerStyle={styles.imageContainer}
                    style={styles.image}
                    parallaxFactor={0}
                    {...parallaxProps}
                />
                {item.title &&
                    <Text style={styles.title} numberOfLines={2}>
                        {item.title}
                    </Text>
                }
            </View>
        );
    }

    render() {
        const { data, onViewPost, config } = this.props;

        // onPress={onViewPost}

        console.log('_renderItem', config._item.info);

        let imageData = config && config._item && config._item.info ? config._item.info : this.state.entries;

        return (
            <View style={[styles.container]}>
                <Carousel
                    sliderWidth={screenWidth}
                    sliderHeight={screenWidth - 10}
                    itemWidth={screenWidth}
                    data={imageData}
                    loop={true}
                    autoplay={true}
                    autoplayInterval={5000}
                    renderItem={this._renderItem}
                    hasParallaxImages={true}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        marginBottom: 10
    },
    item: {
        width: screenWidth,
        height: screenWidth - 195, // 220 for higher device
        marginLeft: 0,
        marginRight: 5,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 0 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'contain',
    },
})
