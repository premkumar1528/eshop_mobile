/** @format */

import React, { PureComponent } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { Images, Styles, withTheme } from "@common";
import { WishListIcon, ImageCache, ProductPrice } from "@components";
import { getProductImage } from "@app/Omni";
import css from "./style";

class TwoColumn extends PureComponent {
  render() {
    const {
      title,
      product,
      viewPost,
      theme: {
        colors: { text },
      },
    } = this.props;

    let imageURI = '';

    if (product && product.images) {
      imageURI = product.images.length > 0
        ? getProductImage(product.images[0].src, Styles.width)
        : Images.PlaceHolderURL;
    } else {
      imageURI = product.thumb;
    }

    console.log('two columns render', this.props)

    // Use imagePanelTwo to reduce size
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={[css.panelTwo, css.boxShadow,]}
        onPress={viewPost}>
        <View style={css.imagePanelThreeContainer}>
          <ImageCache uri={imageURI} style={css.imagePanelThree} />
        </View>
        <Text numberOfLines={2} style={[css.nameTwo, { color: text }]}>
          {title}
        </Text>
        <ProductPrice product={product} />
        <WishListIcon product={product} />
      </TouchableOpacity>
    );
  }
}

export default withTheme(TwoColumn);
