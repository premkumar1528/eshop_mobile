/** @format */

import { StyleSheet } from "react-native";
import { Color, Constants } from "@common";

let margins = 5;

export default StyleSheet.create({
  container: {
    height: 74,
    //backgroundColor: "#fff",
    paddingBottom: 6,
  },
  labelContainer: {
    flex: 1,
    flexDirection: "row",
    // marginLeft: margins, // small dedvice
    marginRight: margins,
    justifyContent: "space-between",
    // paddingTop: 10,
  },
  label: {
    color: Color.blackTextDisable,
    fontSize: 12, // 10
    textAlign: "center",
    // paddingLeft: 10,
    fontFamily: Constants.fontHeader,
  },
  labelActive: {
    color: Color.Text,
    fontFamily: Constants.fontHeader,
  },
  indicatorContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 8,
  },
});
