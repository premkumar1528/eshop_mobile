/** @format */

import React, { PureComponent } from "react";
import { Text, View } from "react-native";
import { connect } from "react-redux";

import { Languages, withTheme, Tools } from "@common";
import styles from "./styles";

class ConfirmCheckout extends PureComponent {

  numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }


  getCurrecyFormatted = (price) => {
    const { symbol_left, symbol_right, symbol } = this.props.currency;
    if (symbol_right && symbol_right != null && symbol_right != '') {
      return price + "" + symbol_right;
    }
    if (symbol_left && symbol_left != null && symbol_left != '') {
      return symbol_left + '' + price;
    } else {
      return symbol + '' + price;
    }
  }

  render() {
    const {
      discountType,
      couponAmount,
      shippingMethod,
      totalPrice,
      _total,
      totalStyle,
      labelStyle,
      orderShipment,
      style,
    } = this.props;

    console.log('ConfirmCheckout', this.props);

    let shippingPrice = shippingMethod
      ? shippingMethod.cost //parseFloat((shippingMethod.cost).replace(',', ''))
      : 0;
    let discount =
      discountType == "percent"
        ? this.getExistCoupon() * totalPrice
        : this.getExistCoupon();
    let prices = totalPrice;
    if (totalPrice && typeof totalPrice === 'string') {
      prices = totalPrice.replace(',', '');
    }

    let total = totalPrice && typeof totalPrice === 'string' ?
      parseFloat(prices.replace('$', '')) +
      parseFloat(shippingPrice) : totalPrice;
    // - parseFloat(discount || 0);  // commented for coupon validation

    console.log('getCurrecyFormatted prices', this.getCurrecyFormatted(prices))

    const {
      theme: {
        colors: { text },
      },
    } = this.props;

    if (orderShipment) {
      shippingPrice = shippingMethod;
      total = _total;
    }

    console.log('total', totalPrice, shippingPrice, total)

    return (
      <View style={[styles.container, style]}>
        <View style={styles.row}>
          <Text style={[styles.label, labelStyle]}>{Languages.Subtotal}</Text>
          <Text style={[styles.value, { color: text }]}>
            {orderShipment ? totalPrice : this.getCurrecyFormatted(totalPrice)}
          </Text>
        </View>
        {couponAmount > 0 && (
          <View style={styles.row}>
            <Text style={[styles.label, labelStyle]}>{Languages.Discount}</Text>
            <Text style={[styles.value, { color: text }]}>
              {discountType == "percent"
                ? `${parseFloat(couponAmount)}%`
                : this.getCurrecyFormatted(couponAmount)}
            </Text>
          </View>
        )}
        {
          shippingMethod &&
          <View style={styles.row}>
            <Text style={[styles.label, labelStyle]}>{Languages.Shipping}</Text>
            <Text style={[styles.value, { color: text }]}>
              {orderShipment ? shippingPrice : this.getCurrecyFormatted(shippingPrice)}
            </Text>
          </View>
        }
        <View style={styles.divider} />
        <View style={styles.row}>
          <Text style={[styles.label, labelStyle]}>{Languages.Total}</Text>
          <Text style={[styles.value, { color: text }, totalStyle]}>
            {orderShipment ? total : this.getCurrecyFormatted(this.numberWithCommas(total))}
          </Text>
        </View>
      </View>
    );
  }

  getExistCoupon = () => {
    const { couponAmount, discountType } = this.props;
    if (discountType == "percent") {
      return couponAmount / 100.0;
    }
    return couponAmount;
  };
}

const mapStateToProps = ({ user, currency }) => {
  return {
    user,
    currency,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...ownProps,
    ...stateProps,
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(ConfirmCheckout));