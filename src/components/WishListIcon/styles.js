/** @format */

import React, {
  StyleSheet,
  Dimensions,
} from "react-native";

const { width, height, scale } = Dimensions.get("window"),
  vw = width / 100,
  vh = height / 100,
  vmin = Math.min(vw, vh),
  vmax = Math.max(vw, vh);

export default StyleSheet.create({
  imageButton: {
    width: 20,
    height: 20,
  },
  buttonStyle: {
    position: "absolute",
    right: 10,
    top: 7,
    zIndex: 9999,
  },
});
