/** @format */

import React, { PureComponent } from "react";
import Ionicons from "react-native-vector-icons/Ionicons";
import { StyleSheet, View, Text, Image, I18nManager } from "react-native";
import { LinearGradient } from "@expo";
import WPUserAPI from "@services/WPUserAPI";
import AppIntroSlider from "react-native-app-intro-slider";
import styles from "./styles";
import { Config } from "@common";
import { connect } from "react-redux";

class AppIntro extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {}
  }
  componentDidMount = async () => {

    const lang = await WPUserAPI.getLang(1, 'en');
    const cur = await WPUserAPI.getCurrency(1, 'en');
    if (lang && lang.success.message === "Success") {
      await this.props._languages(lang.languages)
    }
    if (cur && cur.success.message === "Success") {
      await this.props._currency(cur.currencies)
    }
    console.log(this.props);
  }
  _renderItem = (props) => {
    const { item, index, dimensions } = props;

    return <LinearGradient
      style={[
        styles.mainContent,
        {
          paddingTop: props.topSpacer,
          paddingBottom: props.bottomSpacer,
          width: props.width,
          height: props.height,
        },
      ]}
      colors={item.colors}
      start={{ x: 0, y: 0.1 }}
      end={{ x: 0.1, y: 1 }}>
      <Ionicons
        style={{ backgroundColor: "transparent" }}
        name={item.icon}
        size={200}
        color="white"
      />
      <View>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.text}>{item.text}</Text>
      </View>
    </LinearGradient>
  };

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name={
            I18nManager.isRTL ? "md-arrow-round-back" : "md-arrow-round-forward"
          }
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: "transparent" }}
        />
      </View>
    );
  };

  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: "transparent" }}
        />
      </View>
    );
  };

  render() {
    return (
      <AppIntroSlider
        slides={Config.intro}
        renderItem={this._renderItem}
        renderDoneButton={this._renderDoneButton}
        renderNextButton={this._renderNextButton}
        onDone={this.props.finishIntro}
      />
    );
  }
}

const mapStateToProps = ({ netInfo, user }) => ({ netInfo, languages: user.languages, currencys: user.currencys, });
const mapDispatchToProps = (dispatch) => {
  const { user_actions } = require("@redux/UserRedux");
  return {
    finishIntro: () => dispatch(user_actions.finishIntro()),
    _languages: (languages) => dispatch(user_actions.languages(languages)),
    _currency: (currency) => dispatch(user_actions.currency(currency)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppIntro);
