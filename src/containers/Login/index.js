/**
 * Created by Premji on 19/02/2017.
 *
 * @format
 */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Text, Image, TextInput, TouchableOpacity } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { connect } from "react-redux";

import { Icons, Languages, Styles, Config, withTheme } from "@common";
import { Icon, toast, warn, FacebookAPI } from "@app/Omni";
import { Spinner, ButtonIndex } from "@components";
import WPUserAPI from "@services/WPUserAPI";
import API from "@services/Common";
import styles from "./styles";
import AsyncStorage from "@react-native-community/async-storage";

class LoginScreen extends PureComponent {
  static propTypes = {
    user: PropTypes.object,
    isLogout: PropTypes.bool,
    onViewCartScreen: PropTypes.func,
    onViewHomeScreen: PropTypes.func,
    onViewSignUp: PropTypes.func,
    logout: PropTypes.func,
    navigation: PropTypes.object,
    onBack: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      isLoading: false,
      logInFB: false,
    };

    this.onUsernameEditHandle = (username) => this.setState({ username });
    this.onPasswordEditHandle = (password) => this.setState({ password });

    this.focusPassword = () => this.password && this.password.focus();
  }

  componentDidMount() {
    const { user, isLogout } = this.props;


    console.log('Login screen', user, isLogout)

    // check case after logout
    if (user && isLogout) {
      // this._handleLogout();
    }
  }

  // handle the logout screen and navigate to cart page if the new user login object exist
  componentWillReceiveProps(nextProps) {
    const { onViewCartScreen, user: oldUser, onViewHomeScreen } = this.props;

    const { user } = nextProps.user;
    const { params } = nextProps.navigation.state;

    console.log('componentWillReceiveProps', oldUser, nextProps);
    // check case after logout
    if (user) {
      if (nextProps.isLogout) {
        this._handleLogout();
        // } else if (!oldUser.user) {
      } else if (!oldUser.user) {
        console.log('componentWillReceiveProps', user, nextProps.isLogout);
        // check case after login
        this.setState({ isLoading: false });

        if (params && typeof params.onCart !== "undefined") {
          onViewCartScreen();
        } else {
          onViewHomeScreen();
        }

        const uName =
          user.lastname != null && user.firstname != null
            ? `${user.firstname} ${user.lastname}`
            : user.firstname;
        toast(`${Languages.welcomeBack} ${uName}.`);
        if (user && user.address && user.address.length > 0) {
          let user_address = user.address.filter(a => a.address_id === user.address_id);
          if (user_address.length > 0) {
            user_address[0].phone = user.telephone;
            user_address[0].email = user.email;
            // this.props.initAddresses(user_address[0]);
          }
        }
      }
    }
  }

  _handleLogout = async () => {
    const { logout, onViewHomeScreen } = this.props;
    await WPUserAPI.logOut();
    this.props.emptyCart();
    logout();
    // if (this.state.logInFB) {
    //   if (FacebookAPI.getAccessToken()) {
    //     FacebookAPI.logout();
    //   }
    // }
    let cartId = await API.createCartId();
    console.log('getSetCardId 1', cartId)
    // await WPUserAPI.logOut();
    if (typeof cartId.cart_id === 'string') {
      await AsyncStorage.setItem("@cartId", JSON.stringify(cartId.cart_id), async () => {
        console.log('cart Id set to local', cartId.cart_id);
        await WPUserAPI.getSetCardId(cartId.cart_id)
      });
    }
    onViewHomeScreen();
  };

  _onBack = () => {
    const { onBack, goBack } = this.props;
    if (onBack) {
      onBack();
    } else {
      goBack();
    }
  };

  onLoginPressHandle = async () => {
    const { login, netInfo } = this.props;

    if (!netInfo.isConnected) {
      return toast(Languages.noConnection);
    }

    this.setState({ isLoading: true });

    const { username, password } = this.state;

    // login the customer via Wordpress API and get the access token
    let json = await WPUserAPI.login(username.trim(), password);
    console.log('json', json)
    if (json === undefined) {
      this.stopAndToast(Languages.GetDataError);
    } else if (json.error) {
      // warn(json);
      this.stopAndToast(json.error.message);
    } else {
    this.setState({ isLoading: false });
      await WPUserAPI.getSetToken(json.customer_info.secret_key);
      json.customer_info.cart_id = json.cart_id;
      if (await WPUserAPI.getSetToken()) {
        login(json, json.customer_info.secret_key);
        const address = await API.getUserAddress();
        if (address && address.success) {
          json.customer_info.address = address.addresses;
          await this.props.initAllAddresses(address.addresses)
        }


        let cid = await WPUserAPI.getSetCardId();
        console.log('before merge', cid);
        let result = await API.mergeGuestToCustomerCart(cid, json.cart_id);
        console.log('cart merge', result);

        console.log('validate address', this.props)

        if (this.props.addresses && this.props.addresses.length > 0) {
          this.props.addresses.map(async (item) => {
            if (item.is_default) {
              item['telephone'] = json.customer_info.telephone;
              item['email'] = json.customer_info.email;
              console.log('initAddresses', item)
              await this.props.initAddresses(item);
              await this.props.selectAddress(item);
            }
          })
        }
      }

      await AsyncStorage.setItem("@cartId", JSON.stringify(json.cart_id), async () => {
        console.log('cart Id set to local', json.cart_id);
        await WPUserAPI.getSetCardId(json.cart_id)
      });
      let cartItems = await API.getCartItem();

      console.log('cartItems', cartItems)
      if (cartItems && cartItems.hasOwnProperty('products') && cartItems.products.length > 0) {
        this.props.loadCartItem(cartItems)
      }
      

      console.log('login props', this.props)
      // this._onBack();
    }
  };

  onSignUpHandle = () => {
    this.props.onViewSignUp();
  };

  onForGotHandle = () => {
    this.props.onViewForGot();
  };

  checkConnection = () => {
    const { netInfo } = this.props;
    if (!netInfo.isConnected) toast(Languages.noConnection);
    return netInfo.isConnected;
  };

  stopAndToast = (msg) => {
    toast(msg);
    this.setState({ isLoading: false });
  };

  render() {
    const { username, password, isLoading } = this.state;
    const {
      theme: {
        colors: { background, text, placeholder },
      },
    } = this.props;

    console.log('login page', this.props)

    return (
      <KeyboardAwareScrollView
        enableOnAndroid={false}
        style={{ backgroundColor: background }}
        contentContainerStyle={styles.container}>
        {isLoading ? <View style={{ zIndex: 99, position: 'absolute', top: 0, bottom: 0, backgroundColor: 'transparent', left: 0, right: 0 }}><Spinner mode="overlay" /></View> : null}
        <View style={styles.logoWrap}>
          <Image
            source={Config.LogoWithText}
            style={styles.logo}
            resizeMode="contain"
          />
        </View>
        <View style={styles.subContain}>
          <View style={styles.loginForm}>
            <View style={styles.inputWrap}>
              <Icon
                name={Icons.MaterialCommunityIcons.Email}
                size={Styles.IconSize.TextInput}
                color={text}
              />
              <TextInput
                style={styles.input(text)}
                underlineColorAndroid="transparent"
                placeholderTextColor={placeholder}
                ref={(comp) => (this.username = comp)}
                placeholder={Languages.UserOrEmail}
                keyboardType="email-address"
                onChangeText={this.onUsernameEditHandle}
                onSubmitEditing={this.focusPassword}
                returnKeyType="next"
                value={username}
              />
            </View>
            <View style={styles.inputWrap}>
              <Icon
                name={Icons.MaterialCommunityIcons.Lock}
                size={Styles.IconSize.TextInput}
                color={text}
              />
              <TextInput
                style={styles.input(text)}
                underlineColorAndroid="transparent"
                placeholderTextColor={placeholder}
                ref={(comp) => (this.password = comp)}
                placeholder={Languages.password}
                onChangeText={this.onPasswordEditHandle}
                secureTextEntry
                returnKeyType="go"
                value={password}
              />
            </View>
            <ButtonIndex
              text={Languages.Login.toUpperCase()}
              containerStyle={styles.loginButton}
              onPress={this.onLoginPressHandle}
            />
          </View>
          <View style={styles.separatorWrap}>
            <View style={styles.separator(text)} />
            <Text style={styles.separatorText(text)}>{Languages.Or}</Text>
            <View style={styles.separator(text)} />
          </View>

          <TouchableOpacity
            style={Styles.Common.ColumnCenter}
            onPress={this.onSignUpHandle}>
            <Text style={[styles.signUp, { color: text }]}>
              {Languages.DontHaveAccount}{" "}
              <Text style={styles.highlight}>{Languages.signup}</Text>
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Styles.Common.ColumnCenter}
            onPress={this.onForGotHandle}>
            <Text style={[styles.signUp, { color: text }]}>
              <Text style={styles.highlight}>{Languages.ForGotLink}</Text>
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

LoginScreen.propTypes = {
  netInfo: PropTypes.object,
  login: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
};

const mapStateToProps = ({ netInfo, user, carts, addresses }) => ({ netInfo, user, carts, addresses: addresses.list });

const mapDispatchToProps = (dispatch) => {
  const { user_actions } = require("@redux/UserRedux");
  const AddressRedux = require("@redux/AddressRedux");
  const CartRedux = require("@redux/CartRedux");

  return {
    login: (user, token) => dispatch(user_actions.login(user, token)),
    logout: () => dispatch(user_actions.logout()),
    emptyCart: () => CartRedux.actions.emptyCart(dispatch),
    loadCartItem: (data) => CartRedux.actions.loadCartItem(dispatch, data),
    initAddresses: (customerInfo) => {
      AddressRedux.actions.initAddresses(dispatch, customerInfo);
    },
    initAllAddresses: (address) => AddressRedux.actions.initAllAddresses(dispatch, address),
    selectAddress: (address) => {
      AddressRedux.actions.selectAddress(dispatch, address);
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(LoginScreen));
