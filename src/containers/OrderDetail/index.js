/** @format */

import React from "react";
import { ScrollView, View, Text } from "react-native";
import { connect } from "react-redux";

import Spinner from "@components/Spinner";
import API from '@services/Common';

import { warn } from "@app/Omni";
import { withTheme } from "@common";
import Footer from "./Footer";
import ShippingAddress from "./ShippingAddress";
import LineItemsAndPrice from "./LineItemsAndPrice";
import OrderStatus from "./OrderStatus";
import OrderNotes from "./OrderNotes";
import styles from "./styles";

class OrderDetail extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      _orderDetail: null,
      loader: false,
    };
  }


  async componentDidMount() {
    // this.props.getOrderNotes(this.props.order.order_id);

    this.setState({ loader: true });

    let data = await API.getOrderById(this.props.order_id);

    console.log('Order by Id', data)

    if (data && data.success) {
      this.setState({ _orderDetail: data.info, loader: false })
    } else this.setState({ loader: false })
  }

  render() {
    const { order, theme, orderNotes } = this.props;
    const { loader, _orderDetail } = this.state;

    console.log('Order Details', this.props)
    console.log('Order Details state', loader, _orderDetail);

    if (loader) {
      return <Spinner mode="overlay" />;
    }

    if (_orderDetail == undefined && _orderDetail == null) {
      return (
        <View><Text>No Order Details Found</Text></View>
      )
    }

    return (
      <ScrollView
        style={styles.container(theme.colors.background)}
        contentContainerStyle={styles.contentContainer}>
        <LineItemsAndPrice order={_orderDetail} theme={theme} />
        <OrderStatus order={_orderDetail} theme={theme} />
        <ShippingAddress shipping={_orderDetail} theme={theme} />
        {orderNotes && <OrderNotes orderNotes={orderNotes} theme={theme} />}
        {/* _orderDetail.status != "cancelled" && _orderDetail.status != "refunded" && <Footer order={_orderDetail} /> */}
      </ScrollView>
    );
  }

}

const mapStateToProps = ({ carts }, ownProps) => {
  const order = null; // ownProps.orderDetail; // carts.myOrders.find((o, i) => o.order_id === ownProps.id);

  return { carts, order, orderNotes: carts.orderNotes, order_id: ownProps.id };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/CartRedux");
  return {
    ...ownProps,
    ...stateProps,
    getOrderNotes: (orderId) => {
      actions.getOrderNotes(dispatch, orderId);
    },
  };
}

export default connect(mapStateToProps, null, mergeProps)(withTheme(OrderDetail));
