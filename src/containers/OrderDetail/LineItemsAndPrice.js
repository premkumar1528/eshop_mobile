/** @format */

import React from "react";
import { View, Text } from "react-native";

// import { log } from "@app/Omni";
import { Languages, Tools } from "@common";
import { ConfirmCheckout } from "@components";
import styles from "./styles";

export default class LineItemsAndPrice extends React.PureComponent {
  render() {
    const {
      order,
      theme: {
        colors: { text, primary },
      },
    } = this.props;

    return (
      <View>
        <View style={styles.header}>
          <Text style={styles.label(text)}>
            {Languages.OrderId}: #{order.invoice_no}
          </Text>
        </View>
        <View style={styles.itemContainer}>
          {order.products.map((o, i) => {
            return (
              <View key={i.toString()} style={styles.lineItem}>
                <Text
                  style={styles.name(text)}
                  numberOfLines={2}
                  ellipsizeMode="tail">
                  {o.name}
                </Text>
                <Text style={styles.text(text)}>{`x${o.quantity}`}</Text>
                <Text style={styles.text(text)}>
                  {o.total}
                </Text>
              </View>
            );
          })}
        </View>
        <ConfirmCheckout
          totalPrice={order.totals[0].text}
          shippingMethod={order.totals[1].text}
          _total={order.totals[2].text}
          orderShipment={true}
          style={{ margin: 0 }}
          totalStyle={{ color: primary, fontWeight: "bold" }}
          labelStyle={{ color: text, fontWeight: "bold" }}
        />
      </View>
    );
  }
}
