/**
 * Created by Premji on 03/03/2017.
 *
 * @format
 */

import React from "react";
import PropTypes from "prop-types";
import { View, Text, StyleSheet, Modal, ScrollView } from "react-native";
import { connect } from "react-redux";

import { Button } from "@components";
import { Styles, Color, Languages, withTheme } from "@common";
import { actions as FiltersActions } from "@redux/FilterRedux";
import Section from "./Section";

const sections = [
  { title: "Sub Category", type: "category", storeName: "category" },
  // { title: "Brands", type: "brand", storeName: "brands" },
  // { title: "Tags", type: "tag", storeName: "tags" },
  { title: "Price", type: "price", storeName: "price" },
];

class FilterPicker extends React.PureComponent {
  filters = {};

  constructor(props) {
    super(props);
    this.state = {
      filters: this.props._fils,
    };

  }

  componentDidMount = async () => {
    // const { selectedCategory } = this.props;
    // let data = await this.fetchFilters(selectedCategory.category_id);
    // console.log('filters page', this.props, this.filters, data);

    let filters = [];
    let paramFilter = [];
    // if (this.props.filters_section && this.props.filters_section.hasOwnProperty('filter') &&
    //   this.props.filters_section.filter.hasOwnProperty('filters') && this.props.filters_section.filter.filters.length > 0) {
    //   filters = this.props.filters_section.filter.filters;
    //   if (filters.length > 0) {
    //     filters.map((filter1, index1) => {
    //       paramFilter.filter.forEach((element, index) => {
    //         if (element.filter_id == filter1.filter_id) {
    //           element = filter1;
    //         }
    //       })
    //     })
    //   }
    // }
    // this.setState({ filters: paramFilter });
  }


  componentWillReceiveProps(prevProps, nextProps) {
    console.log('prevProps filter index', prevProps)
    console.log('nextProps filter index', nextProps)
    if (prevProps._fils.length != this.state.filters) {
      this.setState({ filters: prevProps._fils });
      this.filters = {};
    }
  }

  _onFilter = () => {
    const { closeModal } = this.props;

    console.log('calling update filter', this.filters)
    this.props.updateFilter(this.filters);
    closeModal();
  };

  _onChangeFilter = (type, selected) => {
    console.log('_onChangeFilter: ', type, selected, this.filters)
    let update_filter = this.filters && this.filters.hasOwnProperty('filter') && this.filters.filter.filters ? this.filters.filter.filters : [];
    let isPushed = true;
    if (update_filter.length > 0) {
      update_filter.forEach((data, index) => {
        if (data.filter_id == selected.filter_id) {
          data[index] = selected;
          isPushed = false;
        }
      });
      if (isPushed) {
        update_filter.push(selected);
      }
    } else update_filter.push(selected);

    this.filters = {
      ...this.filters,
      'filter': { filters: update_filter },
    };
  };

  render() {
    const { visible, closeModal, _fils, loading } = this.props;
    const {
      theme: {
        colors: { background, text },
      },
    } = this.props;

    const { filters } = this.state;

    console.log('filter check', _fils, loading)

    return (
      <Modal
        animationType="fade"
        transparent
        visible={visible}
        onRequestClose={closeModal}
        onShow={this.onShow}>
        <View style={styles.container}>
          <View style={[styles.subContainer, { backgroundColor: background }]}>
            <ScrollView>
              <View style={styles.titleWrap}>
                <Text style={[styles.title, { color: text }]}>
                  {Languages.Filter}
                </Text>
              </View>
              <View style={styles.filterContainer}>
                {filters.map((o, i) => {
                  return (
                    <Section
                      key={i.toString()}
                      item={o}
                      onChangeFilter={this._onChangeFilter}
                    />
                  );
                })}

                {
                  (filters.length === 0 && loading === false) && <Text style={{ textAlign: 'center' }}>No Filters Available</Text>
                }
              </View>
            </ScrollView>

            <View style={styles.row}>
              <Button
                text={Languages.Cancel}
                style={styles.cancelContainer}
                textStyle={styles.cancelText}
                onPress={closeModal}
              />
              <Button
                text={Languages.Select}
                style={styles.selectContainer}
                textStyle={styles.selectText}
                onPress={this._onFilter}
              />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    paddingTop: 0,
  },
  subContainer: {
    backgroundColor: Color.background,
    height: "100%",
    justifyContent: "space-between",
  },
  titleWrap: {
    ...Styles.Common.ColumnCenter,
    padding: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  title: {
    fontWeight: "500",
    color: Color.blackTextPrimary,
    fontSize: Styles.FontSize.medium,
  },
  contentContainer: {
    marginHorizontal: 20,
  },
  titleSection: {
    color: Color.blackTextPrimary,
    fontSize: Styles.FontSize.medium,
  },
  selectContainer: {
    padding: 15,
    backgroundColor: "rgba(0,145,234,1)",
    flex: 1,
    color: "rgba(0,0,0,1)",
  },
  selectText: {
    color: "white",
    fontSize: 14,
  },
  cancelContainer: {
    padding: 15,
    flex: 1,
    color: "rgba(0,0,0,1)",
  },
  cancelText: {
    color: "white",
    fontSize: 14,
  },
  row: {
    flexDirection: "row",
    alignItems: "flex-end",
  },
  filterContainer: {
    paddingHorizontal: 10,
    marginBottom: 20,
  },
});

FilterPicker.propTypes = {
  visible: PropTypes.bool,
  closeModal: PropTypes.func,
};
FilterPicker.defaultProps = {
  visible: false,
};

const mapStateToProps = (state) => {
  return {
    categories: state.categories,
    filters_section: state.filters,
    selectedCategory: state.categories.selectedCategory,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateFilter: (filters) => {
      dispatch(FiltersActions.updateFilter(filters));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(FilterPicker));
