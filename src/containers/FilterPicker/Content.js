/** @format */

import React from "react";
import { View, Text } from "react-native";
import { connect } from "react-redux";
import CheckBox from '@react-native-community/checkbox';

import { log } from "@app/Omni";
import { actions as BrandsActions } from "@redux/BrandsRedux";
import { actions as TagActions } from "@redux/TagRedux";
import styles from './styles';
import Item from "./Item";

const checkBoxProcess = (data) => {
  data.filter.forEach(data => {
    if(!(data.selected === true)) {
      data.selected = false;
    }
  });
  return data;
}

class Content extends React.PureComponent {
  state = { selected: this.props.selected, section: checkBoxProcess(this.props.section) };

  componentDidMount() {
    console.log('this.props.section', this.props.section)
    const { type, fetchBrands, fetchTags, list } = this.props;

    if (!list || (list && !list.length)) {
      switch (type) {
        case "brand": CheckBox
          fetchBrands();
          break;

        case "tag":
          fetchTags();
          break;

        default:
          break;
      }
    }
  }

  _onSelect = (item, type = {}, index) => {
    console.log('_onSelect', item, type, index)
    type['filter_group_id'] = item.filter_group_id;
    type['selected'] = !type.selected;
    let section = item;
    section.filter[index] = type;
    this.setState({ selected: type, section }, () => {
      this.props.onSelect(item.name, type);
    });
    this.forceUpdate();
  };

  render() {
    const { selected } = this.state;
    let { section } = this.state;
    let item = section && section.filter.length > 0 ? true : false;

    console.log('filter props', this.props, this.state);

    if (!item) {
      return (<View>No Filters are available...</View>)
    };

    const categories = [];
    section.filter.forEach((o, i) => {
      categories.push(
        <View key={i} style={styles.searchCon}>
          <CheckBox
            style={styles.searchCheckbox}
            onChange={() => this._onSelect(section, o, i)}
            value={o.selected}
          />
          <Text style={[styles.searchText]}>{o.name}</Text>
        </View>)
    });

    return (
      <View>{categories}</View>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  let list = [];

  if (ownProps.storeName === "category") {
    const { categories } = state;
    const subId = categories.selectedCategory.mainCategory
      ? categories.selectedCategory.mainCategory.id
      : categories.selectedCategory.parent;

    list = [
      categories.selectedCategory.mainCategory,
      ...categories.list.filter((category) => category.parent === subId),
    ];
  } else {
    list =
      ownProps && ownProps.storeName && state[ownProps.storeName]
        ? state[ownProps.storeName].list
        : [];
  }

  return {
    list,
    categories: state.categories.list,
    mainCategory: state.categories.selectedCategory.mainCategory
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchBrands: () => {
      dispatch(BrandsActions.fetchBrands());
    },
    fetchTags: () => {
      TagActions.fetchTags(dispatch);
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Content);
