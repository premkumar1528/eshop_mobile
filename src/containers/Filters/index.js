/** @format */

import React from "react";
import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import CheckBox from '@react-native-community/checkbox';
import { connect } from "react-redux";
import { ActionSheets } from "@components";
import { Languages, withTheme, Tools, Color, Images } from "@common";

import { ProductCatalog, ProductTags } from "@components";
import Slider from "react-native-fluid-slider";
import styles from "./styles";

class Filters extends React.PureComponent {
  constructor(props) {
    super(props);
    this.filter = {};
    this.state = {
      // scrollY: new Animated.Value(0),
      // expanded: true,
      sub: false,
      desc: false,
      value: 2000,
    };
    this.options = [
      {
        title: 'Sort by Name (A - Z)', display: 'Name (A - Z)', order: "ASC", orderby: "pd.name"
      },
      {
        title: 'Sort by Name (Z - A)', display: 'Name (Z - A)', order: "DESC", orderby: "pd.name"
      },
      {
        title: 'Sort by Price (low > high)', display: 'Price (low > high)', order: "ASC", orderby: "p.price"
      },
      {
        title: 'Sort by Price (high > low) ', display: 'Price (high > low)', order: "DESC", orderby: "p.price"
      },
      {
        title: 'Cancel', actionStyle: 'cancel', isCancel: true
      }
    ];
  }

  onSelectCheckbox = (value) => {
    this.setState({
      [value]: !this.state[value]
    }, () => {
      this.onSelectTag(value, this.state[value]);
    })
  }

  _find = (options, key) => {
    for (let i = 0; i < options.length; i++) {
      if (key.order && key.orderby) {
        if (options[i].order === key.order && options[i].orderby === key.orderby) {
          return options[i];
        }
      } else {
        return options[0];
      }
    }
  }

  render() {
    const { categories, tags } = this.props;

    const { sub, desc } = this.state;

    let valueDefault = this._find(this.options, this.filter.hasOwnProperty('sort') ? this.filter.sort : this.filter);
    console.log(this.filter, valueDefault)

    const {
      theme: {
        colors: { background, text },
        dark: isDark,
      },
    } = this.props;

    let title = '';
    if (this.filter.hasOwnProperty('sort') && this.filter.sort.hasOwnProperty('title')) {
      title = this.filter.sort.title
    }

    return (
      <ScrollView style={[styles.container, { backgroundColor: background }]}>
        <View style={styles.content}>
          <Text style={[styles.headerLabel, { color: text }]}>
            {Languages.Filters}
          </Text>

          <ProductCatalog
            categories={categories}
            onSelectCategory={this.onSelectCategory}
          />
          <View style={styles.searchCon}>
            <CheckBox
              style={styles.searchCheckbox}
              onValueChange={() => this.onSelectCheckbox('sub')}
              value={sub}
            />
            <Text style={[styles.searchText, { color: text }]}>Search in subcategories</Text>
          </View>
          <View style={styles.searchCon}>
            <CheckBox
              style={styles.searchCheckbox}
              onValueChange={() => this.onSelectCheckbox('desc')}
              value={desc}
            />
            <Text style={[styles.searchText, { color: text }]}>Search in product descriptions</Text>
          </View>
          {
            /*
                <ProductTags tags={tags} onSelectTag={this.onSelectTag} />
                <Text style={[styles.pricing, { color: text }]}>
                  {Languages.Pricing}
                </Text>
                <View style={styles.row}>
                  <Text style={styles.label}>{Tools.getCurrecyFormatted(0)}</Text>
                  <Text style={styles.value}>
                    {Tools.getCurrecyFormatted(this.state.value)}
                  </Text>
                  <Text style={styles.label}>{Tools.getCurrecyFormatted(4000)}</Text>
                </View>
                <View style={styles.slideWrap}>
                  <Slider
                    value={this.state.value}
                    onValueChange={this.onValueChange}
                    onSlidingComplete={(value) => {

                    }}
                    minimumTrackTintColor={Color.primary}
                    maximumTrackTintColor="#bdc2cc"
                    thumbTintColor={Color.primary}
                    minimumValue={0}
                    maximumValue={4000}
                  />
                </View>
            */
          }

          <TouchableOpacity
            onPress={() => { this.refs.picker.show() }}
            style={styles.iconAndTextWrap}>
            <Image
              source={Images.IconSort}
              style={[
                styles.iconStyle,
                styles.dark,
                isDark && { tintColor: "#fff" },
              ]}
            />
            <Text style={[styles.sortText, { color: text }]}>{Languages.SortItemBy}{title != '' ? ' - ' + title : ''}</Text>
          </TouchableOpacity>

          <ActionSheets
            ref="picker"
            titles={this.options}
            separateHeight={1}
            separateColor="#dddddd"
            defaultValue={valueDefault.title}
            backgroundColor="rgba(0, 0, 0, 0.3)"
            containerStyle={{ margin: 10, borderRadius: 5 }}
            onPress={(title) => {
              this.filter = { ...this.filter, sort: { order: title.order, title: title.display, orderby: title.orderby } };
              this.forceUpdate();
            }}
          />

          <TouchableOpacity style={styles.btnFilter} onPress={this.onFilter}>
            <Text style={styles.filterText}>{Languages.Filter}</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.btnClear} onPress={this.clearFilter}>
            <Text style={styles.clearFilter}>{Languages.ClearFilter}</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  } 

  onSelectCategory = (item) => {
    this.filter = { ...this.filter, category: item.id };
  };

  onSelectTag = (item, value) => {
    this.filter = { ...this.filter, [item]: value };
  };

  onValueChange = (value) => {
    this.setState({ value });
    this.filter = { ...this.filter, max_price: value };
  };

  onFilter = () => {
    this.props.navigation.state.params.onSearch(this.filter);
    this.props.onBack();
  };

  clearFilter = () => {
    this.props.navigation.state.params.onSearch({});
    this.props.onBack();
  };

  componentDidMount() {
    this.props.fetchTags();
  }
}

Filters.defaultProps = {
  tags: [],
};

const mapStateToProps = (state) => {
  return {
    categories: state.categories.list,
    tags: state.tags.list,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { netInfo } = stateProps;
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/TagRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchTags: () => {
      actions.fetchTags(dispatch);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(Filters));
