/** @format */

import React, { PureComponent } from "react";
import { View, ScrollView, Text, Switch, Share } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from "react-redux";

import {
  UserProfileHeader,
  UserProfileItem,
  ModalBox,
  CurrencyPicker,
} from "@components";
import { Languages, Color, Tools, Config, withTheme } from "@common";
import { getNotification, toast } from "@app/Omni";
import _ from "lodash";
import WPUserAPI from "@services/WPUserAPI";
import API from "@services/Common";
import styles from "./styles";

class UserProfile extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      pushNotification: false,
    };

    this.onCurrencyUpdate = this.onCurrencyUpdate.bind(this);
  }

  async componentDidMount() {
    let token = await WPUserAPI.getSetDeviceToken();
    if (!token) {
      await WPUserAPI.getSetDeviceToken(this.props.userProfile.user.secret_key);
    }
    this._handleSwitchNotification(true);
  }

  componentDidMount() {
    this._getNotificationStatus();
  }

  _getNotificationStatus = async () => {
    const notification = await getNotification();
    this.setState({ pushNotification: notification || false });
  };

  _share_application = async () => {
    console.log('share app clicked');
    try {
      const result = await Share.share({
        message:
          'The favorite Exlcart Application Available on Android - https://play.google.com/store/apps/details?id=com.adyas.exlcart1&hl=en And IOS.',
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
          toast('Thanks for Choosing Exlcart', 2000);
        } else {
          // shared
          // toast('Thanks for Choosing Exlcart', 2000);
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
        toast('Try not Sharing Exlcart to your friends', 2000);
      }
    } catch (error) {
      console.log(error.message);
    }
  }

  /**
   * TODO: refactor to config.js file
   */
  _getListItem = () => {
    const { currency, wishListTotal, userProfile, isDarkTheme } = this.props;
    const listItem = [...Config.ProfileSettings];
    const items = [];
    let index = 0;

    for (let i = 0; i < listItem.length; i++) {
      const item = listItem[i];
      if (item.label === "PushNotification") {
        item.icon = () => (
          <Switch
            onValueChange={this._handleSwitchNotification}
            value={this.state.pushNotification}
            tintColor={Color.blackDivide}
          />
        );
      }
      if (item.label === "DarkTheme") {
        item.icon = () => (
          <Switch
            onValueChange={this._onToggleDarkTheme}
            value={isDarkTheme}
            tintColor={Color.blackDivide}
          />
        );
      }
      if (item.label === "Currency") {
        item.value = currency.code;
      }
      if (item.label === "share") {
        item.value = '';
        item.shareAction = true;
      }

      if (item.label === "WishList") {
        items.push({
          ...item,
          label: `${Languages.WishList} (${wishListTotal})`,
        });
      } else {
        items.push({ ...item, label: Languages[item.label] });
      }
    }

    if (!userProfile.user) {
      index = _.findIndex(items, (item) => item.label == Languages.Address);
      if (index > -1) {
        items.splice(index, 1);
      }
    }

    if (!userProfile.user || Config.Affiliate.enable) {
      index = _.findIndex(items, (item) => item.label == Languages.MyOrder);
      if (index > -1) {
        items.splice(index, 1);
      }
    }
    return items;
  };

  _onToggleDarkTheme = () => {
    this.props.toggleDarkTheme();
  };

  _handleSwitchNotification = (value) => {
    AsyncStorage.setItem("@notification", JSON.stringify(value), () => {
      this.setState({
        pushNotification: value,
      });
    });
  };

  _handlePress = (item) => {
    const { navigation } = this.props;
    const { routeName, isActionSheet, shareAction } = item;

    console.log('clicked item details', item)

    if (routeName && !isActionSheet) {
      navigation.navigate(routeName, item.params);
    }

    if (isActionSheet) {
      this.currencyPicker.openModal();
    }

    if (shareAction) {
      this._share_application();
    }
  };

  onCurrencyUpdate = async (changeCurrency) => {
    await this.props.changeCurrency(changeCurrency);
    this.currencyPicker.closeModalLayout();

    const { navigation } = this.props;
    navigation.navigate("Default");
  }

  onLogout = async () => {
    const { logout, onViewHomeScreen } = this.props;
    await WPUserAPI.logOut();
    this.props.emptyCart();
    logout();
    let cartId = await API.createCartId();
    console.log('getSetCardId 1', cartId)
    // await WPUserAPI.logOut();
    if (typeof cartId.cart_id === 'string') {
      await AsyncStorage.setItem("@cartId", JSON.stringify(cartId.cart_id), async () => {
        console.log('cart Id set to local', cartId.cart_id);
        await WPUserAPI.getSetCardId(cartId.cart_id)
      });
    }
    onViewHomeScreen();
  }

  render() {
    const {
      userProfile,
      language,
      navigation,
      currency,
      changeCurrency,
    } = this.props;
    const user = userProfile.user || {};
    const name = Tools.getName(user);
    const listItem = this._getListItem();
    const {
      theme: {
        colors: { background },
        dark,
      },
    } = this.props;

    console.log('user profile list items', this.props)

    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        <ScrollView ref="scrollView">
          <UserProfileHeader
            onLogin={() => navigation.navigate("LoginScreen", { isLogout: false })}
            onLogout={this.onLogout}
            user={{
              ...user,
              name,
            }}
          />

          {userProfile.user && (
            <View
              style={[styles.profileSection(dark)]}>
              <Text style={styles.headerSection}>
                {Languages.AccountInformations.toUpperCase()}
              </Text>
              <UserProfileItem
                label={Languages.Name}
                onPress={this._handlePress}
                value={name}
              />
              <UserProfileItem label={Languages.Email} value={user.email} />
              {
                user.address && !!user.address.length &&
                <UserProfileItem label={Languages.Address} value={user.address[0].address_1 + ', ' + user.address[0].address_2 + ', ' + user.address[0].city} />
              }
            </View>
          )}

          <View
            style={[styles.profileSection(dark)]}>
            {listItem.map((item, index) => {
              return (
                item && (
                  <UserProfileItem
                    icon
                    key={index.toString()}
                    onPress={() => this._handlePress(item)}
                    {...item}
                  />
                )
              );
            })}
          </View>
        </ScrollView>

        <ModalBox ref={(c) => (this.currencyPicker = c)}>
          <CurrencyPicker currency={currency} Currencys={this.props.currencys} changeCurrency={this.onCurrencyUpdate} />
        </ModalBox>
      </View>
    );
  }
}

const mapStateToProps = ({ user, language, currency, wishList, app }) => ({
  userProfile: user,
  currencys: user.currencys,
  language,
  currency,
  wishListTotal: wishList.wishListItems.length,
  isDarkTheme: app.isDarkTheme,
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions: CurrencyActions } = require("@redux/CurrencyRedux");
  const { toggleDarkTheme } = require("@redux/AppRedux");
  const { user_actions } = require("@redux/UserRedux");
  const CartRedux = require("@redux/CartRedux");

  return {
    ...ownProps,
    ...stateProps,
    changeCurrency: (currency) =>
      CurrencyActions.changeCurrency(dispatch, currency),
    toggleDarkTheme: () => {
      dispatch(toggleDarkTheme());
    },
    logout: () => dispatch(user_actions.logout()),
    emptyCart: () => CartRedux.actions.emptyCart(dispatch),
  };
}

export default connect(
  mapStateToProps,
  null,
  mergeProps
)(withTheme(UserProfile));
