/**
 * Created by Luan on 11/23/2016.
 *
 * @format
 */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Text, View, TouchableOpacity, I18nManager, Image } from "react-native";
import { connect } from "react-redux";
import RNRestart from "react-native-restart";
import { RadioButtons } from "react-native-radio-buttons";

import { Color, Languages, Images, withTheme } from "@common";
import { Button } from "@components";
import { warn } from "@app/Omni";
import styles from "./styles";
import WPUserAPI from "@services/WPUserAPI";

@withTheme
class LanguagePicker extends PureComponent {
  static propTypes = {
    language: PropTypes.object,
    switchLanguage: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedOption: Languages.getLanguage(),
      isLoading: false,
      langs: [],
    };
  }

  _formatLanguage = (lang) => {
    let langs = [];
    lang.map((data) => {
      langs.push(data.code);
    });
    return langs;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.language.lang !== nextProps.language.lang) {
      // Enable for mode RTL
      setTimeout(() => {
        RNRestart.Restart();
      }, 1500);
    }
  }

  _handlePress = async () => {
    const { switchLanguage, switchRtl, language } = this.props;
    const { selectedOption } = this.state;
    if (selectedOption !== language.lang) {
      const isRtl = selectedOption === "ar";
      const language_id = selectedOption === "ar" ? '2' : '1';

      this.setState({ isLoading: true });

      // change RTL
      I18nManager.forceRTL(isRtl);

      let data = await WPUserAPI.getSetLocalData();
      await WPUserAPI.getSetLocalData({
        language_id: language_id,
        language_code: selectedOption,
        currency: data.currency,
      })

      switchLanguage({
        lang: selectedOption,
        rtl: isRtl,
        language_id: language_id,
      });
    }
  };

  render() {
    const {
      theme: {
        colors: { background, text, lineColor },
      },
      language
    } = this.props;

    console.log('LanguagePicker', this.props, this._formatLanguage(this.props.languages), Languages.getLanguage(), Languages.getAvailableLanguages());

    const renderOption = (option, selected, onSelect, index) => {
      let icon = null;
      let name = null;
      // require("@images/icons/icon-love.png")
      if (this.props.languages) {
        let data = this.props.languages && this.props.languages.filter(x => x.code == option);
        if (data.length) {
          icon = { uri: data[0].image };
          name = data[0].name;
        } else {
          icon = Images.IconUkFlag;
          name = "English";
        }
      } else {
        icon = Images.IconUkFlag;
        name = "English";
      }

      return (
        <TouchableOpacity
          onPress={onSelect}
          key={index}
          style={{
            padding: 10,
            backgroundColor: selected ? lineColor : background,
            flexDirection: "row",
            alignItems: "center",
            width: undefined,
          }}>
          {/* <Icon name={selected ? Icons.Ionicons.RatioOn : Icons.Ionicons.RatioOff} size={15}/> */}
          <Image source={icon} style={styles.icon} />
          <Text
            style={[
              selected
                ? { fontWeight: "bold", marginLeft: 10 }
                : { marginLeft: 10 },
              { color: text },
            ]}>
            {name}
          </Text>
        </TouchableOpacity>
      );
    };

    return (
      <View>
        <RadioButtons
          options={this._formatLanguage(this.props.languages)}
          onSelection={(selectedOption) => this.setState({ selectedOption })}
          selectedOption={this.state.selectedOption}
          renderOption={renderOption}
          renderContainer={(optionNodes) => (
            <View style={{ margin: 10 }}>{optionNodes}</View>
          )}
        />
        <View style={styles.buttonContainer}>
          <Button
            text={Languages.SwitchLanguage}
            style={styles.button}
            textStyle={styles.buttonText}
            isLoading={this.state.isLoading}
            onPress={this._handlePress}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({ language: state.language, languages: state.user.languages });

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/LangRedux");
  return {
    ...ownProps,
    ...stateProps,
    switchLanguage: (language) => actions.switchLanguage(dispatch, language),
    switchRtl: (rtl) => actions.switchRtl(dispatch, rtl),
  };
}

module.exports = connect(
  mapStateToProps,
  undefined,
  mergeProps
)(LanguagePicker);
