/**
 * Created by Premji on 27/02/2017.
 *
 * @format
 */

import React, { PureComponent } from "react";
import {
  View,
  Text,
  Image,
  RefreshControl,
  ScrollView,
  Animated,
  FlatList,
  BackHandler
} from "react-native";
import { connect } from "react-redux";
import { isObject } from "lodash";
import API from '@services/Common';
import EventEmitter from "@services/AppEventEmitter";

import { Languages, withTheme, Images } from "@common";
import { Timer, toast, BlockTimer } from "@app/Omni";
import LogoSpinner from "@components/LogoSpinner";
import Empty from "@components/Empty";
import { DisplayMode } from "@redux/CategoryRedux";
import FilterPicker from "@containers/FilterPicker";
import ProductRow from "./ProductRow";
import ControlBar from "./ControlBar";
import styles from "./styles";
import { TouchableScale } from "@components";

class CategoryScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      scrollY: new Animated.Value(0),
      loadingBuffer: true,
      modalVisible: false,
      displayControlBar: true,
      Statefilters: [],
      filterLoading: null,
    };
    this.pageNumber = 1;
    this.scrollHeights = { imageContainer: 132, listItem: 38, total: 0 };

    this.renderList = this.renderList.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderScrollComponent = this.renderScrollComponent.bind(this);
    this.onRowClickHandle = this.onRowClickHandle.bind(this);
    this.onEndReached = this.onEndReached.bind(this);
    this.onRefreshHandle = this.onRefreshHandle.bind(this);
    this.onListViewScroll = this.onListViewScroll.bind(this);

    this.openCategoryPicker = () => this.setState({ modalVisible: true });
    this.closeCategoryPicker = () => this.setState({ modalVisible: false });
  }

  componentDidMount() {
    Timer.setTimeout(() => this.setState({ loadingBuffer: false }), 1000);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    EventEmitter.addListener('cat-back-button', () => this.handleBackPress());

    console.log('CategoryScreen props', this.props);


    const {
      fetchProductsByCategoryId,
      clearProducts,
      selectedCategory,
      setPrevCategory,
    } = this.props;
    setPrevCategory([selectedCategory]);
    this.fetchFilters(selectedCategory.category_id);

    clearProducts();
    if (selectedCategory) {
      fetchProductsByCategoryId(
        selectedCategory.category_id,
        this.pageNumber++
      );
    }

  }

  handleBackPress = () => {
    console.log('Back Press', this.props);
    const { prevCategory, setSelectedCategory, clearProducts } = this.props;

    if (prevCategory.length > 1) {
      this.pageNumber = 1;
      clearProducts();
      if (this.cleaningNavs(prevCategory, false, false)) {
        console.log('navigate to setSelectedCat', prevCategory[0]);
        setSelectedCategory(prevCategory[0], this.pageNumber);
      }
    } else {
      this.props.navigation.goBack(null);
    }
    return true;
  }


  componentWillUnmount() {
    this.backHandler.remove();
  }

  componentWillReceiveProps(nextProps) {
    const props = this.props;
    const { error } = nextProps.products;
    if (error) toast(error);

    console.log('nextProps.filters', props.filters, nextProps.filters);

    // nextProps.filters
    // { category: null, brand: null, tag: null, price: 2000, dsfdsf: { … } }
    // category: nullbrand: nulltag: nullprice: 2000
    // dsfdsf: { filter_id: "3", name: "dsfdsf (0)", selected: true, filter_group_id: "1" } 
    // filter_id: "3" ,  name: "dsfdsf (0)"selected: true filter_group_id: "1"


    if (nextProps.filters !== this.newFilters) {
      console.log('Appliying new filteers', this.newFilters)
      this.newFilters = nextProps.filters; // this._getFilterId(nextProps.filters);

      this.pageNumber = 1;
      props.clearProducts();
      props.fetchProductsByCategoryId(
        props.selectedCategory.category_id,
        this.pageNumber++,
        20,
        this.newFilters
      );
    }

    if (props.selectedCategory != nextProps.selectedCategory) {
      this.fetchFilters(nextProps.selectedCategory.category_id);
      this.pageNumber = 1;
      props.clearProducts();
      props.fetchProductsByCategoryId(
        nextProps.selectedCategory.category_id,
        this.pageNumber++
      );
    }

    if (
      nextProps.products &&
      nextProps.products.listAll &&
      typeof nextProps.products.listAll.children !== 'undefined' && nextProps.products.listAll.children.length > 0
    ) {
      let childs = nextProps.products.listAll.children.length;
      this.scrollHeights.total =
        this.scrollHeights.listItem * childs +
        this.scrollHeights.imageContainer;
    }
  }

  _getFilterId = filters => {
    console.log('_getFilterId', filters)
    let newFilters = {};
    Object.keys(filters).forEach(key => {
      const value = filters[key];
      if (value) {
        newFilters = {
          ...newFilters,
          [key]: isObject(value) ? value.id || value.term_id : value
        };
      }
    });
    // warn(newFilters);
    if (newFilters.price) {
      newFilters.max_price = newFilters.price;
      delete newFilters.price;
    }
    if (!newFilters.category) {
      newFilters.category = this.props.selectedCategory.category_id;
    }
    return newFilters;
  };

  fetchFilters = async (catId) => {
    this.setState({ filterLoading: true })
    let _filters = await API.getFilters(catId);
    console.log('_filters', _filters);
    if (_filters.hasOwnProperty('error')) {
      toast(_filters.error.message, 2000)
    } else {
      // toast('Filter Fetch ' + _filters.success.message, 2000)
      this.setState({ Statefilters: _filters.filter_groups, filterLoading: false })
    };
  }

  render() {
    const { modalVisible, loadingBuffer, displayControlBar, Statefilters, filterLoading } = this.state;
    const {
      products,
      selectedCategory,
      filters,
      fetchProductsByCategoryId
    } = this.props;

    console.log('products.list.products', this.props)

    const {
      theme: {
        colors: { background }
      }
    } = this.props;

    if (!selectedCategory) return null;

    if (products.error) {
      return <Empty text={products.error} />;
    }

    if (loadingBuffer) {
      return <LogoSpinner fullStretch />;
    }

    const min = this.scrollHeights.total - this.scrollHeights.total * 2,
      max = this.scrollHeights.total * 2;

    const marginControlBar = this.state.scrollY.interpolate({
      inputRange: [0, max], // inputRange: [-100, 0, 40, 50],
      outputRange: [0, min], // outputRange: [0, 0, -50, -50],
      extrapolate: "clamp"
    });

    const name =
      (filters && filters.category && filters.category.name) ||
      selectedCategory.name;

    return (
      <View style={[styles.container, { backgroundColor: 'background' }]}>
        <Animated.View style={{ marginTop: 0 }}>
          {this.renderChildrens(products.list ? products.list : {})}
          <ControlBar
            openCategoryPicker={this.openCategoryPicker}
            isVisible={displayControlBar}
            fetchProductsByCategoryId={fetchProductsByCategoryId}
            name={name}
          />
        </Animated.View>

        {this.renderList(
          typeof products.list.products !== 'undefined' && products.list.products.length
            ? products.list.products
            : false
        )}
        <FilterPicker
          closeModal={this.closeCategoryPicker}
          visible={modalVisible}
          _fils={Statefilters}
          loading={filterLoading}
        />
      </View>
    );
  }

  ListEmpty = () => {
    const { products } = this.props;
    return (
      // View to show when list is empty
      products.isFetching === false &&
      <View style={styles.MainContainer}>
        <Text style={{ textAlign: "center" }}>Oops!, No Product Found</Text>
      </View>
    );
  };

  renderList = data => {
    const { products, displayMode } = this.props;
    const isCardMode = displayMode == DisplayMode.CardMode;

    const isListMode =
      displayMode === DisplayMode.ListMode ||
      displayMode === DisplayMode.CardMode;


    console.log('loading test', isCardMode ? false : products.isFetching)

    return (
      <FlatList
        keyExtractor={(item, index) => `${item.product_id}`}
        data={data}
        renderItem={this.renderRow}
        enableEmptySections
        onEndReached={this.onEndReached}
        columnWrapperStyle={{ flexDirection: isListMode ? 'column' : 'row' }}
        numColumns={2}
        style={{ marginBottom: -200 }}
        refreshControl={
          <RefreshControl
            refreshing={isCardMode ? false : products.isFetching}
            onRefresh={this.onRefreshHandle}
          />
        }
        contentContainerStyle={styles.listView}
        initialListSize={6}
        ListEmptyComponent={this.ListEmpty}
        pageSize={6}
        renderScrollComponent={this.renderScrollComponent}
      />
    );
  };

  renderChildrens = data => {
    const { products, displayMode } = this.props;
    const isCardMode = displayMode == DisplayMode.CardMode;
    let childs = data && data.children ? data.children : [];
    // http://renting.exlcart.in/image/cache/catalog/Banners/banner2-1000x500-1000x500.jpg
    // console.log('childs data', childs)
    // 
    return childs && childs.length ? (
      <View style={styles.parentContainer}>
        <Text style={styles.subCats}>Sub Categories</Text>
        <ScrollView
          horizontal
          style={styles.parentListContainer}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
        >
          {childs.map((child, index) => (
            <TouchableScale
              style={styles.parentLink}
              key={`${index}img`}
              onPress={() => this.onColumnClickHandle(child)}
            >
              <View style={styles.parentImageContainer}>
                {
                  child.image === null ? <Image style={styles.parentImage} source={Images.defaultImage} /> : <Image style={styles.parentImage} source={{ uri: child.image }} />
                }
              </View>
              <Text style={styles.parentLinkText}>{child.name}</Text>
            </TouchableScale>
          ))}
        </ScrollView>
      </View>
    ) : null;
  };

  renderRow = product => {
    const { displayMode } = this.props;
    const onPress = () => this.onRowClickHandle(product.item);
    const isInWishList =
      this.props.wishListItems.find(
        item => item.product.product_id == product.product_id
      ) != undefined;

    // console.log('rendering products', displayMode, product)

    return (
      <ProductRow
        product={product.item}
        onPress={onPress}
        displayMode={displayMode}
        wishListItems={this.props.wishListItems}
        isInWishList={isInWishList}
        addToWishList={this.addToWishList}
        removeWishListItem={this.removeWishListItem}
      />
    );
  };

  renderScrollComponent = props => {
    const { displayMode } = this.props;
    const mergeOnScroll = event => {
      props.onScroll(event);
      this.onListViewScroll(event);
    };

    if (displayMode == DisplayMode.CardMode) {
      return (
        <ScrollView
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          props
          {...props}
          onScroll={mergeOnScroll}
        />
      );
    }

    return (
      <ScrollView
        props
        {...props}
        scrollEventThrottle={16}
        onScroll={mergeOnScroll}
      />
    );
  };

  addToWishList = product => {
    this.props.addWishListItem(product);
  };

  removeWishListItem = product => {
    this.props.removeWishListItem(product);
  };

  onRowClickHandle = product => {
    BlockTimer.execute(() => {
      this.props.onViewProductScreen({ product });
    }, 500);
  };

  cleaningNavs = (prev, selected, type) => {
    const { setPrevCategory } = this.props;

    console.log('cleaningNavs', prev, selected, type);

    let current = prev;
    if (type) {
      current.push(selected);
    } else {
      current.pop();
    }
    setPrevCategory(current);
    return true;
  }

  onColumnClickHandle = product => {
    const { setSelectedCategory, setPrevCategory, clearProducts, prevCategory, selectedCategory } = this.props;
    this.pageNumber = 1;
    clearProducts();
    if (product) {
      console.log('on column click', product, this.pageNumber)
      if (this.cleaningNavs(prevCategory, product, true)) {
        setSelectedCategory(product, this.pageNumber);
      }
    }
  };

  onEndReached = () => {
    console.log('onEndReached')
    const {
      products,
      fetchProductsByCategoryId,
      selectedCategory
    } = this.props;
    if (!products.isFetching && products.stillFetch) {
      if (this.newFilters) {
        fetchProductsByCategoryId(
          selectedCategory.category_id,
          this.pageNumber++,
          20,
          this.newFilters
        );
      } else {
        fetchProductsByCategoryId(selectedCategory.category_id, this.pageNumber++);
      }
    }
  };

  onRefreshHandle = () => {
    const {
      fetchProductsByCategoryId,
      clearProducts,
      selectedCategory
    } = this.props;
    this.pageNumber = 1;
    clearProducts();
    fetchProductsByCategoryId(
      selectedCategory.category_id,
      this.pageNumber++,
      20,
      this.newFilters
    );
  };

  onListViewScroll(event) {
    this.state.scrollY.setValue(event.nativeEvent.contentOffset.y);
  }
}

const mapStateToProps = state => {
  return {
    selectedCategory: state.categories.selectedCategory,
    prevCategory: state.categories.previousSelectedCats,
    netInfo: state.netInfo,
    displayMode: state.categories.displayMode,
    products: state.products,
    wishListItems: state.wishList.wishListItems,
    filters: state.filters
  };
};

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { netInfo } = stateProps;
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/ProductRedux");
  const catRedux = require("@redux/CategoryRedux");
  const WishListRedux = require("@redux/WishListRedux");
  const prods = stateProps.products ? stateProps.products.list : stateProps;
  // console.log('stateProps', stateProps.products ? stateProps.products.list : stateProps)
  return {
    ...ownProps,
    ...stateProps,
    fetchProductsByCategoryId: (
      categoryId,
      page,
      per_page = 20,
      filters = {}
    ) => {
      if (!netInfo.isConnected) return toast(Languages.noConnection);
      actions.fetchProductsByCategoryId(
        dispatch,
        categoryId,
        per_page,
        page,
        filters,
        prods
      );
    },
    setSelectedCategory: (category) => dispatch(catRedux.actions.setSelectedCategory(category)),
    setPrevCategory: (category) => dispatch(catRedux.actions.setPrevCategory(category)),
    clearProducts: () => dispatch(actions.clearProducts()),
    addWishListItem: product => {
      WishListRedux.actions.addWishListItem(dispatch, product, null);
    },
    removeWishListItem: (product, variation) => {
      WishListRedux.actions.removeWishListItem(dispatch, product, null);
    }
  };
};

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(CategoryScreen));
