import { StyleSheet } from "react-native";
import { Color, Styles, Constants } from "@common";

const styles = StyleSheet.create({
  //main
  listView: {
    alignItems: "flex-start",
    paddingBottom: Styles.navBarHeight + 10,
  },
  container: {
    flexGrow: 1,
    backgroundColor: Color.background,
  },
  parentInnerContainer: {
    flexGrow: 1,
    width: Styles.width,
    height: 65,
  },

  parentContainer: {
    width: Styles.width / 2,
  },
  subCats: {
    color: Color.Text,
    fontSize: 16,
    paddingLeft: 15,
    marginBottom: 5,
    fontWeight:'bold'
  },
  parentImageContainer: {
    width: 60,
    height: 50,
    resizeMode: 'contain',
    borderColor: '#f2f2f2',
    borderWidth: 1,
    padding: 5,
    marginBottom: 5,
    backgroundColor: '#fff',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    elevation: 2,
  },
  parentImage: {
    width: '100%',
    height: '100%',
  },
  parentListContainer: {
    flexDirection: 'row',
    width: Styles.width,
  },
  parentLink: {
    backgroundColor: '#fff',
    padding: 10,
    marginTop: 0,
    marginBottom: 0,
    marginHorizontal: 5,
    alignItems: 'center',
  },
  parentLinkText: {
    fontSize: 12,
    fontWeight: 'bold',
  },


  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    width: Styles.width,
    marginTop: 30,
  },
  //ProductRows
  container_product: {
    backgroundColor: "white",
    paddingBottom: 10,
    marginHorizontal: Styles.width / 20,
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#eee',
  },
  container_list: {
    width: Styles.width * 0.9,
    marginLeft: Styles.width * 0.05,
    marginRight: Styles.width * 0.05,
    marginTop: Styles.width * 0.05,

  },
  container_grid: {
    width: (Styles.width * 0.9) / 1.94,
    marginLeft: 8,
    marginRight: 0,
    marginTop: 8,
  },
  image: {
    marginBottom: 8,
  },
  image_list: {
    width: Styles.width * 0.9 - 2,
    height: Styles.width * 0.9,

  },
  image_grid: {
    width: Styles.width * 0.48 - 9,
    height: Styles.width * 0.45 * Styles.thumbnailRatio,
    resizeMode: "contain",
  },
  text_list: {
    color: Color.black,
    fontSize: Styles.FontSize.medium,
    fontFamily: Constants.fontFamily,
  },
  text_grid: {
    color: Color.black,
    fontSize: Styles.FontSize.small,
    fontFamily: Constants.fontFamily,
    alignItems: 'center',
    textAlign: 'center',
  },
  textRating: {
    fontSize: Styles.FontSize.small,
  },
  price_wrapper: {
    ...Styles.Common.Row,
    top: 0,
  },
  cardWraper: {
    flexDirection: "column",
  },
  sale_price: {
    textDecorationLine: "line-through",
    color: Color.blackTextDisable,
    marginLeft: 0,
    marginRight: 0,
    fontSize: Styles.FontSize.small,
  },
  cardPriceSale: {
    fontSize: 15,
    marginTop: 2,
    fontFamily: Constants.fontFamily,
  },
  price: {
    color: Color.black,
    fontSize: Styles.FontSize.medium,
  },
  saleWrap: {
    zIndex: 1000,
    position: 'absolute',
    right: 2,
    bottom: 20,
    borderRadius: 5,
    backgroundColor: Color.primary,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 3,
    // marginLeft: 5,
  },
  sale_off: {
    color: Color.lightTextPrimary,
    fontSize: Styles.FontSize.small,
  },
  cardText: {
    fontSize: 20,
    textAlign: "center",
  },
  cardPrice: {
    fontSize: 18,
    marginBottom: 8,
    fontFamily: Constants.fontFamily,
  },
  btnWishList: {
    position: "absolute",
    top: 5,
    right: 5,
    backgroundColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    width: 30,
    height: 30,
  },
});

export default styles;