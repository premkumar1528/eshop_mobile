/**
 * Created by Premji on 01/03/2017.
 *
 * @format
 */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Switch,
  LayoutAnimation,
  I18nManager,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { WooWorker } from "api-ecommerce";
import { connect } from "react-redux";

import { Styles, Languages, Color, withTheme } from "@common";
import { toast, error, Validate } from "@app/Omni";
import Button from "@components/Button";
import Spinner from "@components/Spinner";
import WPUserAPI from "@services/WPUserAPI";

class ForgotPasswordScreen extends Component {
  constructor(props) {
    super(props);

    let state = {
      email: "",
      isLoading: false,
    };

    this.state = state;

    this.onEmailEditHandle = (email) => this.setState({ email });
    this.focusEmail = () => this.email && this.email.focus();
  }

  shouldComponentUpdate() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    return true;
  }

  onForgetPassHandle = async () => {
    console.log('Api hit', user)
    const { login, netInfo } = this.props;
    if (!netInfo.isConnected) return toast(Languages.noConnection);

    const {
      email,
      isLoading,
    } = this.state;
    if (isLoading) return;
    this.setState({ isLoading: true });

    const _error = this.validateForm();
    if (_error) return this.stopAndToast(_error);

    const user = {
      email,
    };
    console.log('Api hit', user)
    const json = await WPUserAPI.forgotPassword(user);
    console.log('Api result', json)
    console.log('props', this.props)
    if (json === undefined) {
      return this.stopAndToast("Server don't response correctly");
    } else if (json.error) {
      return this.stopAndToast(json.error.message);
    }
    // const customer = await WooWorker.getCustomerById(json.user_id);
    if (json.success) {
      this.setState({ isLoading: false });
      console.log('forgot pass', json);
      toast('New password has been sent to your e-mail address', 2000);
      // login(customer, json.cookie);
    } else {
      toast("User ID doesn't match, please try again.", 2000);
    }
  };

  validateForm = () => {
    const {
      email,
    } = this.state;
    if (
      Validate.isEmpty(
        email,
      )
    ) {
      // check empty
      return "Please enter the Email-Id";
    } else if (!Validate.isEmail(email)) {
      return "Email is Incorrect";
    }
    return undefined;
  };

  stopAndToast = (msg) => {
    toast(msg, 2000);
    error(msg);
    this.setState({ isLoading: false });
  };

  render() {

    const {
      email,
      isLoading,
    } = this.state;
    const {
      theme: {
        colors: { background, text, placeholder },
      },
    } = this.props;

    const params = this.props.params;
    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          enableOnAndroid>
          <View style={styles.formContainer}>
            {/* <Text style={[styles.label, { color: text }]}>
              {Languages.profileDetail}
            </Text> */}

            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.email = comp)}
              placeholder={Languages.email}
              onChangeText={this.onEmailEditHandle}
              onSubmitEditing={this.onForgetPassHandle}
              keyboardType="email-address"
              returnKeyType={"done"}
              value={email}
              placeholderTextColor={placeholder}
            />
            <Button
              containerStyle={styles.signUpButton}
              text={Languages.ForgotPasswordButtonTxt}
              onPress={this.onForgetPassHandle}
            />
          </View>
          {isLoading ? <Spinner mode="overlay" /> : null}
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    padding: Styles.width * 0.1,
  },
  label: {
    fontWeight: "bold",
    fontSize: Styles.FontSize.medium,
    color: Color.blackTextPrimary,
    marginTop: 20,
  },

  input: (text) => ({
    borderBottomWidth: 1,
    borderColor: text,
    height: 40,
    marginTop: 10,
    padding: 0,
    margin: 0,
    // flex: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: text,
  }),
  signUpButton: {
    marginTop: 20,
    backgroundColor: Color.primary,
    borderRadius: 5,
    elevation: 1,
  },
  switchWrap: {
    ...Styles.Common.RowCenterLeft,
    marginTop: 10,
  },
  text: {
    marginLeft: 10,
    color: Color.blackTextSecondary,
  },
});

const mapStateToProps = (state) => {
  return {
    netInfo: state.netInfo,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { user_actions } = require("@redux/UserRedux");
  return {
    fotGotPass: (user, token) => dispatch(user_actions.fotGotPass(user, token)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(ForgotPasswordScreen));
