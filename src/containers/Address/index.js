/** @format */

import React, { PureComponent } from "react";
import { FlatList, View, Text, Alert, RefreshControl } from "react-native";
import _ from "lodash";
import { connect } from "react-redux";
import { AddressItem } from "@components";

import { toast } from "@app/Omni";

import { withTheme } from "@common";
import API from "@services/Common";
import styles from './styles';

class Address extends PureComponent {
  state = {
    refreshing: false
  }

  componentDidMount = async () => {
    this.props.fetchAllCountries();
    this._onRefresh();
  }

  _onRefresh = async (log) => {
    console.log('on refresh the view', this.props.user);
    this.setState({ refreshing: true })
    const address = await API.getUserAddress();
    console.log('address', address)
    if (address && address.success) {
      this.props.initAllAddresses(address.addresses)
    }

    if (this.props.list && this.props.list.length > 0) {
      this.props.list.map(item => {
        if (item.is_default) {
          this.props.selectAddress(item);
        }
      })
    }

    this.setState({ refreshing: false })
  }

  ListEmpty = () => {
    return (
      //View to show when list is empty
      <View style={styles.noData}>
        <Text style={{ textAlign: 'center' }}>No Address Found...</Text>
      </View>
    );
  };

  editAddress = (item) => {
    const { navigate } = this.props.navigation;
    console.log('edit item', item);
    navigate("AddAddress", { edit: item })
  }

  render() {
    const { list, reload, selectedAddress } = this.props;
    const {
      theme: {
        colors: { background, text },
      },
    } = this.props;

    console.log(this.props)

    return (
      <FlatList
        overScrollMode="never"
        style={{ backgroundColor: background }}
        extraData={this.props}
        keyExtractor={(item, index) => `${index}`}
        data={list}
        ListEmptyComponent={this.ListEmpty}
        refreshControl={
          <RefreshControl
            colors={["#9Bd35A", "#689F38"]}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh.bind(this)}
          />
        }
        renderItem={({ item, index }) => (
          <AddressItem
            onPress={() => this.selectAddress(item)}
            edit={() => this.editAddress(item)}
            selected={_.isEqual(item, selectedAddress)}
            item={item}
            onRemove={() => this.removeAddress(index)}
          />
        )}
      />
    );
  }

  removeAddress = (index) => {

    try {
      Alert.alert(
        'Confirm Delete',
        'Do you want to Remove this Address?',
        [
          {
            text: 'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Remove', onPress: async () => {
              const item = this.props.list[index];
              const res = await API.removeAddress({ address_id: item.address_id });
              console.log('responce', res)
              if (res && res.success) {
                this.props.removeAddress(index);
                toast(res.success.message, 2000);
              } else if (res && res.error) {
                toast(res.error.message, 2000);
              }
            }
          },
        ],
        { cancelable: false },
      );

    } catch (error) {
      this.props.removeAddress(index);
    }
  };

  selectAddress = async (item) => {
    console.log('selected', item);
    let udpateItem = item;
    udpateItem.default = 1;
    let json = await API.updateUserAddress(udpateItem);
    if (json.success) {
      toast(json.success.message)
      this.props.selectAddress(udpateItem);
    } else if (json.error) {
      toast(json.error.message)
    } else {
      toast('Some thing went wrong. Please try again after sometimes')
    }
  };
}

Address.defaultProps = {
  list: [],
  selectedAddress: {},
};

const mapStateToProps = (state) => {
  return {
    list: state.addresses.list,
    user: state.user,
    reload: state.addresses.reload,
    selectedAddress: state.addresses.selectedAddress,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/AddressRedux");
  const CountryRedux = require("@redux/CountryRedux");

  return {
    ...ownProps,
    ...stateProps,
    removeAddress: (index) => {
      actions.removeAddress(dispatch, index);
    },
    fetchAllCountries: () => CountryRedux.actions.fetchAllCountries(dispatch),
    initAllAddresses: (address) => actions.initAllAddresses(dispatch, address),
    selectAddress: (address) => {
      actions.selectAddress(dispatch, address);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(Address));
