/**
 * Created by Premji on 01/03/2017.
 *
 * @format
 */

import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  AsyncStorage,
  Picker,
  LayoutAnimation,
  I18nManager,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { WooWorker } from "api-ecommerce";
import { connect } from "react-redux";

import { Styles, Languages, Color, withTheme } from "@common";
import { toast, error, Validate } from "@app/Omni";
import Button from "@components/Button";
import Spinner from "@components/Spinner";
import WPUserAPI from "@services/WPUserAPI";
import API from "@services/Common";

class SignUpScreen extends Component {
  constructor(props) {
    super(props);

    let state = {
      username: "",
      firstname: "",
      lastname: "",
      email: "",
      telephone: "",
      fax: "",
      company: "",
      address_1: "",
      address_2: "",
      city: "",
      postcode: "",
      country: "",
      country_id: "",
      state: "",
      zone_id: "",
      password: "",
      confirm: "",
      note: "",
      cca2: "FR",
      zone_str: '',
      counries: [],
      zones: [],
      useGeneratePass: false,
      isLoading: true,
    };

    const params = props.params;
    if (params && params.user) {
      state = { ...state, ...params.user, useGeneratePass: true };
    }

    this.state = state;

    this.onFirstNameEditHandle = (firstname) => this.setState({ firstname });
    this.onLastNameEditHandle = (lastname) => this.setState({ lastname });

    this.onUsernameEditHandle = (username) => this.setState({ username });
    this.onEmailEditHandle = (email) => this.setState({ email });
    this.onPasswordEditHandle = (password) => this.setState({ password });

    this.ontelephone = (telephone) => this.setState({ telephone });
    this.onfax = (fax) => this.setState({ fax });
    this.oncompany = (company) => this.setState({ company });

    this.onaddress_1 = (address_1) => this.setState({ address_1 });
    this.onaddress_2 = (address_2) => this.setState({ address_2 });
    this.onstate = (state, itemIndex) => this.setState({ state, country_id: JSON.parse(state).country_id, zones: JSON.parse(state).zone });
    this.oncity = (city, itemIndex) => this.setState({ zone_str: city, zone_id: JSON.parse(city).zone_id });
    this.onpostcode = (postcode) => this.setState({ postcode });
    this.oncit = (_city) => this.setState({ city: _city });
    this.oncountry_id = (country_id) => this.setState({ country_id });

    this.onzone_id = (zone_id) => this.setState({ zone_id });
    this.onconfirm = (confirm) => this.setState({ confirm });


    this.onPasswordSwitchHandle = () =>
      this.setState({ useGeneratePass: !this.state.useGeneratePass });


    this.focusLastName = () => this.lastname && this.lastname.focus();
    this.focusUsername = () => this.username && this.username.focus();
    this.focusEmail = () => this.email && this.email.focus();
    this.focusPassword = () => this.password && this.password.focus();
    this.focustelephone = () => this.telephone && this.telephone.focus();
    this.focusfax = () => this.fax && this.fax.focus();
    this.focuscompany = () => this.company && this.company.focus();
    this.focusaddress_1 = () => this.address_1 && this.address_1.focus();
    this.focusaddress_2 = () => this.address_2 && this.address_2.focus();
    this.focusstate = () => this._state && this._state.focus();
    this.focuscity = () => this.city && this.city.focus();
    this.focuspostcode = () => this.postcode && this.postcode.focus();
    this.focuscountry = () => this.country && this.country.focus();
    this.focuscountry_id = () => this.country_id && this.country_id.focus();
    this.focuszone_id = () => this.zone_id && this.zone_id.focus();
    this.focusconfirm = () => this.confirm && this.confirm.focus();
  }

  shouldComponentUpdate() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    return true;
  }

  onSignUpHandle = async () => {
    const { login, netInfo } = this.props;
    if (!netInfo.isConnected) return toast(Languages.noConnection, 2000);

    console.log('on APi register', this.state);

    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      password,
      confirm,
      isLoading,
    } = this.state;
    if (isLoading) return;
    this.setState({ isLoading: true });

    const _error = this.validateForm();
    if (_error) return this.stopAndToast(_error, 2000);

    const user = {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      password,
      confirm
    };

    console.log('final result', user)

    let json = await WPUserAPI.register(user);
    console.log('register result', json)
    if (json === undefined) {
      return this.stopAndToast("Server don't response correctly");
    } else if (json.error) {
      this.stopAndToast(json.error.message);
    } else if (json.success) {
      await WPUserAPI.getSetToken(json.customer_info.secret_key);
      json.customer_info.cart_id = json.cart_id;
      if (await WPUserAPI.getSetToken()) {
        const address = await API.getUserAddress();
        if (address && address.success) {
          json.customer_info.address = address.addresses;
        }

        let cid = await WPUserAPI.getSetCardId();
        console.log('before merge', cid);
        let result = await API.mergeGuestToCustomerCart(cid, json.cart_id);
        console.log('cart merge', result);
      }

      await AsyncStorage.setItem("@cartId", JSON.stringify(json.cart_id), async () => {
        console.log('cart Id set to local', json.cart_id);
        await WPUserAPI.getSetCardId(json.cart_id)
      });
      let cartItems = await API.getCartItem();
        
      console.log('cartItems', cartItems)
      if (cartItems && cartItems.hasOwnProperty('products') && cartItems.products.length > 0) {
        this.props.loadCartItem(cartItems)
      }

      toast(json.success.message)
      this.setState({ isLoading: false });
      login(json, json.customer_info.secret_key);
    } else {
      toast("Can't register user, please try again.");
    }
  };

  componentDidMount = async () => {
    
    if (this.props.countries && this.props.countries.list) {
      this.setState({ isLoading: false, counries: this.props.countries.list })
    } else {
      const json = await WPUserAPI.getCounrys();
      console.log('currency result', json)
      if (json === undefined) {
        return this.stopAndToast("Server don't response correctly");
      } else if (json.error) {
        return this.stopAndToast(json.error.message);
      }

      this.setState({ isLoading: false, counries: json.countries })
    }
  }

  validateForm = () => {
    console.log('validateForm', this.state)
    const {
      useGeneratePass,
    } = this.state;

    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      password,
      confirm,
    } = this.state;
    if (
      Validate.isEmpty(
        username,
        firstname,
        lastname,
        email,
        telephone,
        address_1,
        country_id,
        zone_id,
        postcode,
        password,
        confirm,
      )
    ) {
      // check empty
      return "Please complete the form";
    } else if (!Validate.isEmail(email)) {
      return "Email is not correct";
    }
    else if (password !== confirm) {
      return "Confirm password doesn't match";
    }
    // else if (firstname.length > 0 && firstname.length < 33) {
    //   return "First Name must be between 1 and 32 characters!";
    // }
    // else if (lastname.length > 0 && lastname.length < 33) {
    //   return "Last Name must be between 1 and 32 characters";
    // }
    return undefined;
  };

  stopAndToast = (msg) => {
    toast(msg, 2000);
    // error(msg);
    this.setState({ isLoading: false });
  };

  render() {
    // console.log('props', this.props)
    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      state,
      city,
      postcode,
      country,
      country_id,
      zone_id,
      password,
      confirm,
      counries,
      zones,
      zone_str,
      useGeneratePass,
      isLoading,
    } = this.state;

    const {
      theme: {
        colors: { background, text, placeholder },
      },
    } = this.props;

    const params = this.props.params;
    // console.log('this.state', this.state)
    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        {isLoading ? <Spinner mode="overlay" /> : null}
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          enableOnAndroid>
          <View style={styles.formContainer}>
            <Text style={[styles.label, { color: text }]}>
              {Languages.profileDetail}
            </Text>
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.firstname = comp)}
              placeholder={Languages.firstName}
              onChangeText={this.onFirstNameEditHandle}
              onSubmitEditing={this.focusLastName}
              autoCapitalize="words"
              returnKeyType="next"
              value={firstname}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.lastname = comp)}
              placeholder={Languages.lastName}
              onChangeText={this.onLastNameEditHandle}
              onSubmitEditing={this.focusEmail}
              autoCapitalize="words"
              returnKeyType="next"
              value={lastname}
              placeholderTextColor={placeholder}
            />

            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.email = comp)}
              placeholder={Languages.email}
              onChangeText={this.onEmailEditHandle}
              onSubmitEditing={this.focustelephone}
              keyboardType="email-address"
              returnKeyType={"next"}
              value={email}
              placeholderTextColor={placeholder}
            />

            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.telephone = comp)}
              placeholder={Languages.telephone}
              onChangeText={this.ontelephone}
              onSubmitEditing={this.focusfax}
              returnKeyType="next"
              keyboardType='numeric'
              value={telephone}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.fax = comp)}
              placeholder={Languages.fax}
              onChangeText={this.onfax}
              onSubmitEditing={this.focuscompany}
              autoCapitalize="words"
              returnKeyType="next"
              value={fax}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.company = comp)}
              placeholder={Languages.company}
              onChangeText={this.oncompany}
              onSubmitEditing={this.focusaddress_1}
              autoCapitalize="words"
              returnKeyType="next"
              value={company}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.address_1 = comp)}
              placeholder={Languages.address_1}
              onChangeText={this.onaddress_1}
              onSubmitEditing={this.focusaddress_2}
              autoCapitalize="words"
              returnKeyType="next"
              value={address_1}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.address_2 = comp)}
              placeholder={Languages.address_2}
              onChangeText={this.onaddress_2}
              onSubmitEditing={this.focuscountry}
              autoCapitalize="words"
              returnKeyType="next"
              value={address_2}
              placeholderTextColor={placeholder}
            />
            {/* <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.country = comp)}
              placeholder={Languages.country}
              onChangeText={this.oncountry}
              onSubmitEditing={this.focusstate}
              autoCapitalize="words"
              returnKeyType="next"
              value={country}
              placeholderTextColor={placeholder}
            /> */}
            {/* <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this._state = comp)}
              placeholder={Languages.state}
              onChangeText={this.onstate}
              onSubmitEditing={this.focuscity}
              autoCapitalize="words"
              returnKeyType="next"
              value={state}
              placeholderTextColor={placeholder}
            /> */}
            <Picker
              selectedValue={state}
              style={styles.select(text)}
              ref={(comp) => (this._state = comp)}
              onValueChange={this.onstate}>
              <Picker.Item label={Languages.Selectcountry} value={null} />
              {
                counries.map((value, index) => (
                  <Picker.Item key={index} label={value.name} value={JSON.stringify(value)} />
                ))
              }
            </Picker>
            {/* <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.city = comp)}
              placeholder={Languages.city}
              onChangeText={this.oncity}
              onSubmitEditing={this.focuspostcode}
              autoCapitalize="words"
              returnKeyType="next"
              value={city}
              placeholderTextColor={placeholder}
            /> */}
            <Picker
              selectedValue={zone_str}
              style={styles.select(text)}
              ref={(comp) => (this._state = comp)}
              onValueChange={this.oncity}>
              <Picker.Item label={Languages.Selectzone} value={null} />
              {
                zones.map((value, index) => (
                  <Picker.Item key={index} label={value.name} value={JSON.stringify(value)} />
                ))
              }
            </Picker>
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this._state = comp)}
              placeholder={Languages.city}
              onChangeText={this.oncit}
              onSubmitEditing={this.focuspostcode}
              autoCapitalize="words"
              returnKeyType="next"
              value={city}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.postcode = comp)}
              placeholder={Languages.postcode}
              onChangeText={this.onpostcode}
              onSubmitEditing={this.focusUsername}
              autoCapitalize="words"
              keyboardType='numeric'
              returnKeyType="next"
              value={postcode}
              placeholderTextColor={placeholder}
            />

            <Text style={[styles.label, { color: text }]}>
              {Languages.accountDetails}
            </Text>
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.username = comp)}
              placeholder={Languages.username}
              onChangeText={this.onUsernameEditHandle}
              onSubmitEditing={this.focusPassword}
              autoCapitalize="none"
              returnKeyType="next"
              value={username}
              placeholderTextColor={placeholder}
            />

            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.password = comp)}
              placeholder={Languages.password}
              onChangeText={this.onPasswordEditHandle}
              onSubmitEditing={this.focusconfirm}
              secureTextEntry
              returnKeyType="next"
              value={password}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.confirm = comp)}
              placeholder={Languages.confirm}
              onChangeText={this.onconfirm}
              secureTextEntry
              returnKeyType="done"
              value={confirm}
              placeholderTextColor={placeholder}
            />
            <Button
              containerStyle={styles.signUpButton}
              text={Languages.signup}
              onPress={this.onSignUpHandle.bind(this)}
            />
          </View>

        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    padding: Styles.width * 0.1,
  },
  label: {
    fontWeight: "bold",
    fontSize: Styles.FontSize.medium,
    color: Color.blackTextPrimary,
    marginTop: 20,
  },

  input: (text) => ({
    borderBottomWidth: 1,
    borderColor: text,
    height: 40,
    marginTop: 10,
    padding: 0,
    margin: 0,
    // flex: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: text,
  }),
  select: (text) => ({
    borderColor: text,
    height: 40,
    marginTop: 20,
    padding: 0,
    margin: 0,
    paddingLeft: -10,
    // flex: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: text,
  }),
  signUpButton: {
    marginTop: 20,
    backgroundColor: Color.primary,
    borderRadius: 5,
    elevation: 1,
  },
  switchWrap: {
    ...Styles.Common.RowCenterLeft,
    marginTop: 10,
  },
  text: {
    marginLeft: 10,
    color: Color.blackTextSecondary,
  },
});

const mapStateToProps = (state) => {
  return {
    netInfo: state.netInfo,
    countries: state.countries,
  };
};

const mapDispatchToProps = (dispatch) => {
  const { user_actions } = require("@redux/UserRedux");
  const CartRedux = require("@redux/CartRedux");

  return {
    login: (user, token) => dispatch(user_actions.login(user, token)),
    getCounrys: () => dispatch(user_actions.getCounrys()),
    loadCartItem: (data) => CartRedux.actions.loadCartItem(dispatch, data),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withTheme(SignUpScreen));
