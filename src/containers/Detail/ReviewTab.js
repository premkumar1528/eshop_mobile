/** @format */

"use strict";

import React, { Component } from "react";
import { Text, View, TouchableOpacity, TextInput, Image, ScrollView, Animated } from "react-native";
import { connect } from "react-redux";
import { toast } from "@app/Omni";
import { Languages } from "@common";
import API from "@services/Common";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import Modal from "react-native-modalbox";
import { Rating, AirbnbRating } from 'react-native-ratings';

import Spinner from "@components/Spinner";
import styles from "./ReviewTab_Style.js";
import ReviewItem from "./ListItem/ReviewItem.js";

class ReviewTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reviews: [],
      isFetching: true,
      isLoading: false,
      reviewForm: {
        name: '',
        review: '',
        product_id: '',
        rating: 0
      }
    }
    this._modalReview = React.createRef();
  }

  async componentDidMount() {
    // this.props.fetchReviews(this.props.product.id);
    // console.log('this.props.product', this.props.product)
    this.onChangeText('product_id', this.props.product.product_id);
    this.loadReviews();
  }

  loadReviews = async () => {
    let reviews = await API.getReviewById(this.props.product.product_id);

    try {
      if (reviews && reviews.success) {
        this.setState({ reviews: reviews.reviews, isFetching: false });
      }
    } catch (error) {
      console.log('error', error);
    }

    this.setState({ isFetching: false });
  }

  handleReviewModal = (event) => {
    if (event === 'open') {
      this._modalReview.open();
      // setTimeout(() => {
      //   this.focus('name');
      // }, 2000);
    } else this._modalReview.close();
  }

  focus = (key) => {
    switch (key) {
      case 'name':
        this._name.focus();
        break;
      case 'review':
        this._review.focus();
        break;

      default:
        console.log('default', key)
        break;
    }
  }

  onChangeText = (element, value) => {
    console.log('element', element, value);
    let reviewForm = this.state.reviewForm;
    reviewForm[element] = value;
    this.setState({ reviewForm })
  }

  async onSubmit() {
    console.log("Rating is: ", this.state.reviewForm);
    const { name, review, rating } = this.state.reviewForm;
    if (name === '') {
      toast('Please fill the name', 5000);
      return null;
    } else if (review === '') {
      toast('Please fill the review', 5000);
      return null;
    } else if (rating === 0) {
      toast('Please fill the rating', 5000);
      return null;
    }

    this.setState({ isLoading: true });
    let result = await API.addReview(this.state.reviewForm);
    this.setState({ isLoading: false });

    console.log('result', result)
    if (result && result.success) {
      await this.loadReviews();
      toast(result.success.message, 5000);
      this._modalReview.close();
    } else if (result.error) {
      toast(result.error.message, 5000);
    } else {
      toast('Bad Connection. Try after sometimes...', 5000);
      this._modalReview.close();
    }
  }

  render() {


    return (
      <View>
        {this.reviewModal()}
        {this.addReviewButton()}
        <View style={styles.container}>{this.renderContent()}</View>
      </View>
    );
  }

  reviewModal = () => {
    const { name, review } = this.state.reviewForm;
    const { isLoading } = this.state;

    return (
      <Modal
        ref={(com) => (this._modalReview = com)}
        swipeToClose={false}
        animationDuration={200}
        backdropPressToClose={false}
        style={styles.modalBoxWrap}>
        <View style={{
          padding: 15,
        }}>
          {isLoading ? <Spinner mode="center" /> : null}
          <Text
            style={styles.modalContainerTitle}
          >Write a review
          </Text>
          <TouchableOpacity style={styles.modalClose} onPress={this.handleReviewModal}>
            <Text style={styles.modalCloseText}>X</Text>
          </TouchableOpacity>
          <KeyboardAwareScrollView
            showsVerticalScrollIndicator={false}
            enableOnAndroid>
            <Animated.View>
              <View>
                <Text style={styles.title}>
                  <Text style={styles.titlex}> * </Text>
                  Your Name
              </Text>
                <TextInput
                  style={styles.input()}
                  underlineColorAndroid="transparent"
                  ref={(comp) => (this._name = comp)}
                  onChangeText={(v) => this.onChangeText('name', v)}
                  onSubmitEditing={() => this.focus('review')}
                  autoCapitalize="words"
                  returnKeyType="next"
                  value={name}
                />
              </View>
              <View>
                <Text style={styles.title}>
                  <Text style={styles.titlex}> * </Text>
                  Review
              </Text>
                <TextInput
                  style={styles.input()}
                  underlineColorAndroid="transparent"
                  ref={(comp) => (this._review = comp)}
                  onChangeText={(v) => this.onChangeText('review', v)}
                  autoCapitalize="words"
                  returnKeyType="next"
                  value={review}
                />
              </View>
              <View>
                <Text style={styles.title}>
                  <Text style={styles.titlex}> * </Text>
                  Rating
              </Text>
                <AirbnbRating
                  count={5}
                  type='star'
                  defaultRating={0}
                  showRating={false}
                  size={20}
                  onFinishRating={(v) => this.onChangeText('rating', v)}
                />
              </View>
              <TouchableOpacity
                style={styles.submitBtn}
                onPress={() => this.onSubmit()}
              >
                <Text style={styles.submitBtnTxt}>Submit Review</Text>
              </TouchableOpacity>
            </Animated.View>
          </KeyboardAwareScrollView>
        </View>
      </Modal>
    );
  }

  addReviewButton = () => {
    return (
      <TouchableOpacity
        style={styles.addReviewButton}
        onPress={() => this.handleReviewModal('open')}
      >
        <Image
          style={{ height: 20, width: 20 }}
          source={require("@images/icons/plus.png")}
        />
      </TouchableOpacity>
    );
  }

  renderContent() {
    const { reviews, isFetching } = this.state;

    if (isFetching) {
      return <Spinner fullStretch />;
    }

    if (reviews && reviews.length == 0) {
      return (
        <View>
          <Text style={styles.message}> {Languages.NoReview}</Text>
        </View>
      );
    }

    if (reviews && reviews.length > 0) {
      console.log('render review container', reviews)
      return (
        <View>
          <ScrollView style={styles.message}>
            {reviews.map((rowData, index) => (
              <ReviewItem key={index} review={rowData} />
            ))}
          </ScrollView>
        </View>
      );
    }
  }
}



const mapStateToProps = (state) => {
  return {
    netInfo: state.netInfo,
    reviews: state.products.reviews,
    isFetching: state.products.isFetching,
    message: state.products.message,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { netInfo } = stateProps;
  const { dispatch } = dispatchProps;
  const { actions } = require("./../../redux/ProductRedux");

  return {
    ...ownProps,
    ...stateProps,
    fetchReviews: (productId) => {
      if (!netInfo.isConnected) return toast(Languages.NoConnection);
      actions.fetchReviewsByProductId(dispatch, productId);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(ReviewTab);
