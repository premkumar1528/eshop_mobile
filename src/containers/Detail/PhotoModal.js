/** @format */

import React, { PureComponent } from "react";
import {
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { getProductImage } from "@app/Omni";
import Swiper from "react-native-swiper";
import {
  Styles,
  Languages,
  Constants,
} from "@common";
import Modal from "react-native-modalbox";
import Spinner from "@components/Spinner";
import styles from "./ProductDetail_Style";
import ImageViewer from 'react-native-image-zoom-viewer';

class PhotoModal extends PureComponent {
  open = () => {
    // console.log('PhotoModal', props)
    this._modalPhoto.open()
  }

  close = () => {
    this._modalPhoto.close()
  }

  render() {
    const {
      product,
      image,
    } = this.props;



    const imageArray = (product.hasOwnProperty('images') && product.images.length > 0) ? product.images : false;
    console.log('image', product.hasOwnProperty('images') && product.images.length > 0)
    return (
      <Modal
        ref={(com) => (this._modalPhoto = com)}
        swipeToClose={false}
        animationDuration={200}
        style={styles.modalBoxWrap}>
        <Swiper
          height={Constants.Window.height - 60}
          activeDotStyle={styles.dotActive}
          containerStyle={{ bottom: 75 }}
          removeClippedSubviews={false}
          dotStyle={styles.dot}
          paginationStyle={{ zIndex: 9999 }}>
          {
            imageArray ?
              <ImageViewer imageUrls={imageArray.map((image, index) => ({ url: image.popup }))} renderIndicator={() => <Spinner mode="normal" />} />
              :
              <ImageViewer imageUrls={[{ url: image || product.thumb }]} renderIndicator={() => <Spinner mode="normal" />} />
          }

        </Swiper>

        <TouchableOpacity style={styles.iconZoom} onPress={this.close}>
          <Text style={styles.textClose}>{Languages.close}</Text>
        </TouchableOpacity>
      </Modal>
    );
  }

  renderImages = (gallery) => {
    var images = []
    if (gallery) {
      gallery.map((element, idx) => {
        images.push({ url: element.url })
      })
    }

    return <ImageViewer imageUrls={images} renderIndicator={() => <View />} />
  }
}

export default PhotoModal
