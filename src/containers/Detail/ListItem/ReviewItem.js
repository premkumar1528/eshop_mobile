/** @format */

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  I18nManager,
} from "react-native";
import styles from "./ReviewItem_Style.js";
import Rating from "@components/Rating";
import moment from "moment";
import { Constants } from "@common";

export default class ReviewItem extends Component {
  constructor(props) {
    super(props)
  }
  
  render() {
    let { review } = this.props;
    console.log('render review', review)
    return (
      <View style={styles.container}>
        <Text style={styles.name}>{review.author}</Text>
        <Text style={styles.review}>{review.text}</Text>
        <View
          style={{
            justifyContent: "space-between",
            flexDirection: 'row'
          }}>
          <Text style={styles.date_created}>
            {review.date_added}
          </Text>
          <Rating rating={review.rating} style={styles.rating} />
        </View>
        <View style={styles.separator} />
      </View>
    );
  }

  dateFormat(date) {
    return moment.parseZone(date).format("MMMM DD, YYYY, HH:mm");
  }
}
