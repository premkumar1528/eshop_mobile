/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import {
  Text,
  TouchableOpacity,
  FlatList,
  View,
  Animated,
  Image,
  Share,
  Dimensions,
  TextInput,
  CheckBox,
  Modal
} from "react-native";
import { connect } from "react-redux";
import { Timer, getProductImage, toast } from "@app/Omni";
import {
  Button,
  WebView,
  ProductSize as ProductAttribute,
  ProductColor,
  ProductRelated,
  Rating,
  ActionSheets
} from "@components";
import {
  Styles,
  Languages,
  Config,
  Constants,
  Events,
  withTheme,
  Tools,
  Images
} from "@common";
import { find, filter } from "lodash";
import * as Animatable from "react-native-animatable";
import ReviewTab from "./ReviewTab.js";
import PhotoModal from "./PhotoModal";
import styles from "./ProductDetail_Style";
import Spinner from "@components/Spinner";
import API from '@services/Common';

const PRODUCT_IMAGE_HEIGHT = 500;
const NAVI_HEIGHT = 64;

class Detail extends PureComponent {
  static propTypes = {
    product: PropTypes.any,
    getProductVariations: PropTypes.func,
    productVariations: PropTypes.any,
    onViewCart: PropTypes.func,
    addCartItem: PropTypes.func,
    removeWishListItem: PropTypes.func,
    addWishListItem: PropTypes.func,
    cartItems: PropTypes.any,
    navigation: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
      tabIndex: 0,
      modalImage: '',
      // selectedAttribute: [],
      // selectedColor: 0,
      checkboxAttr: {
        product_option_id: "296",
        product_option_value: [
          {
            product_option_value_id: "28",
            option_value_id: "55",
            name: "Red",
            image: null,
            price: "$200.00",
            price_prefix: "+",
          }, {
            product_option_value_id: "29",
            option_value_id: "56",
            name: "Blue",
            image: null,
            price: "$100.00",
            price_prefix: "+",
          }
        ],
        option_id: "14",
        name: "Color",
        type: "checkbox",
        value: "",
        required: "1",
        selectedOption: "Red",
      },
      loading: false,
      checked: false,
      quantityModal: false,
      quantity: 1,
      selectVariation: null,
      selectedImageId: 0, // is placeholder image
      selectedImage: null,
      attributeOption: null,
      product: this.props.product,
    };


    console.log('state value', this.state);

    this.productInfoHeight = PRODUCT_IMAGE_HEIGHT;
    this.inCartTotal = 0;
    this.isInWishList = false;
    this.openPhoto = this.openPhoto.bind(this);
    this.onQuantityChange = (value, available) => {

      console.log(value, available);

      if (value && value.indexOf('.') > -1) {
        value = value.replace('.', '')
      }
      if (value && value.indexOf(',') > -1) {
        value = value.replace(',', '')
      }

      if (available == 0) {
        this.setState({ quantityModal: false });
        return toast("No Quantity Available now!");
      }

      if (Number(value) > Number(available)) {
        console.log(value > available)
        this.setState({ quantity: available });
      } else {
        console.log('quantity update', value)
        this.setState({ quantity: value });
      }
    };
  }

  componentDidMount() {
    this.getCartTotal(this.props);
    this.getWishList(this.props);

    this.getProductAttribute(this.props.product);
    this.props.getProductVariations(this.props.product);
  }

  componentWillReceiveProps(nextProps) {
    this.getCartTotal(nextProps, true);
    this.getWishList(nextProps, true);

    // this important to update the variations from the product as the Life cycle is not run again !!!

    if (this.state.product !== nextProps.product) {
      this.props.getProductVariations(nextProps.product);
      this.getProductAttribute(nextProps.product);
      // this.forceUpdate();
    }

    if (this.state.productVariations !== nextProps.productVariations) {
      this.updateSelectedVariant(nextProps.productVariations);
    }
  }

  getProductAttribute = (product) => {
    console.log('getProductAttribute', product);

    // product && product.options.filter((data) => data.name === 'Color');
    // console.log(product && );

    this.productAttributes = product.options;
    // this.productAttributes.push(this.state.checkboxAttr)
    console.log('comment the code here...')
    // const defaultAttribute = product.default_attributes;

    if (typeof this.productAttributes != "undefined") {
      if (this.productAttributes.length == 0) {

      }
      this.productAttributes.map((attribute) => {
        const selectedAttribute = undefined;
        //   defaultAttribute.find(
        //   (item) => item.name === attribute.name
        // );
        attribute.selectedOption =
          typeof selectedAttribute != "undefined"
            ? selectedAttribute.option.toLowerCase()
            : "";
      });
    }
  };

  closePhoto = () => {
    this._modalPhoto.close();
  };

  openPhoto = (prod) => {
    console.log('open modal', prod);
    this.setState({ modalImage: prod }, () => {
      this._modalPhoto.open();
    })
  };

  handleClickTab(tabIndex) {
    this.setState({ tabIndex });
    // Timer.setTimeout(() => this.state.scrollY.setValue(0), 150);
  }

  getColor = (value) => {
    console.log('getColor', value)
    const color = value.name.toLowerCase();
    if (typeof color !== "undefined") {
      return color;
    }
    return "#333";
  };

  share = () => {
    console.log('share', this.state.product)
    Share.share({
      message: this.state.product.description.replace(/(<([^>]+)>)/gi, ""),
      url: 'http://ecommerce.groceryesoft.com/',
      title: this.state.product.name,
    });
  };

  addToCart = (go = false) => {

    this.setState({ quantityModal: true });
  };

  addToCartConform = (go = false) => {
    this.setState({ quantityModal: false });
    const { quantity } = this.state;

    console.log('addToCartConform', quantity)

    if (quantity == 0) {
      return toast("Please enter the quantity");
    }

    this.setState({ loading: true });
    const { addCartItem, onViewCart } = this.props;
    const { product } = this.state;

    const { selectVariation } = this.state;
    let prod = { ...product };
    const limit = product.stock ? product.quantity : Constants.LimitAddToCart;
    if (quantity < 999) { // old
      if (product.stock) {
        console.log('addCartItem', prod, selectVariation);
        prod.quantity = quantity;
        addCartItem(prod, selectVariation);
        this.refreshProduct(prod.product_id);
      } else {
        alert(Languages.ProductLimitWaring.replace("{num}", limit));
      }
    } else {
      toast("Please add below " + 999 + " Quantity");
    }
    this.setState({ loading: false });
    // if (go) onViewCart();
  }

  formatProductResult = (prod) => {
    try {
      if (prod.options.length > 0) {
        const options = [];
        prod.options.map((data) => {
          data.selectedOption = data.product_option_value[0].name;
          options.push(data)
        });
        prod.options = options;
      }
      return prod;
    } catch (error) {
      console.log('Error formatProductResult => ', error)
      return prod;
    }
  };

  refreshProduct = async (product_id) => {
    let prods = await API.getProductById(product_id);
    console.log('refresh product', prods);
    try {
      if (prods && prods.success) {
        // prods.product_info.images.push(prods.product_info.thumb)
        prods.product_info.product_id = prods.product_id;
        this.setState({ product: this.formatProductResult(prods.product_info) })
      } else {
        if (prods && prods.error) {
          toast(prods.error.message);
        }
      }
    } catch (error) {
      console.log('error', error)
    }
  }

  addToWishList = (isAddWishList) => {
    if (isAddWishList) {
      this.props.removeWishListItem(this.state.product);
    } else {
      let product = this.state.product;
      product.quantity = 1;
      this.props.addWishListItem(product);
    }
  };

  getCartTotal = (props, check = false) => {
    const { cartItems } = props;

    console.log('cartItems', cartItems)

    if (cartItems !== null && cartItems) {
      if (check === true && props.cartItems === this.props.cartItems) {
        return;
      }

      this.inCartTotal = cartItems.reduce((accumulator, currentValue) => {
        if (currentValue.product.product_id === this.props.product.product_id) {
          return accumulator + currentValue.quantity;
        }
        return 0;
      }, 0);

      const sum = cartItems.reduce(
        (accumulator, currentValue) => accumulator + currentValue.quantity,
        0
      );
      const params = this.props.navigation.state.params;
      params.cartTotal = sum;
      this.props.navigation.setParams(params);
    }
  };

  getWishList = (props, check = false) => {
    const { navigation, wishListItems } = props;
    const { product } = this.state;

    if (props.hasOwnProperty("wishListItems")) {
      if (check && props.wishListItems === this.props.wishListItems) {
        return;
      }
      this.isInWishList =
        find(props.wishListItems, (item) => item.product.product_id === product.product_id) !== "undefined";

      const sum = wishListItems.length;
      const params = navigation.state.params;
      params.wistListTotal = sum;
      this.props.navigation.setParams(params);
    }
  };

  onSelectAttribute = (attributeName, option) => {
    console.log('onSelectAttribute', this.productAttributes, option);
    const selectedAttribute = this.productAttributes.find(
      (item) => item.name.toLowerCase() === attributeName.toLowerCase()
    );
    selectedAttribute.selectedOption = option.name.toLowerCase();
    this.updateSelectedVariant(this.props.productVariations);
  };

  updateSelectedVariant = (productVariations) => {
    let hasAttribute = false;
    const defaultVariant =
      productVariations && productVariations.length
        ? productVariations[0]
        : null;
    // filter selectedOption null or don't have variation
    const selectedAttribute = filter(
      this.productAttributes,
      (item) =>
        (item.selectedOption && item.selectedOption !== "") || item.variation
    );
    let selectedImage = (defaultVariant && (defaultVariant.image && defaultVariant.image.src)) || "";
    let selectedImageId = 0;

    if (productVariations && productVariations.length) {
      productVariations.map((variant) => {
        let matchCount = 0;
        selectedAttribute.map((selectAttribute) => {
          const isMatch = find(
            variant.attributes,
            (item) =>
              item.name.toUpperCase() === selectAttribute.name.toUpperCase() &&
              item.option.toUpperCase() ===
              selectAttribute.selectedOption.toUpperCase()
          );
          if (isMatch !== undefined) {
            matchCount += 1;
          }
        });

        if (matchCount === selectedAttribute.length) {
          hasAttribute = true;
          selectedImage = (variant.image && variant.image.src) || "";
          selectedImageId = variant.image.id;
          this.setState({
            selectVariation: variant,
            selectedImage,
            selectedImageId,
          });
        }
      });
    }

    // set default variant
    if (!hasAttribute && defaultVariant) {
      this.setState({
        selectVariation: defaultVariant,
        selectedImage,
        selectedImageId,
      });
    }

    this.forceUpdate();
  };

  /**
   * render Image top
   */
  _renderImages = (product) => {

    console.log('_renderImages', product && !(product.images && product.images.length > 0) && product.thumb)

    const { selectedImage, selectedImageId } = this.state;
    const imageScale = this.state.scrollY.interpolate({
      inputRange: [-300, 0, NAVI_HEIGHT, this.productInfoHeight / 2],
      outputRange: [2, 1, 1, 0.7],
      extrapolate: "clamp",
    });

    if (!product) {
      return null;
    }

    // set variant image and only show when do not placeholder image
    if (product && !(product.images && product.images.length > 0) && product.thumb)
      return (
        <View
          style={{
            // height: PRODUCT_IMAGE_HEIGHT,
            width: Constants.Window.width - 90,
          }}>
          <TouchableOpacity activeOpacity={1} onPress={() => this.openPhoto(product.thumb)}>
            <Animated.Image
              source={{
                uri: product.thumb,
              }}
              style={[
                styles.imageProduct,
                { transform: [{ scale: imageScale }] },
              ]}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      );

    return (
      <FlatList
        contentContainerStyle={{ paddingLeft: Styles.spaceLayout }}
        ref={(comp) => (this._photos = comp)}
        data={product.images}
        renderItem={({ item, index }) => {
          return (
            <TouchableOpacity
              activeOpacity={1}
              key={index}
              onPress={() => this.openPhoto(item.popup)}>
              <Animated.Image
                source={{ uri: item.thumb }}
                key={index + 'image'}
                style={[
                  styles.imageProductSlide,
                  { transform: [{ scale: imageScale }] },
                ]}
                resizeMode="contain"
              />
            </TouchableOpacity>
          );
        }}
        keyExtractor={(item, index) => `${item.product_id}` || index.toString()}
        showsHorizontalScrollIndicator={false}
        horizontal
        pagingEnabled
      />
    );
  };

  /**
   * Render tabview detail
   */
  _renderTabView = (product) => {
    const {
      theme: {
        colors: { background, text, lineColor },
      },
    } = this.props;

    return (
      <View style={[styles.tabView, { backgroundColor: background }]}>
        <View
          style={[
            styles.tabButton,
            { backgroundColor: lineColor },
            { borderTopColor: lineColor },
            { borderBottomColor: lineColor },
            //Constants.RTL && { flexDirection: "row-reverse" },
          ]}>
          <View style={[styles.tabItem, { backgroundColor: lineColor }]}>
            <Button
              type="tab"
              textStyle={[styles.textTab, { color: text }]}
              selectedStyle={{ color: text }}
              text={Languages.AdditionalInformation}
              onPress={() => this.handleClickTab(0)}
              selected={this.state.tabIndex === 0}
            />
          </View>
          {/* <View style={[styles.tabItem, { backgroundColor: lineColor }]}>
            <Button
              type="tab"
              textStyle={[styles.textTab, { color: text }]}
              selectedStyle={{ color: text }}
              text={Languages.ProductFeatures}
              onPress={() => this.handleClickTab(1)}
              selected={this.state.tabIndex == 1}
            />
          </View> */}
          <View style={[styles.tabItem, { backgroundColor: lineColor }]}>
            <Button
              type="tab"
              textStyle={[styles.textTab, { color: text }]}
              selectedStyle={{ color: text }}
              text={Languages.ProductReviews}
              onPress={() => this.handleClickTab(2)}
              selected={this.state.tabIndex == 2}
            />
          </View>
        </View>
        {this.state.tabIndex === 0 && (
          <View style={[styles.description, { backgroundColor: lineColor }]}>
            <WebView
              textColor={text}
              html={`<p style="text-align: left">${product.description}</p>`}
            />
          </View>
        )}
        {/* {this.state.tabIndex === 1 && (
          <AttributesView attributes={product.attributes} />
        )} */}
        {this.state.tabIndex === 2 && (
          <ReviewTab product={product} />
        )}
      </View>
    );
  };

  _writeReview = () => {
    const { product, userData, onLogin } = this.props;
    if (userData) {
      Events.openModalReview(product);
    } else {
      onLogin();
    }
  };

  renderActionSheet = (name, options) => {
    const titles = [];
    options.map((item, index) => {
      titles.push({ title: item });
    })
    titles.push({ title: "Cancel", isCancel: true, actionStyle: 'cancel' })
    let defaultVal = this.state.attributeOption ? this.state.attributeOption : titles[0].title;
    return (
      <ActionSheets
        ref="picker"
        titles={titles}
        separateHeight={1}
        separateColor="#dddddd"
        defaultValue={defaultVal}
        backgroundColor="rgba(0, 0, 0, 0.3)"
        containerStyle={{ margin: 10, borderRadius: 5 }}
        onPress={(option) => {
          this.setState({ attributeOption: option.name })
          this.onSelectAttribute(name, option.name)
        }}
      />
    )
  }

  renderDropdown = (attribute) => {
    const { attributeOption } = this.state;
    const title = attributeOption ? attributeOption : attribute.options[0];
    const {
      theme: {
        colors: { background, text, lineColor },
        dark,
      },
    } = this.props;

    return (
      <TouchableOpacity onPress={() => this.refs.picker.show()} style={styles.dropdownStyle}>
        <View style={[styles.dropdownLeftStyle, {}]}>
          <Text style={{ fontSize: 13, color: text }}>{attribute.name.toUpperCase()}</Text>
        </View>
        <View style={styles.dropdownRightStyle}>
          <View style={styles.dropdownRightTitleStyle}>
            <Text style={[styles.dropdownTextStyle, { color: text }]}>{title}</Text>
          </View>
          <View style={styles.dropdownRightIconStyle}>
            <Image
              source={Images.IconDropdown}
              style={[styles.iconStyle, { tintColor: text }]}
            />
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  onSelectCheckbox = (parent, index, checkedItem) => {
    console.log(parent, checkedItem);
    if (this.productAttributes == undefined) {
      return;
    }
    this.productAttributes.forEach((element) => {
      if (element.product_option_id == parent) {
        if (checkedItem.checked) {
          element.product_option_value[index].checked = false;
        } else {
          element.product_option_value[index].checked = true;
        }
        // console.log('check item', element.product_option_value, index)
      }
    });
    this.forceUpdate();
  }

  adjustQuantity = (type) => {
    console.log('add', type)
    const { quantity } = this.state;
    if (type) {
      this.onQuantityChange(Number(quantity) > 0 ? (Number(quantity) + 1).toString() : "1", this.props.product.quantity)
    } else {
      this.onQuantityChange(Number(quantity) > 0 ? (Number(quantity) - 1).toString() : "0", this.props.product.quantity)
    }
    this.forceUpdate();
  }

  render() {
    const { selectVariation, modalImage, loading, quantityModal, quantity, product } = this.state;

    const {
      wishListItems,
      onViewProductScreen,
      // product,
      cartItems,
      theme: {
        colors: { background, text, lineColor, placeholder },
        dark,
      }
    } = this.props;


    console.log('product rendering', this.state)

    const isAddToCart = !!(
      cartItems &&
      cartItems.filter((item) => item.product.product_id === product.product_id).length > 0
    );
    const isAddWishList =
      wishListItems.filter((item) => item.product.product_id === product.product_id).length > 0;
    const procductPriceIncludedTax = Tools.getPriceIncluedTaxAmount(
      product,
      selectVariation,
      true
    );
    const productRegularPrice = Tools.getCurrecyFormatted(
      selectVariation ? selectVariation.regular_price : product.regular_price
    );
    const isOnSale = selectVariation
      ? selectVariation.on_sale
      : product.on_sale;
    const outOfStock = !product.stock; // || (product.quantity ? product.quantity <= this.inCartTotal : false)

    if (this.productAttributes == 'undefined') {
      this.productAttributes = this.props.product.options;
    }

    const renderButtons = () => (
      <View
        style={[
          styles.bottomView,
          dark && { borderTopColor: lineColor },
          //Constants.RTL && { flexDirection: "row-reverse" },
        ]}>
        <View
          style={[
            styles.buttonContainer,
            dark && { backgroundColor: lineColor },
          ]}>
          <Button
            type="image"
            source={require("@images/icons/icon-share.png")}
            imageStyle={[styles.imageButton, { tintColor: text }]}
            buttonStyle={styles.buttonStyle}
            onPress={this.share}
          />
          <Button
            type="image"
            isAddWishList={isAddWishList}
            source={require("@images/icons/icon-love.png")}
            imageStyle={[styles.imageButton, { tintColor: text }]}
            buttonStyle={styles.buttonStyle}
            onPress={() => this.addToWishList(isAddWishList)}
          />
          {Config.AddToCartInline.enable && (
            <Button
              type="image"
              isAddToCart={isAddToCart}
              source={require("@images/icons/icon-cart.png")}
              imageStyle={[styles.imageButton, { tintColor: text }]}
              disabled={outOfStock}
              buttonStyle={styles.buttonStyle}
              onPress={() => product.stock && this.addToCart(true)}
            />
          )}
        </View>
        <Button
          text={!outOfStock ? Languages.AddtoCart : Languages.OutOfStock}
          style={[styles.btnBuy, outOfStock && styles.outOfStock]}
          textStyle={styles.btnBuyText}
          disabled={outOfStock}
          onPress={() => {
            if (!Config.Affiliate.enable) {
              !outOfStock && this.addToCart(true);
            } else {
              this.props.onOpenWebsite(product.permalink)
            }
          }}
        />
        <Modal
          animationType="slide"
          transparent
          visible={quantityModal}
          onShow={this.onShow}>
          <View style={styles.modalContainer}>
            <View style={styles.modalBody}>
              <TouchableOpacity
                style={styles.closeModal}
                onPress={() => this.setState({ quantity: 0, quantityModal: false })}
              >
                <Text style={styles.closeModalText}>X</Text>
              </TouchableOpacity>
              <Text style={styles.modalTite}>Update Quantity</Text>
              <View style={styles.quantityContainer}>
                <TouchableOpacity
                  onPress={() => this.adjustQuantity()}
                >
                  <Text style={styles.quantityActionText(true)}>-</Text>
                </TouchableOpacity>
                <TextInput
                  underlineColorAndroid="transparent"
                  placeholder={Languages.EnterQuantity}
                  onChangeText={(e) => this.onQuantityChange(e, product.quantity)}
                  autoCapitalize="none"
                  keyboardType='numeric'
                  value={quantity}
                  defaultValue="1"
                  editable={product.quantity > 0}
                  style={styles.quantityBox}
                  onSubmitEditing={this.addToCartConform}
                  placeholderTextColor={placeholder}
                />
                <TouchableOpacity
                  onPress={() => this.adjustQuantity(true)}
                >
                  <Text style={styles.quantityActionText()}>+</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.modalDesc}>* Minum Quantity:{product.minimum}</Text>
              <Text style={styles.modalDesc}>* Available Quantity: {product.quantity}</Text>
              <TouchableOpacity
                style={styles.quantityActionButton}
                onPress={this.addToCartConform}
              >
                <Text style={styles.addText}>Add</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );

    const renderRating = (product) => {
      return (
        <View style={styles.price_wrapper(background)}>
          <Rating rating={Number(product.rating)} size={19} />

          <Text style={[styles.textRating, { color: text }]}>
            {`(${product.rating})`}
          </Text>

          <TouchableOpacity>
            <Text style={[styles.textRating, { color: text }]}>
              {Languages.writeReview}
            </Text>
          </TouchableOpacity>
        </View>
      );
    };

    const strikePrice = product.strike_price === '' ? false : product.strike_price;
    console.log('strikePrice', strikePrice);

    const renderTitle = () => (
      <View style={{ justifyContent: "center", marginTop: 6, marginBottom: 8 }}>
        {product.model !== '' && product.model !== null &&
          <Text style={[styles.productModel, { color: text }]}>
            Product Code: {product.model}
          </Text>
        }
        <Text style={[styles.productName, { color: text }]}>
          {product.name}
        </Text>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            marginTop: 2,
            marginBottom: 4,
          }}>
          <Animatable.Text
            animation="fadeInDown"
            style={[strikePrice ? styles.sale_price : styles.productPrice, { color: text, }]}>
            {product.price}
          </Animatable.Text>
          <Text>&nbsp;</Text>
          {strikePrice &&
            <Animatable.Text
              animation="fadeInDown"
              style={[styles.productPrice, { color: text, }]}>
              {strikePrice}
            </Animatable.Text>
          }
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            marginTop: 2,
            marginBottom: 4,
          }}>
          <Animatable.Text style={[styles.productTax]} animation="fadeInDown">
            Ex Tax: {product.tax}
          </Animatable.Text>
        </View>
      </View>
    );

    const renderAttributes = () => {
      if (this.productAttributes == 'undefined') {
        this.productAttributes = this.props.product.options;
      }
      let checkBoxes = [];
      try {
        console.log(this.productAttributes);
        checkBoxes = this.props.product.options.filter((item) => item.type.toLowerCase() === 'checkbox');
      } catch (e) {
        console.log('error in productAttributes', e)
      }
      console.log('checkBoxes', checkBoxes)
      // this.state.checkboxAttr
      // if(checkBoxes.length > 0) {
      //   checkBoxes = [this.state.checkboxAttr];
      // }
      // console.log('renderAttributes', this.state)
      return (
        <View>
          {typeof this.productAttributes != "undefined" &&
            this.productAttributes.map((attribute, attrIndex) => (
              <View
                key={`attr${attrIndex}`}
                style={[
                  styles.productSizeContainer,
                  Constants.RTL && { flexDirection: "row-reverse" },
                ]}>
                {this.productAttributes.length > 0 && attribute.name.toLowerCase() !== Constants.productAttributeColor.toLowerCase() &&
                  attribute.type.toLowerCase() !== 'checkbox' &&
                  attribute.product_option_value.map((option, index) => (
                    <ProductAttribute
                      key={index + 'attr'}
                      text={option}
                      style={styles.productSize}
                      onPress={() =>
                        this.onSelectAttribute(attribute.name, option)
                      }
                      selected={
                        attribute.selectedOption.toLowerCase() === option.name.toLowerCase()
                      }
                    />
                  ))
                }
                {
                  /*this.productAttributes.length <= 2 && attribute.name.toLowerCase() !== Constants.productAttributeColor.toLowerCase() &&
                    <View key={attrIndex} style={{ width: width * 0.85 }}>
                      {
                        this.renderDropdown(attribute)
                      }
                      {
                        this.renderActionSheet(attribute.name, attribute.options)
                      }
                    </View> */
                }
              </View>
            ))}
          {
            checkBoxes && checkBoxes.length > 0 && (
              <View>
                {
                  checkBoxes.map((checkitem, checkindex) => (
                    <View style={{
                      flexDirection: "row",
                      justifyContent: "center",
                    }}
                      key={checkindex + 'check'}>
                      <Text style={styles.checkboxConainer}>Available {checkitem.name}s:</Text>
                      {
                        checkitem.product_option_value.map((item, index) => (
                          <View key={index} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <CheckBox
                              style={{ paddingLeft: 5, paddingRight: 5 }}
                              onValueChange={() => this.onSelectCheckbox(checkitem.product_option_id, index, item)}
                              value={item.checked}
                            />
                            <Text style={{ alignSelf: 'center', textAlign: 'center', color: item.name.toLowerCase() }}>{item.name}</Text>
                          </View>
                        ))
                      }
                    </View>
                  ))
                }
              </View>
            )
          }

        </View>
      )
    };

    const renderProductColor = (product) => {
      console.log('renderProductColor', this.productAttributes, product)

      // if (typeof this.productAttributes == "undefined") {
      //   return;
      // }

      // let color = product.options.filter((data) => data.name === 'Color') || [];

      // if(color.length === 0) {
      //   return;
      // }

      const productColor = product.options.find(
        (item) => item.name.toLowerCase() === Constants.productAttributeColor.toLowerCase()
      );

      if (productColor) {
        const translateY = this.state.scrollY.interpolate({
          inputRange: [0, PRODUCT_IMAGE_HEIGHT / 2, PRODUCT_IMAGE_HEIGHT],
          outputRange: [0, -PRODUCT_IMAGE_HEIGHT / 3, -PRODUCT_IMAGE_HEIGHT],
          extrapolate: "clamp",
        });

        if (!productColor.selectedOption) {
          productColor.selectedOption = productColor.product_option_value[0].name
        }

        console.log('productColor', translateY);

        // return (null);

        return (
          <Animated.View
            style={[
              styles.productColorContainer,
              { transform: [{ translateY }] },
            ]}>
            {productColor.product_option_value.map((option, index) => (
              <ProductColor
                key={index}
                color={this.getColor(option)}
                onPress={() => {
                  this.onSelectAttribute(
                    Constants.productAttributeColor,
                    option
                  )
                }}
                selected={
                  productColor.selectedOption && productColor.selectedOption.toLowerCase() ===
                  option.name.toLowerCase()
                }
              />
            ))}
          </Animated.View>
        );
      }
    };

    const renderProductRelated = () => (
      <ProductRelated
        onViewProductScreen={(product) => {
          this.list.getNode().scrollTo({ x: 0, y: 0, animated: true });
          onViewProductScreen(product);
        }}
        product={product}
      // tags={product.related_ids}
      />
    );

    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        <Animated.ScrollView
          ref={(c) => (this.list = c)}
          overScrollMode="never"
          style={styles.listContainer}
          scrollEventThrottle={1}
          onScroll={(event) => {
            this.state.scrollY.setValue(event.nativeEvent.contentOffset.y);
          }}>

          <View
            style={[styles.productInfo, { backgroundColor: background }]}
            onLayout={(event) =>
              (this.productInfoHeight = event.nativeEvent.layout.height)
            }>
            {loading ? <Spinner mode="overlay" /> : null}
            {this._renderImages(product)}

            {renderAttributes()}
            {renderTitle(product)}
            {renderRating(product)}
          </View>
          {this._renderTabView(product)}
          {/* 
            {renderProductRelated()} 
              <AdMob />
          */}
        </Animated.ScrollView>
        {renderProductColor(product)}

        {renderButtons(product)}
        <PhotoModal ref={(com) => (this._modalPhoto = com)} image={modalImage} product={product} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartItems: state.carts.cartItems,
    wishListItems: state.wishList.wishListItems,
    productVariations: state.products.productVariations,
    userData: state.user.user,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CartRedux = require("@redux/CartRedux");
  const WishListRedux = require("@redux/WishListRedux");
  const ProductRedux = require("@redux/ProductRedux");
  return {
    ...ownProps,
    ...stateProps,
    addCartItem: (product, variation) => {
      CartRedux.actions.addCartItem(dispatch, product, variation);
    },
    addWishListItem: (product) => {
      WishListRedux.actions.addWishListItem(dispatch, product);
    },
    removeWishListItem: (product) => {
      WishListRedux.actions.removeWishListItem(dispatch, product);
    },
    getProductVariations: (product) => {
      ProductRedux.actions.getProductVariations(dispatch, product);
    },
  };
}

export default withTheme(
  connect(
    mapStateToProps,
    undefined,
    mergeProps
  )(Detail)
);
