/** @format */

import { StyleSheet, Dimensions, I18nManager } from "react-native";
import { Styles, Color } from "@common";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    height: height / 2,
    alignItems: "center",
    justifyContent: "center",
  },
  message: {
    marginLeft: 10,
    marginRight: 10,
  },
  list: {
    flex: 1,
  },
  modalBoxWrap: {
    width: width / 1.2,
    height: 'auto',
    marginTop: 5,
    marginLeft: I18nManager.isRTL ? 70 : 0,
  },
  formContainer: {
    padding: Styles.width * 0.1,
  },
  modalClose: {
    position: 'absolute',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'red',
    right: 10,
    padding: 10,
    paddingLeft: 10,
    paddingTop: 3,
    paddingBottom: 4,
    fontWeight: 'bold',
    borderRadius: 100,
    top: 10
  },
  modalCloseText: {
    fontSize: 16,
    color: 'red',
    fontWeight: 'bold'
  },
  addReviewButton: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 35,
    height: 35,
    position: 'absolute',
    top: 20,
    right: 25,
    zIndex: 1,
    backgroundColor: '#fff',
    borderRadius: 100,
  },
  label: {
    fontWeight: "bold",
    fontSize: Styles.FontSize.medium,
    color: Color.blackTextPrimary,
    marginTop: 20,
  },
  titlex: {
    color: 'red',
  },
  title: {
    alignSelf: I18nManager.isRTL ? "flex-start" : "auto",
  },
  submitBtn: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: 40,
    marginTop: 10,
    backgroundColor: '#fff',
    borderRadius: 100,
  },
  modalContainerTitle: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 15,
    width: I18nManager.isRTL ? '60%' : '50%',
  },
  submitBtnTxt: {
    color: Color.attributes.green,
    alignItems: 'center'
  },
  input: () => ({
    borderBottomWidth: 1,
    borderColor: Color.blackTextPrimary,
    height: 20,
    marginBottom: 10,
    marginTop: 4,
    padding: 0,
    margin: 0,
    paddingLeft: 10,
    paddingBottom: 4,
    fontSize: 12,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: Color.blackTextPrimary,
  }),
  signUpButton: {
    marginTop: 20,
    backgroundColor: Color.primary,
    borderRadius: 5,
    elevation: 1,
  },
  switchWrap: {
    ...Styles.Common.RowCenterLeft,
    marginTop: 10,
  },
  text: {
    marginLeft: 10,
    color: Color.blackTextSecondary,
  },
});
