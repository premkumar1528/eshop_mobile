/** @format */

import React, { PureComponent } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Switch,
  Picker,
  LayoutAnimation,
  I18nManager,
} from "react-native";
import { connect } from "react-redux";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { Styles, Languages, Color, withTheme } from "@common";
import { toast, error, Validate, CustomToast } from "@app/Omni";
import Button from "@components/Button";
import Spinner from "@components/Spinner";
import WPUserAPI from "@services/WPUserAPI";
import API from "@services/Common";
// const labelStyle = cloneDeep(Tcomb.form.Form.stylesheet);

class AddAddress extends PureComponent {
  constructor(props) {
    super(props);

    let state = {
      username: "",
      firstname: "",
      lastname: "",
      email: "",
      telephone: "",
      fax: "",
      company: "",
      address_1: "",
      address_2: "",
      city: "",
      postcode: "",
      country: "",
      country_id: "",
      state: "",
      zone_id: "",
      password: "",
      confirm: "",
      note: "",
      cca2: "FR",
      zone_str: '',
      counries: [],
      zones: [],
      default: 1,
      isLoading: true,
      toast: {
        on: false,
        msg: ''
      },
      viewEdit: false
    };

    this.state = state;

    this.onFirstNameEditHandle = (firstname) => this.setState({ firstname });
    this.onLastNameEditHandle = (lastname) => this.setState({ lastname });

    this.onUsernameEditHandle = (username) => this.setState({ username });
    this.onEmailEditHandle = (email) => this.setState({ email });
    this.onPasswordEditHandle = (password) => this.setState({ password });

    this.ontelephone = (telephone) => this.setState({ telephone });
    this.onfax = (fax) => this.setState({ fax });
    this.oncompany = (company) => this.setState({ company });

    this.onaddress_1 = (address_1) => this.setState({ address_1 });
    this.onaddress_2 = (address_2) => this.setState({ address_2 });
    this.onstate = (state, itemIndex) => this.setState({ state, country_id: JSON.parse(state).country_id, zones: JSON.parse(state).zone });
    this.oncity = (city, itemIndex) => this.setState({ zone_str: city, zone_id: JSON.parse(city).zone_id });
    this.onpostcode = (postcode) => this.setState({ postcode });
    this.oncit = (_city) => this.setState({ city: _city });
    this.oncountry_id = (country_id) => this.setState({ country_id });

    this.onzone_id = (zone_id) => this.setState({ zone_id });
    this.onconfirm = (confirm) => this.setState({ confirm });


    this.focusLastName = () => this.lastname && this.lastname.focus();
    this.focusUsername = () => this.username && this.username.focus();
    this.focusEmail = () => this.email && this.email.focus();
    this.focusPassword = () => this.password && this.password.focus();
    this.focustelephone = () => this.telephone && this.telephone.focus();
    this.focusfax = () => this.fax && this.fax.focus();
    this.focuscompany = () => this.company && this.company.focus();
    this.focusaddress_1 = () => this.address_1 && this.address_1.focus();
    this.focusaddress_2 = () => this.address_2 && this.address_2.focus();
    this.focusstate = () => this._state && this._state.focus();
    this.focuscity = () => this.city && this.city.focus();
    this.focuspostcode = () => this.postcode && this.postcode.focus();
    this.focuscountry = () => this.country && this.country.focus();
    this.focuscountry_id = () => this.country_id && this.country_id.focus();
    this.focuszone_id = () => this.zone_id && this.zone_id.focus();
    this.focusconfirm = () => this.confirm && this.confirm.focus();
  }

  shouldComponentUpdate() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    return true;
  }

  onAddHandle = async () => {
    const { login, netInfo } = this.props;
    if (!netInfo.isConnected) return this.stopAndToast(Languages.noConnection, 2000);

    console.log('on APi register', this.state);

    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      isLoading,
    } = this.state;
    if (isLoading) return;
    this.setState({ isLoading: true });

    const _error = this.validateForm();
    if (_error) return this.stopAndToast(_error, 2000);

    const addresses = {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      _default: this.state.default,
      zone_id
    };

    let json = await API.addUserAddress(addresses);

    console.log('address', json);

    if (json === undefined) {
      return this.stopAndToast("Server don't response correctly");
    } else if (json.error) {
      return this.stopAndToast(json.error.message);
    } else if (json.success) {
      this.stopAndToast(json.success.message)
      this.setState({ isLoading: false });
      addresses.address_id = json.address_id;
      addresses.first_name = firstname;
      addresses.last_name = lastname;
      this.props.addAddress(addresses);
      // this.props.initAllAddresses(addresses);
      // toast("Address Added Success fully.");
      this.props.onBack();
    } else {
      this.stopAndToast("Can't register user, please try again.");
    }
  };


  onUpdateHandle = async () => {
    const { login, netInfo } = this.props;
    if (!netInfo.isConnected) return this.stopAndToast(Languages.noConnection, 2000);

    console.log('on APi register', this.state);

    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      isLoading,
    } = this.state;
    if (isLoading) return;
    this.setState({ isLoading: true });

    const _error = this.validateForm();
    if (_error) return this.stopAndToast(_error, 2000);

    const addresses = {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      _default: this.state.default,
      zone_id
    };

    let json = await API.updateUserAddress(addresses);

    console.log('address', json);

    if (json === undefined) {
      this.stopAndToast("Server don't response correctly");
    } else if (json.error) {
      this.stopAndToast(json.error.message);
    } else if (json.success) {
      this.stopAndToast(json.success.message)
      this.setState({ isLoading: false });
      addresses.address_id = json.address_id;
      addresses.first_name = firstname;
      addresses.last_name = lastname;
      this.props.initAllAddresses(addresses);
      const address = await API.getUserAddress();
      if (address && address.success) {
        // this.props.initAllAddresses(address.addresses)
      }
      this.stopAndToast("Address Updated Successfully.");
      this.props.onBack();
    } else {
      this.stopAndToast("Can't update user address, please try again.");
    }
  };

  componentDidMount = async () => {
    console.log('this.props', this.props)
    const { navigation } = this.props;
    let params = navigation.getParam('edit');

    if (this.props.countries && this.props.countries.list) {
      this.setState({ counries: this.props.countries.list })
    } else {
      const json = await WPUserAPI.getCounrys();
      console.log('currency result', json)
      if (json === undefined) {
        return this.stopAndToast("Server don't response correctly");
      } else if (json.error) {
        return this.stopAndToast(json.error.message);
      }

      this.setState({ counries: json.countries })
    }

    if (params) {
      await this.setState({
        viewEdit: params
      });

      let formValue = {
        username,
        firstname,
        lastname,
        email,
        telephone,
        company,
        address_1,
        address_2,
        city,
        postcode,
        country,
        country_id,
        zone_id
      } = params; // missing state and tax
      formValue['state'] = formValue.country;
      if (this.props.user && this.props.user.user) {
        formValue['tax'] = this.props.user.user.fax;
        formValue['telephone'] = this.props.user.user.telephone;
      }

      this.setState(formValue, () => {
        console.log(this.state);
      });

      console.log('form value', formValue);
      try {
        if (this.state.counries.length > 0) {
          let filterCountry = this.state.counries.filter(con => con.country_id == formValue.country_id);
          let zones = [];
          console.log('filterCountry', filterCountry)
          if (filterCountry.length > 0) {
            zones = filterCountry[0].zone.filter(zone => zone.zone_id == formValue.zone_id);
            console.log('zones', zones)
            if (zones.length > 0) {
              this.setState({
                zones: filterCountry[0].zone,
                state: JSON.stringify(filterCountry[0]),
                zone_str: JSON.stringify(zones[0]),
              })
            }
          }
        }
      } catch (error) {
        console.log('error', error)
      }
    }

    this.setState({ isLoading: false });
  }

  validateForm = () => {
    console.log('validateForm', this.state)

    const {
      username,
      firstname,
      lastname,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      city,
      postcode,
      country,
      country_id,
      state,
      zone_id,
      password,
      confirm,
    } = this.state;

    if (
      Validate.isEmpty(
        firstname,
        lastname,
        telephone,
        address_1,
        country_id,
        zone_id,
        postcode,
      )
    ) {
      // check empty
      return "Please complete the form";
    }
    // else if (firstname.length > 0 && firstname.length < 33) {
    //   return "First Name must be between 1 and 32 characters!";
    // }
    // else if (lastname.length > 0 && lastname.length < 33) {
    //   return "Last Name must be between 1 and 32 characters";
    // }
    return undefined;
  };

  stopAndToast = (msg) => {
    this.setState({
      toast: { on: true, msg: msg }
    }, () => {
      setTimeout(() => {
        this.setState({ toast: { on: false, msg: '' } })
      }, 2000)
    })
    this.setState({ isLoading: false });
  };

  render() {
    console.log('props', this.props)
    const {
      username,
      firstname,
      lastname,
      email,
      telephone,
      fax,
      company,
      address_1,
      address_2,
      state,
      city,
      postcode,
      counries,
      zones,
      zone_str,
      useGeneratePass,
      isLoading,
      viewEdit,
    } = this.state;

    const {
      theme: {
        colors: { background, text, placeholder },
      },
    } = this.props;

    // console.log('this.state', this.state)
    return (
      <View style={[styles.container, { backgroundColor: background }]}>
        {isLoading ? <Spinner mode="overlay" /> : null}
        <CustomToast visible={this.state.toast.on} message={this.state.toast.msg} />
        <KeyboardAwareScrollView
          showsVerticalScrollIndicator={false}
          enableOnAndroid>
          <View style={styles.formContainer}>
            <Text style={[styles.label, { color: text }]}>
              {viewEdit ? Languages.EditAddress : Languages.AddNewAddress}
            </Text>
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.firstname = comp)}
              placeholder={Languages.firstName}
              onChangeText={this.onFirstNameEditHandle}
              onSubmitEditing={this.focusLastName}
              autoCapitalize="words"
              returnKeyType="next"
              value={firstname}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.lastname = comp)}
              placeholder={Languages.lastName}
              onChangeText={this.onLastNameEditHandle}
              onSubmitEditing={this.focustelephone}
              autoCapitalize="words"
              returnKeyType="next"
              value={lastname}
              placeholderTextColor={placeholder}
            />

            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.telephone = comp)}
              placeholder={Languages.telephone}
              onChangeText={this.ontelephone}
              onSubmitEditing={this.focusfax}
              returnKeyType="next"
              keyboardType='numeric'
              value={telephone}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.fax = comp)}
              placeholder={Languages.fax}
              onChangeText={this.onfax}
              onSubmitEditing={this.focuscompany}
              autoCapitalize="words"
              returnKeyType="next"
              value={fax}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.company = comp)}
              placeholder={Languages.company}
              onChangeText={this.oncompany}
              onSubmitEditing={this.focusaddress_1}
              autoCapitalize="words"
              returnKeyType="next"
              value={company}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.address_1 = comp)}
              placeholder={Languages.address_1}
              onChangeText={this.onaddress_1}
              onSubmitEditing={this.focusaddress_2}
              autoCapitalize="words"
              returnKeyType="next"
              value={address_1}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.address_2 = comp)}
              placeholder={Languages.address_2}
              onChangeText={this.onaddress_2}
              onSubmitEditing={this.focuscountry}
              autoCapitalize="words"
              returnKeyType="next"
              value={address_2}
              placeholderTextColor={placeholder}
            />
            <Picker
              selectedValue={state}
              style={styles.select(text)}
              ref={(comp) => (this._state = comp)}
              onValueChange={this.onstate}>
              <Picker.Item label={'Select Country'} value={null} />
              {
                counries.map((value, index) => (
                  <Picker.Item key={index} label={value.name} value={JSON.stringify(value)} />
                ))
              }
            </Picker>
            <Picker
              selectedValue={zone_str}
              style={styles.select(text)}
              ref={(comp) => (this._state = comp)}
              onValueChange={this.oncity}>
              <Picker.Item label={'Select Zone'} value={null} />
              {
                zones.map((value, index) => (
                  <Picker.Item key={index} label={value.name} value={JSON.stringify(value)} />
                ))
              }
            </Picker>
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this._state = comp)}
              placeholder={Languages.city}
              onChangeText={this.oncit}
              onSubmitEditing={this.focuspostcode}
              autoCapitalize="words"
              returnKeyType="next"
              value={city}
              placeholderTextColor={placeholder}
            />
            <TextInput
              style={styles.input(text)}
              underlineColorAndroid="transparent"
              ref={(comp) => (this.postcode = comp)}
              placeholder={Languages.postcode}
              onChangeText={this.onpostcode}
              autoCapitalize="words"
              keyboardType='numeric'
              returnKeyType="done"
              value={postcode}
              placeholderTextColor={placeholder}
            />
            {
              viewEdit ?
                <Button
                  containerStyle={styles.signUpButton}
                  text={Languages.UpdateAddress}
                  onPress={this.onUpdateHandle.bind(this)}
                /> :
                <Button
                  containerStyle={styles.signUpButton}
                  text={Languages.AddToAddress}
                  onPress={this.onAddHandle.bind(this)}
                />
            }
          </View>

        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  formContainer: {
    padding: Styles.width * 0.1,
  },
  label: {
    fontWeight: "bold",
    fontSize: Styles.FontSize.medium,
    color: Color.blackTextPrimary,
    marginTop: 20,
  },

  input: (text) => ({
    borderBottomWidth: 1,
    borderColor: text,
    height: 40,
    marginTop: 10,
    padding: 0,
    margin: 0,
    // flex: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: text,
  }),
  select: (text) => ({
    borderColor: text,
    height: 40,
    marginTop: 20,
    padding: 0,
    margin: 0,
    paddingLeft: -10,
    // flex: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: text,
  }),
  signUpButton: {
    marginTop: 20,
    backgroundColor: Color.primary,
    borderRadius: 5,
    elevation: 1,
  },
  switchWrap: {
    ...Styles.Common.RowCenterLeft,
    marginTop: 10,
  },
  text: {
    marginLeft: 10,
    color: Color.blackTextSecondary,
  },
});

const mapStateToProps = (state) => {
  return {
    netInfo: state.netInfo,
    user: state.user,
    countries: state.countries
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const { actions } = require("@redux/AddressRedux");

  return {
    ...ownProps,
    ...stateProps,
    addAddress: (address) => actions.addAddress(dispatch, address),
    initAllAddresses: (address) => actions.initAllAddresses(dispatch, address),
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(AddAddress));
