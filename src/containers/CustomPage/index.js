/** @format */

import React, { Component } from "react";
import WebView from "@components/WebView/WebViewSrc";

export default class CustomPage extends Component {
  constructor(props) {
    super(props);
    this.state = { src: "http://google.com/" };
    this.fetchPage = this.fetchPage.bind(this);
  }

  componentDidMount() {
    // this.fetchPage(this.props.id);
    console.log(this.props)
  }

  componentWillReceiveProps(nextProps) {
    // this.fetchPage(nextProps.id);
    console.log(nextProps)
  }

  fetchPage(id) {
  
  }

  render() {
    return <WebView data={this.props.data} />;
  }
}
