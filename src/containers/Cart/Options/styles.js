/** @format */

import { StyleSheet, Dimensions } from "react-native";
import { Color, Constants } from "@common";

const color = Color.Text;

const { height, width } = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: "white",
    flexWrap: "wrap",
    width: width,
  },
  scrollView: {
    width,
    marginBottom: 60
  },
  paymentOption: {
    marginTop: 40,
    flexDirection: "row",
    flexWrap: "wrap",
    width,
    justifyContent: "center",
  },
  shipmentOption: {
    marginTop: 20,
    flexWrap: "wrap",
    paddingBottom: 10,
    marginLeft: 50,
    justifyContent: "center",
  },
  optionContainer: {
    width: width / 2 - 10,
    height: 90,
    justifyContent: "center",
    alignItems: "center",
  },
  btnOption: {
    width: 80,
    height: 60,
  },
  selectedBtnOption: {
    width: width / 2 - 30,
    height: width / 3 - 40,
    backgroundColor: "rgba(206, 215, 221, 0.6)",
    padding: 10,
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 6,
  },
  imgOption: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: "contain",
  },
  message: {
    fontSize: 13,
    color: "#333",
    textAlign: "center",
    padding: 30,
    marginTop: 0,
    paddingTop: 30,
    fontFamily: Constants.fontFamily,
  },

  modalContainer: {
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    height: height,
    width: width,
  },
  modalBody: {
    backgroundColor: '#fff',
    height: 'auto',
    width: width / 1.3,
    marginLeft: (width / 1.5) / 6,
    marginTop: (height / 4),
    borderRadius: 5,
    alignItems: 'center',
  },
  modalTite: {
    textAlign: 'center',
    paddingTop: 20,
    paddingBottom: 10,
    fontSize: 16,
    color,
  },
  modalHeaderTitle: {
    fontSize: 16,
    color,
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 10,
  },
  modalcontentContainer: {
    alignItems: 'center',
    marginTop: 10,
  },
  modalConfirmBtn: {
    width: width / 1.3,
    backgroundColor: 'green',
    height: 44,
    justifyContent: 'center',
    marginTop: 20,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
  },
  modalConfirmBtnText: {
    textAlign: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 16,
    color: '#fff',
    fontWeight: 'bold'
  },
  modalDesc: {
    fontSize: 16,
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
  },
  modalContentImage: {
    height: 80,
    width: 80,
    marginBottom: 20,
  },

  formCard: {
    marginTop: 10,
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 10,
  },
  btnNextContainer: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  btnNext: {
    marginBottom: 20,
    backgroundColor: "#0091ea",
    height: 40,
    width: 200,
    borderRadius: 20,
  },
  shipContainer: {
    paddingLeft: 10,
    paddingBottom: 20,
  },
  shipTitle: {
    fontSize: 16,
    fontFamily: Constants.fontFamily,
    color,
  },
  checkImage: {
    height: 20,
    width: 20,
    marginLeft: 5,
  },
  shipbutton: {
    paddingLeft: 20,
    paddingTop: 5,
    flexDirection: 'row',
  },
  shipSelected: {
  },
  shipText: {
    fontSize: 13,
    fontFamily: Constants.fontFamily,
    color: '#869681',
  },
  btnNextText: {
    fontWeight: "bold",
  },
  label: {
    fontSize: 18,
    color: Color.Text,
    fontFamily: Constants.fontHeader,
    paddingLeft: 15,
  },
  descriptionView: {
    marginTop: 20,
  },
});
