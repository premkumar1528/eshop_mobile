/** @format */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Text, Dimensions, Image, ScrollView, View, Modal, Alert, TouchableOpacity, Platform } from "react-native";
import css from "@cart/styles";
import { connect } from "react-redux";
import { warn, toast } from "@app/Omni";
import { Button, ConfirmCheckout } from "@components";
import { Languages, Config, Images, withTheme } from "@common";
import Buttons from "@cart/Buttons";
import { WooWorker } from "api-ecommerce";
import { LogoSpinner } from "@components";
import HTML from "react-native-render-html";
import styles from "./styles";
import API from "@services/Common";

const { width } = Dimensions.get("window");

class Options extends PureComponent {
  static propTypes = {
    fetchPayments: PropTypes.func,
    message: PropTypes.array,
    type: PropTypes.string,
    cleanOldCoupon: PropTypes.func,
    onNext: PropTypes.func,
    user: PropTypes.object,
    userInfo: PropTypes.object,
    currency: PropTypes.any,
    payments: PropTypes.object,
    isLoading: PropTypes.bool,
    cartItems: PropTypes.any,
    onShowCheckOut: PropTypes.func,
    emptyCart: PropTypes.func,
    couponCode: PropTypes.any,
    couponId: PropTypes.any,
    couponAmount: PropTypes.any,
    shippingMethod: PropTypes.any,
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      // token: null,
      selectedIndex: 0,
      selectedShipIndex: 0,
      selectedShipmet: false,
      selectedShipment: false,
      paymentShipment: false,
      confirmModal: false,
      // accountNumber: '',
      // holderName: '',
      // expirationDate: '',
      // securityCode: '',
      // paymentState: '',
      // createdOrder: {},
    };
    this.response_status = {};
  }

  async componentWillMount () {
    await this.props.fetchShipments();
    // this.props.fetchPayments();
    console.log('called fetchPayments')
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.message && nextProps.message.length > 0) {
      // Alert.alert(Languages.Error, nextProps.carts.message)
      toast(nextProps.message);
    }

    if (
      nextProps.type !== this.props.type &&
      nextProps.type == "CREATE_NEW_ORDER_SUCCESS"
    ) {
      warn(nextProps);
      this.props.cleanOldCoupon();
      this.props.onNext();
    }
  }

  nextStep = async () => {
    const { user, token } = this.props.user;
    const { list, shipMethod } = this.props.payments;
    const { deleiveryInfo, currency } = this.props;
    const { selectedShipment, selectedIndex, selectedShipmet } = this.state;
    // const coupon = this.getCouponInfo();

    console.log('Order Confirm', this.props, this.state)

    if (shipMethod && shipMethod.length > 0 && selectedShipment === false) {
      return false;
    }

    if (list && list.length > 0 && selectedIndex === false) {
      return false;
    }

    // "language_id": "1",
    // "language_code": "en",
    // "cart_id": "4-1113",

    const payload = {
      customer: {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        telephone: user.telephone,
        fax: user.fax,
      },
      shipping_method: {
        code: shipMethod[selectedShipment].quote[0].code,
      },
      payment_method: {
        code: list[selectedIndex].code
      },
      comment: '',
      user_agent: Platform.OS,
      post_type: ""
    };

    console.log('check out', payload)

    // check the shipping info
    if (Config.shipping.visible) {
      // payload.shipping_lines = this.getShippingMethod();
    }

    // check the coupon
    // if (coupon.length !== 0) {
    //   // payload.coupon_lines = this.getCouponInfo();
    // }

    // this.setState({ loading: this.props.isLoading });

    // warn([userInfo, payload]);

    if (list[selectedIndex].code === "cod" || list[selectedIndex].code === "free_checkout") {
      this.setState({ loading: true });

      let response = await API.addOrder(payload);
      console.log('addOrder', response)
      if (response && response.success) {
        toast(response.success.message);
        this.setState({ loading: false });

        // Alert.alert(
        //   'Order',
        //   'Order Confirmed!',
        //   [
        //     // {
        //     //   text: 'No',
        //     //   onPress: () => console.log('Cancel Pressed'),
        //     //   style: 'cancel',
        //     // },
        //     {
        //       text: 'Okay', onPress: async () => {
        //         let status = await API.confirmOrder(response.order_id);
        //         console.log('confirmOrder', status)
        //         if (status.success) {
        //           this.props.emptyCart();
        //           this.props.onNext();
        //         } else {
        //           this.setState({ loading: false });
        //         }
        //       }
        //     },
        //   ],
        //   { cancelable: false },
        // );

        this.response_status = response;
        this.setState({ confirmModal: true })


      } else if (response && response.error) {
        toast(response.error.message);
      } else {
        toast("Something went wrong...");
      }
      this.setState({ loading: false });
    } else {
      // other kind of payment
      // this.props.onNext();
      toast('Other Payment Method');
    }
  };

  orderConfirm = async () => {
    let status = await API.confirmOrder(this.response_status.order_id);
    console.log('confirmOrder', status)
    if (status.success) {
      this.props.emptyCart();
      this.props.onNext();
    } else {
      this.setState({ confirmModal: false })
      toast("Please try again after some times");
    }
    this.setState({ confirmModal: false })
  }

  getItemsCart = () => {
    const { cartItems } = this.props;
    const items = [];
    for (let i = 0; i < cartItems.length; i++) {
      const cartItem = cartItems[i];

      const item = {
        product_id: cartItem.product.id,
        quantity: cartItem.quantity,
      };

      if (cartItem.variation !== null) {
        item.variation_id = cartItem.variation.id;
      }
      items.push(item);
    }
    return items;
  };

  getCouponInfo = () => {
    const { couponCode, couponAmount } = this.props;
    if (
      typeof couponCode !== "undefined" &&
      typeof couponAmount !== "undefined" &&
      couponAmount > 0
    ) {
      return [
        {
          code: couponCode,
        },
      ];
    }
    return {};
  };

  getShippingMethod = () => {
    const { shippingMethod } = this.props;

    if (typeof shippingMethod !== "undefined") {
      return [
        {
          method_id: `${shippingMethod.method_id}:${shippingMethod.id}`,
          method_title: shippingMethod.title,
          total:
            shippingMethod.id === "free_shipping" ||
              shippingMethod.method_id === "free_shipping"
              ? "0"
              : shippingMethod.settings.cost.value,
        },
      ];
    }
    // return the free class as default
    return [
      {
        method_id: "free_shipping",
        total: "0",
      },
    ];
  };



  renderDesLayout = (item) => {
    if (typeof item === "undefined") {
      return <View />;
    }
    if (item.terms === null || item.terms === "") return <View />;

    const tagsStyles = {
      p: {
        color: "#666",
        flex: 1,
        textAlign: "center",
        width: width,
        // paddingLeft: 20,
      },
    };
    return (
      <View style={styles.descriptionView}>
        <HTML tagsStyles={tagsStyles} html={`<p>${item.terms}</p>`} />
      </View>
    );
  };

  getShipping = async (ship, index) => {
    await this.props.fetchPayments({ shipping_method: { code: ship.code } });
    const { list } = this.props.payments;
    if (list.length > 0) {
      this.setState({ paymentShipment: false, selectedShipment: index, selectedShipmet: ship });
    } else {
      this.setState({ paymentShipment: false, selectedShipment: false, selectedShipmet: ship });
    }
  }

  render() {
    const { list, shipMethod } = this.props.payments;
    const {
      theme: {
        colors: { text },
      },
    } = this.props;

    const { selectedShipment, paymentShipment, selectedShipmet, confirmModal } = this.state;

    console.log('payment method', this.state);


    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.scrollView}
        >
          <View style={css.rowEmpty}>
            <Text style={[styles.label, { color: text }]}>
              {Languages.SelectShipment}:
            </Text>
          </View>

          <View style={styles.shipmentOption}>
            {shipMethod && shipMethod.map((item, index) => {
              return (
                <View style={styles.shipContainer} key={index.toString()}>
                  <Text style={styles.shipTitle}>
                    {item.title}:
                  </Text>
                  <TouchableOpacity style={styles.shipbutton}
                    onPress={() => this.getShipping(item.quote[0], index)}
                  >
                    <Text style={styles.shipText}>
                      {item.quote[0].title} - {item.quote[0].text}
                    </Text>
                    <Image
                      source={selectedShipment === index ? Images.IconShipCheckbox : Images.IconShipCheckGraybox}
                      style={[styles.checkImage]}
                    ></Image>
                  </TouchableOpacity>
                </View>
              );
            })}
            {
              !shipMethod &&
              <View style={css.rowEmpty}>
                <Text style={[styles.label, { color: text, fontSize: 12 }]}>
                  {Languages.NoMethod}
                </Text>
              </View>
            }
          </View>
          {
            selectedShipment !== false &&
            <View style={css.rowEmpty}>
              <Text style={[styles.label, { color: text }]}>
                {Languages.SelectPayment}
              </Text>
            </View>
          }
          {
            selectedShipment !== false &&
            <View style={styles.paymentOption}>
              {list.map((item, index) => {

                const image =
                  typeof Config.Payments[item.code] !== "undefined" &&
                  Config.Payments[item.code];
                return (
                  <View style={styles.optionContainer} key={index.toString()}>
                    <Button
                      type="image"
                      source={image}
                      defaultSource={Images.defaultPayment}
                      onPress={() => this.setState({ selectedIndex: index })}
                      title={item.title}
                      buttonStyle={[
                        styles.btnOption,
                        this.state.selectedIndex == index &&
                        styles.selectedBtnOption,
                      ]}
                      imageStyle={styles.imgOption}
                    />
                  </View>
                );
              })}
            </View>
          }
          {this.renderDesLayout(list[this.state.selectedIndex])}

          <Modal
            animationType="slide"
            transparent
            visible={confirmModal}
            onShow={this.onShow}>
            <View style={styles.modalContainer}>
              <View style={styles.modalBody}>
                <Text style={styles.modalHeaderTitle}>Your Order Successfully Placed</Text>
                <View style={styles.modalcontentContainer}>
                  <Image
                    source={Images.IconTick}
                    style={[styles.modalContentImage]}
                  ></Image>
                  <Text style={styles.modalDesc}>
                    You can view your order history by going to the my account page and by clicking on history.
                    If your purchase has an associated download, you can go to the account downloads page to view them.
                    Please direct any questions you have to the store owner.
                    Thanks for shopping with us online!
                </Text>
                  <TouchableOpacity style={styles.modalConfirmBtn}
                    onPress={this.orderConfirm}
                  >
                    <Text style={styles.modalConfirmBtnText}>Continue</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
          <ConfirmCheckout
            couponAmount={this.props.couponAmount}
            discountType={this.props.discountType}
            shippingMethod={selectedShipmet}
            totalPrice={this.props.totalPrice}
          />
        </ScrollView>

        <Buttons
          isAbsolute
          onPrevious={this.props.onPrevious}
          isLoading={this.state.loading}
          disabled={!paymentShipment}
          nextText={Languages.ConfirmOrder}
          onNext={this.nextStep}
        />
      </View>
    );
  }
}

const mapStateToProps = ({ payments, carts, user, products, currency }) => {
  return {
    payments,
    user,
    type: carts.type,
    cartItems: carts.cartItems,
    totalPrice: carts.totalPrice,
    message: carts.message,
    customerInfo: carts.customerInfo,

    couponCode: products.coupon && products.coupon.code,
    couponAmount: products.coupon && products.coupon.amount,
    discountType: products.coupon && products.coupon.type,
    couponId: products.coupon && products.coupon.id,

    shippingMethod: carts.shippingMethod,

    currency,
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CartRedux = require("@redux/CartRedux");
  const productActions = require("@redux/ProductRedux").actions;
  const paymentActions = require("@redux/PaymentRedux").actions;

  return {
    ...ownProps,
    ...stateProps,
    emptyCart: () => CartRedux.actions.emptyCart(dispatch),
    createNewOrder: (payload) => {
      CartRedux.actions.createNewOrder(dispatch, payload);
    },
    cleanOldCoupon: () => {
      productActions.cleanOldCoupon(dispatch);
    },
    fetchPayments: (code) => {
      paymentActions.fetchPayments(dispatch, code);
    },
    fetchShipments: () => {
      paymentActions.fetchShipments(dispatch);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(Options));
