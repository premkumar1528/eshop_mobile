/** @format */

import { StyleSheet, Dimensions, Platform } from "react-native";

import { Color, Styles } from "@common";

const { width, height } = Dimensions.get("window");

const vh = height / 100;
const color = Color.Text;

export default StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: "white",
  },
  address_container: {
    height: 64,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 130,
  },
  internalPickerContainer: {
    flex: Platform.OS === 'ios' ? 1 : null, // for Android, not visible otherwise.
    width: Platform.OS === 'ios' ? undefined : width / 1.6,
  },
  imageStyle: {
    height: 15,
    width: 15,
    paddingRight: 5,
  },
  editAddress: {
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 5,
    borderRadius: 0,
    marginTop:-6,
  },
  editAddressText: {
    fontSize: 14,
    textAlign: 'center',
    flex: 0.4,
    paddingLeft: 5,
    paddingRight: 5,
    color,
  },
  pickerIosListItemContainer: {
    flex: 1,
    height: 60,
    justifyContent: 'space-between',
    textAlign: 'center',
    alignItems: 'center',
  },
  pickerIosListItemText: {
    fontSize: 16,
    alignItems: 'center',
  },
  rowEmpty: {
    paddingLeft: 20,
  },

  rowLabelText: {
    fontSize: 15,
    color
  },
  btnNextContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 20,
    marginTop: 30,
  },
  btnNext: {
    backgroundColor: "#0091ea",
    height: 40,
    width: 200,
    borderRadius: 20,
  },
  btnNextText: {
    fontWeight: "bold",
  },
  form: {
    marginBottom: 30,
  },
  picker: {
    width: width - 80,
  },
  formContainer: {
    padding: 20,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20,
    borderWidth: 1,
    borderColor: '#f1f1f1',
  },
  formRow: {
    flex: 1,
    flexDirection: 'row',
    paddingBottom: 5,
    paddingTop: 5,
  },
  formLable: {
    fontSize: 18,
    color,
  },
  formValue: {
    fontSize: 14,
    color,
    paddingTop: 3,
    paddingLeft: 5,
  },
});
