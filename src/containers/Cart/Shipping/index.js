/** @format */

import React, { PureComponent } from "react";
import { Text, View, Image, ScrollView, Picker, TouchableOpacity } from "react-native";
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from "react-redux";
import Tcomb from "tcomb-form-native";
import { cloneDeep } from "lodash";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { ShippingMethod } from "@components";

import API from '@services/Common';

import Buttons from "@cart/Buttons";
import { Images, Languages, withTheme, Styles } from "@common";
import { toast } from "@app/Omni";
import css from "@cart/styles";
import styles from "./styles";
import { LogoSpinner } from "@components";
const Form = Tcomb.form.Form;

const customStyle = cloneDeep(Tcomb.form.Form.stylesheet);
const customInputStyle = cloneDeep(Tcomb.form.Form.stylesheet);

class Shipping extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        first_name: "",
        last_name: "",
        address_1: "",
        address_2: "",
        country: "",
        state: "",
        city: "",
        postcode: "",
        email: "",
        phone: "",
        company: "",
        iso_code_3: "",
        zone: "",
      },
      selectedAdd: {},
      loading: false,
    };

    this.form = {};

  }

  componentDidMount() {
    console.log('this.props', this.props)
    const { addresses: { list, selectedAddress } } = this.props;
    if (list.length > 0) {
      this.setState({ selectedAdd: this.updateNewAddress(selectedAddress) })
    }
  }

  componentWillReceiveProps(nextProps, prevProps) {
    console.log('componentWillReceiveProps, address', nextProps)
    if (prevProps.selectedAddress !== nextProps.selectedAddress) {
      this.updateNewAddress(nextProps.selectedAddress);
    }
  }

  onPress = () => this.refs.form.getValue();

  updateNewAddress = (selectedAddress) => {
    if (selectedAddress) {
      this.form = {
        address_id: selectedAddress.address_id,
        language_code: selectedAddress.language_code,
        first_name: selectedAddress.first_name,
        firstname: selectedAddress.first_name,
        lastname: selectedAddress.last_name,
        last_name: selectedAddress.last_name,
        address_1: selectedAddress.address_1,
        address_2: selectedAddress.address_2,
        country: selectedAddress.country,
        country_id: selectedAddress.country_id,
        state: selectedAddress.state,
        city: selectedAddress.city,
        postcode: selectedAddress.postcode,
        company: selectedAddress.company,
        iso_code_3: selectedAddress.iso_code_3,
        zone: selectedAddress.zone,
        zone_id: selectedAddress.zone_id,
      }
    };
    return selectedAddress.address_id;
  }

  nextStep = async () => {
    // if validation fails, value will be null
    this.setState({ loading: true })
    let res = await API.assignShippingAddressToCart(this.form);
    console.log('assignPaymentAddressToCart', res);
    // this.props.onNext({ ...this.form }) ;  // testing purpose

    if (res && res.success) {
      await this.props.fetchShipments();
      toast(res.success.message)
      this.setState({ loading: false });
      this.props.onNext({ ...this.form });
    } else if (res.error) {
      this.setState({ loading: false });
      toast(res.error.message)
    }
  };

  render() {

    const {
      theme: {
        colors: { text },
      },
      addresses: { list, selectedAddress }
    } = this.props;

    const { loading } = this.state;

    // const _form = this.updateNewAddress(selectedAddress);
    const form = this.form;

    // console.log('payment address screen render', this.state.selectedAdd, list);
    // console.log('payment address screen render', form);


    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView style={styles.form} enableOnAndroid>
          {loading && <LogoSpinner fullcover />}
          <View style={css.rowEmpty}>
            <Text style={[css.label, { color: text }]}>
              {Languages.YourShippingInfo}
            </Text>
            <TouchableOpacity
              style={[styles.editAddress, Styles.Common.boxShadow]}
              onPress={() => this.props.navigateAddAddress('Address')}>
              <Text style={styles.editAddressText}>
                <Image resizeMode="contain" source={Images.IconEditPage} style={styles.imageStyle} />
                {Languages.EditButton}
              </Text>
            </TouchableOpacity>
          </View>

          <View style={styles.address_container}>
            <Text style={styles.rowLabelText}>Selected Address:</Text>
            <Picker
              selectedValue={this.state.selectedAdd}
              style={styles.internalPickerContainer}
              mode="dropdown"
              itemStyle={styles.pickerIosListItemContainer}
              itemTextStyle={styles.pickerIosListItemText}
              onValueChange={(itemValue, itemIndex) => {
                console.log(itemValue, this.form);
                this.setState({ selectedAdd: this.updateNewAddress(list[itemIndex]) });
              }}>
              {
                list.map((item) => (
                  <Picker.Item label={item.first_name + ' ' + item.last_name} value={item.address_id} />
                ))
              }
            </Picker>
          </View>

          <ScrollView style={styles.formContainer}>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>First Name: </Text>
              <Text style={styles.formValue}>{form.first_name}</Text>
            </View>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Last Name: </Text>
              <Text style={styles.formValue}>{form.last_name}</Text>
            </View>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Address 1: </Text>
              <Text style={styles.formValue}>{form.address_1}</Text>
            </View>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Address 2: </Text>
              <Text style={styles.formValue}>{form.address_2}</Text>
            </View>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Country: </Text>
              <Text style={styles.formValue}>{form.country}</Text>
            </View>
            {/* <View style={styles.formRow}>
              <Text style={styles.formLable}>State: </Text>
              <Text style={styles.formValue}>{form.state}</Text>
            </View> */}
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Zone: </Text>
              <Text style={styles.formValue}>{form.zone}</Text>
            </View>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>City: </Text>
              <Text style={styles.formValue}>{form.city}</Text>
            </View>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Postal Code: </Text>
              <Text style={styles.formValue}>{form.postcode}</Text>
            </View>
            {/* <View style={styles.formRow}>
              <Text style={styles.formLable}>Email: </Text>
              <Text style={styles.formValue}>{form.email}</Text>
            </View>
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Phone: </Text>
              <Text style={styles.formValue}>{form.phone}</Text>
            </View> */}
            <View style={styles.formRow}>
              <Text style={styles.formLable}>Company: </Text>
              <Text style={styles.formValue}>{form.company}</Text>
            </View>

            <View style={styles.formRow}>
              <Text style={styles.formLable}>ISO Code: </Text>
              <Text style={styles.formValue}>{form.iso_code_3}</Text>
            </View>
          </ScrollView>
        </KeyboardAwareScrollView>

        <Buttons
          isAbsolute
          onPrevious={this.props.onPrevious}
          onNext={this.nextStep}
        />
      </View>
    );
  }
}

Shipping.defaultProps = {
  shippings: [],
  selectedAddress: {},
  addresses: [],
};

const mapStateToProps = ({ carts, user, addresses, currency }) => {
  return {
    user,
    customerInfo: carts.customerInfo,
    message: carts.message,
    type: carts.type,
    isFetching: carts.isFetching,
    selectedAddress: addresses.selectedAddress,
    addresses: addresses,
    currency
  };
};

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CartRedux = require("@redux/CartRedux");
  const AddressRedux = require("@redux/AddressRedux");
  const paymentActions = require("@redux/PaymentRedux").actions;

  return {
    ...ownProps,
    ...stateProps,
    validateCustomerInfo: (customerInfo) => {
      CartRedux.actions.validateCustomerInfo(dispatch, customerInfo);
    },
    getShippingMethod: (zoneId) => {
      CartRedux.actions.getShippingMethod(dispatch, zoneId);
    },
    selectShippingMethod: (shippingMethod) => {
      CartRedux.actions.selectShippingMethod(dispatch, shippingMethod);
    },
    updateSelectedAddress: (address) => {
      AddressRedux.actions.updateSelectedAddress(dispatch, address);
    },
    fetchShipments: () => {
      paymentActions.fetchShipments(dispatch);
    },
  };
}

export default connect(
  mapStateToProps,
  undefined,
  mergeProps
)(withTheme(Shipping));
