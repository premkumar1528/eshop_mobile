/**
 * Created by Premji on 19/02/2017.
 *
 * @format
 */

import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { View, Text, I18nManager, Dimensions } from "react-native";
import { connect } from "react-redux";
import { Constants, withTheme, AppConfig, Languages } from "@common";
import { HorizonList, ModalLayout, PostList } from "@components";
import WPUserAPI from "@services/WPUserAPI";
import Spinner from "@components/Spinner";
import API from "@services/Common";
import { isEmpty } from "lodash";
import styles from "./styles";
import { warn } from '@app/Omni';
import AsyncStorage from "@react-native-community/async-storage";

const { width, height } = Dimensions.get("window");

class Home extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    }
  }

  static propTypes = {
    fetchAllCountries: PropTypes.func.isRequired,
    layoutHome: PropTypes.any,
    onViewProductScreen: PropTypes.func,
    onShowAll: PropTypes.func,
    showCategoriesScreen: PropTypes.func,
  };

  async componentDidMount() {
    // Language and currency
    try {
      // const lang = await WPUserAPI.getLang(1, 'en');
      // const cur = await WPUserAPI.getCurrency(1, 'en');

      await this.refreshLayout();

    } catch (error) { console.log(error) };

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currency !== this.props.currency) {
      this.refreshLayout();
    }
  }

  refreshLayout = async () => {
    let token = await WPUserAPI.getSetToken();
    console.log('devices width and height', width, height)
    // Lower device - devices width and height 360 592

    if (this.props.user && this.props.user.token) {
      await WPUserAPI.getSetToken(this.props.user.token);
      token = await WPUserAPI.getSetToken();
      this.props.fetchMyOrder(this.props.user);

      const address = await API.getUserAddress();
      console.log('address', address)
      if (address && address.success) {
        this.props.initAllAddresses(address.addresses)
      }

      if (this.props.addresses && this.props.addresses.length > 0) {
        this.props.addresses.map(item => {
          if (item.is_default) this.props.selectAddress(item);
        })
      }

    }


    try {
      let prods = await API.getHomeData();
      console.log('Home data', prods);
      if (prods) {
        if (prods.success) {
          this.props.load_products(prods.modules);
          await this.props._languages(prods.languages);
          await this.props._currency(prods.currencies)
        }
      }

      await WPUserAPI.getSetLocalData({
        language_id: this.props.language.language_id,
        language_code: this.props.language.lang,
        currency: this.props.currency.code,
      });


      let cartIdValidate = await WPUserAPI.getSetCardId();

      if (cartIdValidate === undefined || cartIdValidate === null || cartIdValidate === false) {
        let cartId = {};
        if (this.props.user) {
          cartId = await API.createCartId();
        } else {
          cartId = await API.getCartId();
        }
        if (typeof cartId.cart_id === 'string') {
          await AsyncStorage.setItem("@cartId", JSON.stringify(cartId.cart_id), async () => {
            console.log('cart Id set to local', cartId.cart_id);
            await WPUserAPI.getSetCardId(cartId.cart_id)
          });
        }
      }

      let cartItems = await API.getCartItem();

      console.log('', cartItems)
      if (cartItems && cartItems.hasOwnProperty('products') && cartItems.products.length > 0) {
        this.props.loadCartItem(cartItems)
      }


      const { fetchAllCountries, isConnected, fetchCategories, countries, language } = this.props;
      if (isConnected) {
        fetchCategories();
        fetchAllCountries();
      }

      this.setState({ loading: false });

      console.log('getSetLocalData', await WPUserAPI.getSetLocalData());
      console.log('getSetDeviceToken', await WPUserAPI.getSetDeviceToken());
      console.log('getSetCardId', await WPUserAPI.getSetCardId());
      console.log('token', token);

      Languages.setLanguage(language.lang);

      // Enable for mode RTL
      I18nManager.forceRTL(language.rtl);

    } catch (error) {
      console.log('Error In Home Layout***********')
      console.log('Error Is :', error)
    }
  }

  render() {
    const {
      layoutHome,
      onViewProductScreen,
      showCategoriesScreen,
      onShowAll,
      theme: {
        colors: { background },
      },
      language
    } = this.props;

    const { loading } = this.state;

    const isHorizontal = layoutHome == Constants.Layout.horizon || layoutHome == 7;
    const disableViews = ['post'];

    console.log('home', isHorizontal, this.props)

    return (
      <View style={[styles.container, { backgroundColor: Constants.backgroundColor }]}>
        {loading && <Spinner mode="overlay" />}
        {isHorizontal && (
          <HorizonList
            onShowAll={onShowAll}
            onViewProductScreen={onViewProductScreen}
            showCategoriesScreen={showCategoriesScreen}
            onRefresh={this.refreshLayout}
          />
        )}

        {/* disableViews.includes('post') && !isHorizontal &&
          (
            <PostList parentLayout={layoutHome} onViewProductScreen={onViewProductScreen} />
          ) */}
        <ModalLayout />
      </View>
    );
  }
}

const mapStateToProps = ({ products, countries, netInfo, language, user, addresses, currency }) => ({
  layoutHome: products.layoutHome,
  countries,
  isConnected: netInfo.isConnected,
  language,
  languages: user.languages,
  user,
  addresses: addresses.list,
  currency,
  language: language,
  currencys: user.currencys
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps;
  const CountryRedux = require("@redux/CountryRedux");
  const CartRedux = require("@redux/CartRedux");
  const { actions } = require("@redux/CategoryRedux");
  const { user_actions } = require("@redux/UserRedux");
  const { product_actions } = require("@redux/Products");
  const addressRedux = require("@redux/AddressRedux").actions;


  return {
    ...ownProps,
    ...stateProps,
    fetchCategories: () => actions.fetchCategories(dispatch),
    fetchAllCountries: () => CountryRedux.actions.fetchAllCountries(dispatch),
    _languages: (languages) => dispatch(user_actions.languages(languages)),
    _currency: (currency) => dispatch(user_actions.currency(currency)),
    initAllAddresses: (address) => addressRedux.initAllAddresses(dispatch, address),
    selectAddress: (address) => {
      addressRedux.selectAddress(dispatch, address);
    },
    fetchMyOrder: (user) => {
      CartRedux.actions.fetchMyOrder(dispatch, user);
    },
    loadCartItem: (data) => CartRedux.actions.loadCartItem(dispatch, data),
    load_products: (data) => dispatch(product_actions.load_products(data)),
  };
}

export default withTheme(
  connect(
    mapStateToProps,
    undefined,
    mergeProps
  )(Home)
);
